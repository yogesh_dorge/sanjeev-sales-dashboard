﻿Public Module CommonVariables

    Public gLogDateTime_RTC As String = 0
    Public gFailed As String = "FAILED"
    Public gSuccess As String = "SUCCESS"


    Public ColumnStart As String = String.Empty

    Public defaultDate As String = "19/Mar/1990"

    Public fileRefME2M As String = "ME2M"
    Public stringDate As String = "Date"
    Public stringDecimal As String = "Decimal"
    Public imported As String = "Imported"

End Module

﻿Imports vtSqlConnection.vtWindows
Module Tables

    Public tblSync_FileManager_Obj As tblSync_FileManager = New tblSync_FileManager()

    Public Class tblSync_FileManager
        Private vKeyNameArray() As String
        Private vKeyTypeArray() As String
        Private vNonKeyNameArray() As String
        Private vTableName As String
        Private vKeyFieldsString As String

        Private vKeyValueArray() As Object
        Private vNonKeyValueArray() As Object

        Private vDr As DataRow

        Private vKeyElementCount As Integer
        Public RowsAffected As Integer
        Private i As Integer

        Public Exists As Boolean

        Public Sub New()
            vTableName = "tblSync_FileManager"
            vKeyFieldsString = "Id"
            vKeyElementCount = 0
        End Sub

        Property Id As String
            Get
                If vDr Is Nothing Then
                    Return ""
                End If
                Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Id"))))
            End Get

            Set(ByVal value As String)

                vDr.Item("Id") = value
            End Set

        End Property

        Property File_Name As String
            Get
                If vDr Is Nothing Then
                    Return ""
                End If
                Return (Convert.ToString(DBNulls.StringValue(vDr.Item("File_Name"))))
            End Get

            Set(ByVal value As String)

                vDr.Item("File_Name") = value
            End Set

        End Property

        Property File_Path As String
            Get
                If vDr Is Nothing Then
                    Return ""
                End If
                Return (Convert.ToString(DBNulls.StringValue(vDr.Item("File_Path"))))
            End Get

            Set(ByVal value As String)

                vDr.Item("File_Path") = value
            End Set

        End Property

        Property No_of_Columns As Double
            Get
                If vDr Is Nothing Then
                    Return ""
                End If
                Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("No_of_Columns"))))
            End Get

            Set(ByVal value As Double)

                vDr.Item("No_of_Columns") = value
            End Set

        End Property

        Property Table_Name As String
            Get
                If vDr Is Nothing Then
                    Return ""
                End If
                Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Table_Name"))))
            End Get

            Set(ByVal value As String)

                vDr.Item("Table_Name") = value
            End Set

        End Property

        Property Column_Start As String
            Get
                If vDr Is Nothing Then
                    Return ""
                End If
                Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Column_Start"))))
            End Get

            Set(ByVal value As String)

                vDr.Item("Column_Start") = value
            End Set

        End Property

        Private Function tbl_TableName() As String
            Return vTableName
        End Function

        Private Function tbl_whereClause() As String
            Dim Vary() As Object = Nothing
            Dim j As Integer
            Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
            Dim vcol As DataColumn
            With vDr.Table
                j = 0
                For Each vcol In .Columns
                    If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                        ReDim Preserve Vary(j + 2)
                        Vary(j) = vcol.ColumnName
                        Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                        Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                        j = j + 3
                    End If
                Next
            End With
            tbl_whereClause = Sql.WhereClause(Vary)
        End Function
        Private Function tbl_SetClause() As String
            Dim Vary() As Object = Nothing
            Dim i As Integer
            Dim j As Integer
            For i = 0 To vDr.ItemArray.Length - 1
                j = i * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
            Next
            tbl_SetClause = Sql.SetClause(Vary)
        End Function

        Private Function tbl_setClause_NonPk()
            Dim Vary() As Object = Nothing
            Dim i As Integer
            Dim i_used As Integer
            Dim j As Integer
            For i = 0 To vDr.ItemArray.Length - 1
                If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                    j = i_used * 3
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vDr.Table.Columns(i).ColumnName
                    Vary(j + 1) = vDr.Table.Columns(i).DataType
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                    Vary(j + 2) = vDr.Item(i).ToString
                    i_used = i_used + 1
                End If
            Next
            tbl_setClause_NonPk = Sql.SetClause(Vary)
        End Function
        Public Sub read(inId As Int32)
            Clear()
            Try
                vDr.Item("Id") = inId

                Dim vsql As String
                vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
                vDr = Sql.getDataRow(vsql)
                If vDr Is Nothing Then
                    Clear()
                    Exists = False
                Else
                    Exists = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub Update()
            Try
                Dim Vsql As String
                Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
                Sql.ExecuteSql(Vsql)
                RowsAffected = Sql.RowsAffected
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub Insert()
            Dim VcolsCommaseperated As String = ""
            Dim vcolValuesComaSeperated As String = ""
            Dim i As Integer
            Try
                For i = 0 To vDr.ItemArray.Length - 1
                    VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                    vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                    Strings0.PaddedString( _
                    vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString))
                Next
                VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
                vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
                Dim Vsql As String
                Vsql = "insert into  " & vTableName & _
                " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
                Sql.ExecuteSql(Vsql)
                RowsAffected = Sql.RowsAffected
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub Delete()
            Try
                Dim Vsql As String
                Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
                Sql.ExecuteSql(Vsql)
                RowsAffected = Sql.RowsAffected
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub Clear()
            Dim dt As New DataTable
            dt = Sql.getDatatable("Select * from " & tbl_TableName() & " where 1< 0")
            vDr = dt.NewRow()
            RowsAffected = 0
        End Sub
        Private Function fParametersInfo() As String
            Return tbl_whereClause()
        End Function

    End Class

End Module


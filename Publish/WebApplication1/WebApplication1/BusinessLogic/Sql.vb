﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.DateTime
Imports System.Xml
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Exception
Imports System.Reflection
Imports System.ComponentModel
Imports System.IO.IsolatedStorage
Imports System.Runtime.InteropServices

''' <summary>
''' UPDATED ON : 2012 Nov 19, 12:19 PM
''' UPDATED BY : Mahesh
''' </summary>
''' <remarks>TRANSACTION RELATED CHANGES</remarks>
Public Class SqlClass

    Public Enum EN_DatabaseType
        SqlServer
        MSAccess
        MySql
        Oracle
    End Enum

    Private com As SqlCommand
    'Private con As SqlConnection
    Private trans As SqlTransaction
    Private conStr As String
    Public RowsAffected As Integer
    Private dt As New DataTable
    Private vDataRow As DataRow
    Public DatabaseType As EN_DatabaseType
    Public DateWithTimeRequired As Boolean = True

    'Public Class Amount
    Shared mOnesArray(8) As String

    Shared mOneTensArray(9) As String

    Shared mTensArray(7) As String

    Shared mPlaceValues(4) As String

    Public Sub valuearray()

        mOnesArray(0) = "One"

        mOnesArray(1) = "Two"

        mOnesArray(2) = "Three"

        mOnesArray(3) = "Four"

        mOnesArray(4) = "Five"

        mOnesArray(5) = "Six"

        mOnesArray(6) = "Seven"

        mOnesArray(7) = "Eight"

        mOnesArray(8) = "Nine"

        mOneTensArray(0) = "Ten"

        mOneTensArray(1) = "Eleven"

        mOneTensArray(2) = "Twelve"

        mOneTensArray(3) = "Thirteen"

        mOneTensArray(4) = "Fourteen"

        mOneTensArray(5) = "Fifteen"

        mOneTensArray(6) = "Sixteen"

        mOneTensArray(7) = "Seventeen"

        mOneTensArray(8) = "Eightteen"

        mOneTensArray(9) = "Nineteen"

        mTensArray(0) = "Twenty"

        mTensArray(1) = "Thirty"

        mTensArray(2) = "Forty"

        mTensArray(3) = "Fifty"

        mTensArray(4) = "Sixty"

        mTensArray(5) = "Seventy"

        mTensArray(6) = "Eighty"

        mTensArray(7) = "Ninety"

        mPlaceValues(0) = "Hundred"

        mPlaceValues(1) = "Thousand"

        mPlaceValues(2) = "Lakh"

        mPlaceValues(3) = "Crore"



    End Sub
    Public Function GetOnes(ByVal OneDigit As Integer) As String

        GetOnes = ""

        If OneDigit = 0 Then

            Exit Function

        End If

        GetOnes = mOnesArray(OneDigit - 1)

    End Function
    Public Function GetTens(ByVal TensDigit As Integer) As String

        GetTens = ""

        If TensDigit = 0 Or TensDigit = 1 Then

            Exit Function

        End If

        GetTens = mTensArray(TensDigit - 2)

    End Function
    Public Function ConvertNumberToWords(ByVal NumberValue As String) As String
        Try


            valuearray()

            Dim Delimiter As String = " "

            Dim TensDelimiter As String = "-"

            Dim mNumberValue As String = ""

            Dim mNumbers As String = ""

            Dim mNumWord As String = ""

            Dim mFraction As String = ""

            Dim mNumberStack(10) As String

            Dim j As Integer = 0

            Dim i As Integer = 0

            Dim mOneTens As Boolean = False

            ConvertNumberToWords = ""

            ' validate input

            Try

                j = CDbl(NumberValue)

            Catch ex As Exception

                ConvertNumberToWords = "Invalid input."

                Exit Function

            End Try

            'This will take only 2 decimal places only
            NumberValue = Math.Round(CDbl(NumberValue), 2)

            ' get fractional part {if any}
            If InStr(NumberValue, ".") = 0 Then

                ' no fraction

                mNumberValue = NumberValue

            Else

                mNumberValue = Microsoft.VisualBasic.Left(NumberValue, InStr(NumberValue, ".") - 1)

                mFraction = Mid(NumberValue, InStr(NumberValue, ".") + 1)
                If mFraction.Length = 1 Then
                    mFraction = mFraction * 10
                End If
                mFraction = Math.Round(CSng(mFraction), 2) * 100

                If CInt(mFraction) = 0 Then

                    mFraction = ""

                Else

                    'mFraction = "&& " & mFraction & "/100"
                    mFraction = ConvertNumberToWords(mFraction / 100)
                End If

            End If

            mNumbers = mNumberValue.ToCharArray

            ' move numbers to stack/array backwards

            For j = mNumbers.Length - 1 To 0 Step -1

                ReDim Preserve mNumberStack(i)

                mNumberStack(i) = mNumbers(j)

                i += 1

            Next

            For j = mNumbers.Length - 1 To 0 Step -1

                If j >= 9 Then

                    ConvertNumberToWords = "Number length should not be greater than eight characters"

                    Exit Function

                End If

                Select Case j

                    Case 0, 3, 5, 7

                        ' ones value

                        If Not mOneTens Then

                            mNumWord &= GetOnes(Val(mNumberStack(j))) & Delimiter

                        End If

                        Select Case j

                            Case 3

                                ' thousands

                                mNumWord &= mPlaceValues(1) & Delimiter

                            Case 5

                                mNumWord &= mPlaceValues(2) & Delimiter

                            Case 7

                                mNumWord &= mPlaceValues(3) & Delimiter

                        End Select



                    Case Is = 1, 4, 6, 8

                        ' tens value1471013

                        If Val(mNumberStack(j)) = 0 Then

                            mNumWord &= GetOnes(Val(mNumberStack(j - 1))) & Delimiter

                            mOneTens = True

                            'Exit Select

                            Exit For

                        End If

                        If Val(mNumberStack(j)) = 1 Then

                            mNumWord &= mOneTensArray(Val(mNumberStack(j - 1))) & Delimiter

                            mOneTens = True

                            Exit Select

                        End If

                        mNumWord &= GetTens(Val(mNumberStack(j)))

                        ' this places the tensdelimiter; check for succeeding 0

                        If Val(mNumberStack(j - 1)) <> 0 Then

                            mNumWord &= TensDelimiter

                        End If

                        mOneTens = False

                    Case Is = 2

                        ' hundreds value

                        mNumWord &= GetOnes(Val(mNumberStack(j))) & Delimiter

                        If Val(mNumberStack(j)) <> 0 Then

                            mNumWord &= mPlaceValues(0) & Delimiter

                        End If

                End Select

            Next
            If String.IsNullOrEmpty(mFraction) Then
                Return "Rupees " & mNumWord & mFraction & " Only"
            Else
                ' mFraction = mFraction.Replace("Paise", "Rupees")
                mFraction = mFraction.Replace("Rupees", "Paise")
                Return "Rupees " & mNumWord & " and " & mFraction
            End If
            Return ""
        Catch ex As Exception
            'vt_Cls_ErrorObject.Save("Business.Amount.ConvertNumberToWords", ex.Message, "", "")
            'vt_Cls_ErrorObject.Raise()
            Return ""
        End Try

        Return ""
    End Function


    'End Class


    'Public Shared Sub valuearray()

    '    mOnesArray(0) = "One"

    '    mOnesArray(1) = "Two"

    '    mOnesArray(2) = "Three"

    '    mOnesArray(3) = "Four"

    '    mOnesArray(4) = "Five"

    '    mOnesArray(5) = "Six"

    '    mOnesArray(6) = "Seven"

    '    mOnesArray(7) = "Eight"

    '    mOnesArray(8) = "Nine"

    '    mOneTensArray(0) = "Ten"

    '    mOneTensArray(1) = "Eleven"

    '    mOneTensArray(2) = "Twelve"

    '    mOneTensArray(3) = "Thirteen"

    '    mOneTensArray(4) = "Fourteen"

    '    mOneTensArray(5) = "Fifteen"

    '    mOneTensArray(6) = "Sixteen"

    '    mOneTensArray(7) = "Seventeen"

    '    mOneTensArray(8) = "Eightteen"

    '    mOneTensArray(9) = "Nineteen"

    '    mTensArray(0) = "Twenty"

    '    mTensArray(1) = "Thirty"

    '    mTensArray(2) = "Forty"

    '    mTensArray(3) = "Fifty"

    '    mTensArray(4) = "Sixty"

    '    mTensArray(5) = "Seventy"

    '    mTensArray(6) = "Eighty"

    '    mTensArray(7) = "Ninety"

    '    mPlaceValues(0) = "Hundred"

    '    mPlaceValues(1) = "Thousand"

    '    mPlaceValues(2) = "Lakh"

    '    mPlaceValues(3) = "Crore"



    'End Sub

    'Public Shared Function ConvertNumberToWords(ByVal NumberValue As String) As String
    '    Try


    '        valuearray()

    '        Dim Delimiter As String = " "

    '        Dim TensDelimiter As String = "-"

    '        Dim mNumberValue As String = ""

    '        Dim mNumbers As String = ""

    '        Dim mNumWord As String = ""

    '        Dim mFraction As String = ""

    '        Dim mNumberStack(10) As String

    '        Dim j As Integer = 0

    '        Dim i As Integer = 0

    '        Dim mOneTens As Boolean = False

    '        ConvertNumberToWords = ""

    '        ' validate input

    '        Try

    '            j = CDbl(NumberValue)

    '        Catch ex As Exception

    '            ConvertNumberToWords = "Invalid input."

    '            Exit Function

    '        End Try

    '        'This will take only 2 decimal places only
    '        NumberValue = Math.Round(CDbl(NumberValue), 2)

    '        ' get fractional part {if any}
    '        If InStr(NumberValue, ".") = 0 Then

    '            ' no fraction

    '            mNumberValue = NumberValue

    '        Else

    '            mNumberValue = Microsoft.VisualBasic.Left(NumberValue, InStr(NumberValue, ".") - 1)

    '            mFraction = Mid(NumberValue, InStr(NumberValue, ".") + 1)
    '            If mFraction.Length = 1 Then
    '                mFraction = mFraction * 10
    '            End If
    '            mFraction = Math.Round(CSng(mFraction), 2) * 100

    '            If CInt(mFraction) = 0 Then

    '                mFraction = ""

    '            Else

    '                'mFraction = "&& " & mFraction & "/100"
    '                mFraction = ConvertNumberToWords(mFraction / 100)
    '            End If

    '        End If

    '        mNumbers = mNumberValue.ToCharArray

    '        ' move numbers to stack/array backwards

    '        For j = mNumbers.Length - 1 To 0 Step -1

    '            ReDim Preserve mNumberStack(i)

    '            mNumberStack(i) = mNumbers(j)

    '            i += 1

    '        Next

    '        For j = mNumbers.Length - 1 To 0 Step -1

    '            If j >= 9 Then

    '                ConvertNumberToWords = "Number length should not be greater than eight characters"

    '                Exit Function

    '            End If

    '            Select Case j

    '                Case 0, 3, 5, 7

    '                    ' ones value

    '                    If Not mOneTens Then

    '                        mNumWord &= GetOnes(Val(mNumberStack(j))) & Delimiter

    '                    End If

    '                    Select Case j

    '                        Case 3

    '                            ' thousands

    '                            mNumWord &= mPlaceValues(1) & Delimiter

    '                        Case 5

    '                            mNumWord &= mPlaceValues(2) & Delimiter

    '                        Case 7

    '                            mNumWord &= mPlaceValues(3) & Delimiter

    '                    End Select



    '                Case Is = 1, 4, 6, 8

    '                    ' tens value1471013

    '                    If Val(mNumberStack(j)) = 0 Then

    '                        mNumWord &= GetOnes(Val(mNumberStack(j - 1))) & Delimiter

    '                        mOneTens = True

    '                        'Exit Select

    '                        Exit For

    '                    End If

    '                    If Val(mNumberStack(j)) = 1 Then

    '                        mNumWord &= mOneTensArray(Val(mNumberStack(j - 1))) & Delimiter

    '                        mOneTens = True

    '                        Exit Select

    '                    End If

    '                    mNumWord &= GetTens(Val(mNumberStack(j)))

    '                    ' this places the tensdelimiter; check for succeeding 0

    '                    If Val(mNumberStack(j - 1)) <> 0 Then

    '                        mNumWord &= TensDelimiter

    '                    End If

    '                    mOneTens = False

    '                Case Is = 2

    '                    ' hundreds value

    '                    mNumWord &= GetOnes(Val(mNumberStack(j))) & Delimiter

    '                    If Val(mNumberStack(j)) <> 0 Then

    '                        mNumWord &= mPlaceValues(0) & Delimiter

    '                    End If

    '            End Select

    '        Next
    '        If String.IsNullOrEmpty(mFraction) Then
    '            Return "Rupees " & mNumWord & mFraction & " Only"
    '        Else
    '            ' mFraction = mFraction.Replace("Paise", "Rupees")
    '            mFraction = mFraction.Replace("Rupees", "Paise")
    '            Return "Rupees " & mNumWord & " and " & mFraction
    '        End If
    '        Return ""
    '    Catch ex As Exception
    '        'vt_Cls_ErrorObject.Save("Business.Amount.ConvertNumberToWords", ex.Message, "", "")
    '        'vt_Cls_ErrorObject.Raise()
    '        Return ""
    '    End Try

    '    Return ""
    'End Function


    Public Function ModifySqlAsperDatabase(ByVal insql As String) As String
        'If undefined - zero or Nullable set default to ms access
        Dim vsql As String = ""
        If Trim(insql) <> "" Then
            vsql = insql

            'If DatabaseType = EN_DatabaseType.MSAccess Then
            '    ' change double quote to single quote
            '    vsql = Replace(vsql, """", "'")
            '    vsql = Strings0.WhiteSpaceReplace(vsql)
            'ElseIf DatabaseType = EN_DatabaseType.SqlServer Then

            Dim vQuoteChar As String
            Dim vHashChar As String
            Dim vWildChar As String

            vQuoteChar = "'"
            vHashChar = "'"
            vWildChar = "%"

            vsql = Replace(vsql, """", vQuoteChar)

            vsql = Replace(vsql, "#", vHashChar)

            vsql = Replace(vsql, "%", vWildChar)

            vsql = Replace(vsql, "= NULL", "= ''")
            ' End If

            Return vsql
        End If
        Return ""
    End Function

    Public Property ConnectionString() As String
        Get
            Return conStr
        End Get
        Set(ByVal value As String)
            If Trim(value) <> "" Then
                conStr = value
            End If
        End Set
    End Property

    Public Function ServerSettings(ByRef con As SqlConnection)
        Try
            If DatabaseType = EN_DatabaseType.SqlServer Then
                ExecuteSql(con, "SET DATEFORMAT dmy")
                Return True
            End If

        Catch ex As Exception
        End Try

    End Function

    'Public Function getConnectionObject() As SqlConnection
    '    Try
    '        If con Is Nothing Then
    '            Throw New Exception("getConnectionObject in (Sql) VTelebyte Class, Connection is not established(Use VT_Setup() at starting point)")
    '        End If
    '        If con.State = ConnectionState.Open Then
    '            Return con
    '        Else
    '            OpenConnection()
    '            Return con
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Sub OpenConnection(ByRef con As SqlConnection)
        Try
            If con Is Nothing Then
                con = New SqlClient.SqlConnection(ConnectionString)
                con.Open()
                com = New SqlCommand("", con)
                com.CommandTimeout = 120
                ServerSettings(con)
            End If
            If con.State <> ConnectionState.Open Then
                ' con.ConnectionString = conStr
                con.Open()
                com = New SqlCommand("", con)
                com.CommandTimeout = 120
            End If
            If com Is Nothing Then
                com = New SqlCommand("", con)
                com.CommandTimeout = 120
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub CloseConnection(ByRef con As SqlConnection)
        If trans Is Nothing Then
            If con.State = ConnectionState.Open Then
                con.Close()
                com = Nothing
            End If
        End If

    End Sub
    Public Sub CloseConnection_WithTransaction(ByRef con As SqlConnection)

        If con.State = ConnectionState.Open Then
            con.Close()
            com = Nothing
        End If
    End Sub

    Public Sub BeginTran(ByRef con As SqlConnection)
        trans = Nothing
        Try
            OpenConnection(con)
            trans = con.BeginTransaction
            com.Transaction = trans
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub CommitTran(ByRef con As SqlConnection)
        Try
            If trans IsNot Nothing Then
                trans.Commit()
                trans.Dispose()
                trans = Nothing
            End If
            CloseConnection(con)

        Catch ex As Exception
            Throw ex
        Finally
            If trans IsNot Nothing Then
                trans.Dispose()
            End If
            CloseConnection(con)
        End Try
    End Sub

    Public Sub RollBack(ByRef con As SqlConnection)
        Try
            If trans IsNot Nothing Then
                trans.Rollback()
                trans.Dispose()
                trans = Nothing
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If trans IsNot Nothing Then
                trans.Dispose()
            End If
            CloseConnection(con)
        End Try
    End Sub

    '#End Region

#Region "WITHOUT TRANSACTION"

    Public Sub ExecuteSql(ByRef con As SqlConnection, ByVal psql As String)
        If Trim(psql) = "" Then
            Throw New Exception("Query is not given")
            Exit Sub
        End If

        OpenConnection(con)
        psql = ModifySqlAsperDatabase(psql)
        com.CommandText = psql
        RowsAffected = 0
        Try
            RowsAffected = com.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection(con)
        End Try


    End Sub

    Public Function getReader(ByRef con As SqlConnection, ByVal psql As String) As SqlDataReader
        Try
            If Trim(psql) <> "" Then
                'If con.State = ConnectionState.Closed Then
                '    con.Open()
                '    ServerSettings()
                'End If
                OpenConnection(con)
                psql = ModifySqlAsperDatabase(psql)
                com.CommandText = psql
                If trans Is Nothing Then
                    Return com.ExecuteReader(CommandBehavior.CloseConnection)
                Else
                    Return com.ExecuteReader()
                End If

            Else
                getReader = Nothing
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function getDatatable(ByRef con As SqlConnection, ByVal insql As String) As DataTable
        Try
            If Trim(insql) <> "" Then
                Dim dt As New DataTable
                dt.Clear()
                dt.Columns.Clear()
                Dim Vdr1 As SqlClient.SqlDataReader
                OpenConnection(con)
                Vdr1 = getReader(con, insql)
                dt.Load(Vdr1)
                Vdr1.Close()
                Vdr1 = Nothing

                DataTable0.AllowNulls(dt)

                Return dt
            End If
        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection(con)
        End Try
    End Function

#End Region

    'Public Function getASN_Number(ByRef con As SqlConnection, ByVal VendCode As String, ByVal PlantCode As String, ByVal PO_Type As String) As String
    '    Try
    '        Dim ASN_No As String
    '        com.CommandType = CommandType.StoredProcedure
    '        com.Connection = con
    '        con.Open()
    '        com.Parameters.Add(New SqlParameter("@VendorCode", VendCode))
    '        com.Parameters.Add(New SqlParameter("@Plant_Code", PlantCode))
    '        com.Parameters.Add(New SqlParameter("@PO_Type", PO_Type))
    '        com.CommandText = "Sp_GetASNNo"
    '        com.ExecuteNonQuery()

    '        com.CommandType = CommandType.Text
    '        ASN_No = get_Next_ASNNo(con)
    '        con.Close()
    '        Return ASN_No
    '    Catch ex As Exception
    '        com.CommandType = CommandType.Text
    '        Throw ex
    '        Return ""
    '    Finally
    '        com.CommandType = CommandType.Text
    '    End Try

    'End Function

    Public Function getASN_Number(ByRef con As SqlConnection, ByVal VendCode As String, ByVal PlantCode As String, ByVal PO_Type As String) As String
        Try
            Dim ASN_No As String
            com.CommandType = CommandType.StoredProcedure
            com.Connection = con
            con.Open()
            Dim asnNo As String = String.Empty
            com.Parameters.Add(New SqlParameter("@retValue", SqlDbType.VarChar, 50))
            com.Parameters("@retValue").Direction = ParameterDirection.Output


            com.CommandText = "getNextStickerNumber"
            com.ExecuteNonQuery()

            com.CommandType = CommandType.Text
            ASN_No = com.Parameters("@retValue").Value  'get_Next_ASNNo(con)
            con.Close()
            Return ASN_No
        Catch ex As Exception
            com.CommandType = CommandType.Text
            Throw ex
            Return ""
        Finally
            com.CommandType = CommandType.Text
        End Try

    End Function
    Public Function get_Next_ASNNo(ByRef con As SqlConnection) As String
        Dim vsql As String = String.Empty
        Dim vval As String = String.Empty


        Try
            Dim vvalds As String
            Dim vvald As Double
            vsql = "Select max(docID) from tbl_DocID"

            vval = SqlObj.Sql.GetColumnValue(con, vsql)
            vvald = DBNulls.NumberValue(vval)

            'vvalds = Microsoft.VisualBasic.Strings.Right("000000000" & Trim(Convert.ToString(vvald)), 10)

            vvalds = (Convert.ToString(vvald)).PadLeft(10, "0")
            Return vvalds

        Catch ex As Exception

            Throw ex
            Return ""
        End Try

    End Function

    Public Function getDataRow(ByRef con As SqlConnection, ByVal insql As String) As DataRow
        Try
            If Trim(insql) <> "" Then
                dt = getDatatable(con, insql)
                If dt.Rows.Count = 0 Then
                    Return Nothing
                End If
                vDataRow = dt.Rows(0)
                Return vDataRow
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetColumnValue(ByRef con As SqlConnection, ByVal insql As String) As String
        Dim drw As DataRow
        Try
            drw = getDataRow(con, insql)

            If drw Is Nothing Then
                Return ""
            Else
                Dim col As Object
                col = Convert.ToString(drw.Item(0))
                Return col.ToString
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function WhereClause(ByVal ParamArray Plist_ColumnName_Type_value() As Object) As String
        Dim p() As Object
        Try
            If UCase(Trim(TypeName(Plist_ColumnName_Type_value(0)))) = "VARIANT()" Then
                p = CType(Plist_ColumnName_Type_value(0), Object())
            Else
                p = Plist_ColumnName_Type_value
            End If

            Dim n As Integer
            Dim i As Integer
            Dim Wherestr As String
            Dim vcomma As String

            n = UBound(p)
            Wherestr = ""
            For i = 0 To n Step 3
                If i > 0 Then
                    vcomma = "  AND   "
                Else
                    vcomma = ""
                End If
                Wherestr = Wherestr & vcomma & CStr(p(i)) & " = "
                Wherestr = Wherestr & Strings0.PaddedString(CStr(p(i + 2)) & vbNullString, CStr(p(i + 1)), DateWithTimeRequired)
                Wherestr = Replace(Wherestr, "= Null", "is null")

            Next i
            Wherestr = ModifySqlAsperDatabase(Wherestr)
            Return Wherestr
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function WhereClauseNonEqual(ByVal ParamArray Plist_ColumnName_Type_ComparisonSign_value() As Object) As String
        Dim p() As Object
        Try
            If UCase(Trim(TypeName(Plist_ColumnName_Type_ComparisonSign_value(0)))) = "VARIANT()" Then
                p = Plist_ColumnName_Type_ComparisonSign_value(0)
            Else
                p = Plist_ColumnName_Type_ComparisonSign_value
            End If
            Dim n As Integer
            Dim i As Integer
            Dim Wherestr As String
            Dim vcomma As String
            n = UBound(p)
            Wherestr = ""
            For i = 0 To n Step 4
                If i > 0 Then
                    vcomma = "  AND   "
                Else
                    vcomma = ""
                End If
                Wherestr = Wherestr & vcomma & CStr(p(i)) & CStr(p(i + 2)) '" = "
                Wherestr = Wherestr & Strings0.PaddedString(CStr(p(i + 2 + 1) & vbNullString), CStr(p(i + 1)), DateWithTimeRequired)
            Next i
            Wherestr = ModifySqlAsperDatabase(Wherestr)
            Return Wherestr
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SetClause(ByVal ParamArray Plist_ColumnName_Type_value() As Object) As String
        Dim p() As Object
        Dim n As Integer
        Dim i As Integer
        Dim Wherestr As String
        Dim vcomma As String
        Try
            If UCase(Trim(TypeName(Plist_ColumnName_Type_value(0)))) = "VARIANT()" Then
                p = CType(Plist_ColumnName_Type_value(0), Object())
            Else
                p = Plist_ColumnName_Type_value
            End If

            n = UBound(p)
            Wherestr = ""
            For i = 0 To n Step 3
                If i > 0 Then
                    vcomma = "  ,   "
                Else
                    vcomma = ""
                End If
                Wherestr = Wherestr & vcomma & CStr(p(i)) & " = "
                Wherestr = Wherestr & Strings0.PaddedString(CStr(p(i + 2)) & vbNullString, CStr(p(i + 1)), DateWithTimeRequired)
            Next i
            Wherestr = ModifySqlAsperDatabase(Wherestr)
            Return Wherestr
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Datatype_S_N_D_B(ByVal intype As String) As String

        Select Case UCase(intype)
            Case UCase("System.Byte")
                Return "S"
            Case UCase("System.Char")
                Return "S"
            Case UCase("System.String")
                Return "S"

            Case UCase("System.Int32")
                Return "N"
            Case UCase("System.Double")
                Return "N"
            Case UCase("System.Int64")
                Return "N"
            Case UCase("System.Int16")
                Return "N"
            Case UCase("System.Decimal")
                Return "N"
            Case UCase("System.Boolean")
                Return "N"
            Case UCase("System.Single")
                Return "N"

            Case UCase("System.DateTime")
                Return "D"

            Case UCase("System.Date")
                Return "D"

            Case UCase("System.Boolean")
                Return "B"
        End Select
        Return ""
    End Function

    Function checkforprimarykey(ByVal str As String, ByVal str1 As String) As Boolean
        Dim pkstrs() As String = Split(str1, ",")
        For i = 0 To UBound(pkstrs)
            If UCase(str) = UCase(pkstrs(i)) Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Function get_Next_DocID(ByRef con As SqlConnection) As String
        Return Guid.NewGuid().ToString()
        'Dim vsql As String = String.Empty
        'Dim vval As String = String.Empty

        'Try
        '    Dim vvalds As String
        '    Dim vvald As Double
        '    vsql = "Select docID from tbl_DocID"

        '    vval = SqlObj.Sql.GetColumnValue(con, vsql)

        '    If Val(vval) = 0 Then
        '        vsql = "Insert into tbl_DocID values ('000000000')"
        '        vval = "000000000"
        '        SqlObj.Sql.ExecuteSql(con, vsql)
        '    End If


        '    vvald = Convert.ToDouble(vval)
        '    vvald = vvald + 1
        '    vvalds = Microsoft.VisualBasic.Strings.Right("000000000" & Trim(Convert.ToString(vvald)), 9)
        '    vsql = "Update tbl_DocID set " & _
        '   SqlObj.Sql.SetClause("DocID", "S", vvalds)


        '    SqlObj.Sql.ExecuteSql(con, vsql)
        '    Return vvalds

        'Catch ex As Exception

        '    Throw ex
        '    Return ""
        'End Try

    End Function


    Public Function Get_Next_Doc_No(ByRef con As SqlConnection, ByVal indocDate As Date, ByVal inDoc_Date_Caption As String, ByVal inTableName As String, ByVal inSubTypeText As String, ByVal inDocumen_number_Increament_On As String, ByVal inColNames As String, ByVal inDocNumberPattern As String, ByVal UserID As String) As String
        Try

            Dim vTableName As String
            Dim vsql As String
            Dim vval As String
            Dim vLastDocNo As String
            Dim Vpattern As String
            Dim vretValue As String
            Dim vIncrementOn As String

            If inTableName = "" Then
                Throw New Exception("Invalid table name : document number generation")
            Else
                vTableName = inTableName
            End If

            ' --- increment on
            vIncrementOn = inDocumen_number_Increament_On

            '-- last docno from lable

            If vIncrementOn = "" Then
                vsql = "Select max(" & inColNames & ") from " & vTableName
            End If

            '--last Date
            If UCase(vIncrementOn) = "D" Then
                vsql = "Select max(" & inColNames & ") from " & vTableName & " where " & _
                SqlObj.Sql.WhereClause(inDoc_Date_Caption, "D", Format(Now, "dd/MMM/yyyy"))
            End If

            If UCase(vIncrementOn) = "MM" Or UCase(vIncrementOn) = "MMM" Or UCase(vIncrementOn) = "M" Or UCase(vIncrementOn) = "YYYYMM" Then
                Dim vmonth As Int32
                vmonth = Convert.ToInt32(Format(indocDate, "MM"))
                vsql = "Select max(" & inColNames & ") from " & vTableName & " where " & _
                SqlObj.Sql.WhereClause("month(" & inDoc_Date_Caption & ")", "N", vmonth, "CreatedBy", "S", "" & UserID & "")
            End If

            'If UCase(vIncrementOn) = "YY" Or UCase(vIncrementOn) = "YYYY" Or UCase(vIncrementOn) = "Y" Then
            '    Dim vYear As Int32
            '    vYear = Convert.ToInt32(Format(indocDate, "yyyy"))

            '    Dim dt As DataTable
            '    dt = SqlObj.Sql.getDatatable(con, "SELECT * FROM COM_YearMaster WHERE StartDate <= '" & Date0.Format_DD_MON_YYYY(indocDate) & "' AND EndDate >= '" & Date0.Format_DD_MON_YYYY(indocDate) & "'")

            '    vsql = "Select max(Doc_Number) from " & vTableName & " where Doc_Date >= '" & Date0.Format_DD_MON_YYYY(dt.Rows(0)(1)) & "' AND Doc_Date <= '" & Date0.Format_DD_MON_YYYY(dt.Rows(0)(2)) & "'" & VSubTypeWhereClauseAND

            'End If

            vsql = vsql.Trim

            'If Right(UCase(vsql), 5) = "WHERE" Then
            '    vsql = Left(vsql, Len(vsql) - 5)
            'End If

            vLastDocNo = SqlObj.Sql.GetColumnValue(con, vsql)

            Dim Index As Integer
            Index = InStr(vLastDocNo, "/")
            vLastDocNo = vLastDocNo.Substring(Index, Len(vLastDocNo) - Index)

            Vpattern = inDocNumberPattern

            Dim PreviousYear As Integer
            Dim NextYear As Integer
            Dim CurrentYear As Integer = indocDate.Year

            If indocDate.Month > 3 Then
                PreviousYear = indocDate.Year
                NextYear = indocDate.Year + 1
            Else
                CurrentYear = indocDate.Year - 1
                PreviousYear = indocDate.Year - 1
                NextYear = indocDate.Year
            End If


            Vpattern = Replace(Vpattern, "$YYYY$", CurrentYear.ToString)
            Vpattern = Replace(Vpattern, "$FAY1$", PreviousYear.ToString.Substring(2, 2))
            Vpattern = Replace(Vpattern, "$FAY2$", NextYear.ToString.Substring(2, 2))
            Vpattern = Replace(Vpattern, "$YY$", CurrentYear.ToString.Substring(2, 2))
            Vpattern = Replace(Vpattern, "$MMM$", Format(indocDate, "MMM"))
            Vpattern = Replace(Vpattern, "$MM$", indocDate.Month.ToString.PadLeft(2, "0"))
            Vpattern = Replace(Vpattern, "$DD$", indocDate.Day.ToString.PadLeft(2, "0"))


            Dim sp As Integer, ep As Integer
            sp = InStr(Vpattern, "?")
            ep = Strings0.Get_Ending_Postion_Of_RepeatingCharSet(Vpattern, "?")
            Dim vValDbl As Double

            If vLastDocNo > "" Then
                Try
                    vval = Mid(vLastDocNo, sp, ep - sp + 1)
                    vValDbl = Convert.ToDouble(vval) + 1
                Catch ex As Exception
                    vValDbl = 1
                End Try
            Else
                vValDbl = 1
            End If

            vretValue = Strings0.Substitute_Repeating_CharSet_BY_Number(Vpattern, "?", vValDbl)

            Return vretValue

        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class

Public Class DBNulls
    Public Shared Function NumberValue(ByVal invalue As Object) As Double
        Try
            Return Convert.ToDouble(invalue)
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Shared Function StringValue(ByVal invalue As Object) As String
        Try
            Return Convert.ToString(invalue)
        Catch ex As Exception
            Return ""
        End Try
    End Function


End Class

Public Class Strings0

    Public Shared Function C_DBL(ByVal in_str As String) As Double
        If String.IsNullOrEmpty(in_str) Then
            Return 0
        Else
            Return CDbl(in_str)
        End If
    End Function

    Public Shared Function GetUsableDateRTC(ByVal vDate As Date) As String
        Return Format(vDate, "yyyyMMdd")
    End Function

    Public Shared Function GetUsableTimeRTC(ByVal vDate As Date) As String
        Return Format(vDate, "HHmmss")
    End Function

    Public Shared Function GetUsableDateTimeRTC(ByVal vDate As Date) As String
        Return Format(vDate, "yyyyMMddHHmmss")
    End Function

    Public Shared Function GetUsableSystemTimeStamp() As String
        Return Format(Now(), "yyyyMMddHHmmss")
    End Function

    Public Shared Function findIndexsWord(ByVal instring As String, ByVal index1 As Integer) As String
        Dim vstr() As String
        vstr = Split(instring, " ")
        If UBound(vstr) >= index1 Then
            Return vstr(index1)
        Else
            Return ""
        End If

    End Function
    Public Shared Function FindWordsIndex(ByVal instring As String, ByVal inWord As String) As Integer
        Dim vstr() As String
        vstr = Split(instring, " ")
        Dim i As Integer

        For i = 0 To vstr.Length - 1
            If vstr(i) = (inWord) Then
                Return i
                Exit Function
            End If
        Next
        Return -1
    End Function

    Public Shared Function PaddedString(ByVal ValueToPad As Object, ByVal DataType_SND As String, ByVal DateWithTimeRequired As Boolean) As String
        Try

            ValueToPad = RemoveCharacters(CStr((ValueToPad)), """")
            ValueToPad = RemoveCharacters(CStr((ValueToPad)), "'")
            'ValueToPad = Replace(CStr((ValueToPad)), """", """""")

            Dim strString As String
            strString = ""
            If Trim(UCase(Left(DataType_SND, 1))) = "S" Or Trim(UCase(Left(DataType_SND, 1))) = "T" Then
                strString = """" & CStr(ValueToPad) & """"
            ElseIf UCase(Left(DataType_SND, 1)) = "N" Then
                strString = "" & CStr(ValueToPad) & ""
            ElseIf UCase(Left(DataType_SND, 1)) = "D" Then
                ' Only For Access database
                'If Sql.DatabaseType = EN_DatabaseType.MSAccess Then
                If ValueToPad.Trim() <> "" Then
                    If DateWithTimeRequired Then
                        Dim s As String
                        s = Date0.Format_DD_MON_YYYY__HH_MI_SS(ValueToPad)

                        strString = "#" & s & "#"
                    Else
                        strString = "#" & Date0.Format_DD_MON_YYYY(Date.Parse(ValueToPad)).Replace(" 12:00:00 PM", "") & "#"
                        '#01/Jan/1930#
                        If Mid(strString, 9, 2) = "19" Then
                            If Mid(strString, 11, 2) < "50" Then
                                strString = Mid(strString, 1, 8) & "20" & Mid(strString, 11)
                            End If
                        End If
                    End If
                Else
                    strString = "null"
                End If
                'Else
                '   strString = "#" & ((CStr(ValueToPad))) & "#"
                'End If
            ElseIf UCase(Left(DataType_SND, 1)) = "B" Then
                strString = "" & CStr(ValueToPad) & ""
            End If
            If strString = """""" Or strString = "" Or _
                strString = "##" Or strString = "#//#" Then
                strString = "null"
            End If
            Return strString
        Catch ex As Exception
            Throw ex
        End Try
        Return ""
    End Function

    Public Shared Function RemoveCharacters(ByVal Pstr As String, ByVal pCharachers As String) As String
        Try
            Dim n As Integer
            Dim retstr As String
            Dim ch As String
            Dim RmCh As String
            Dim i As Integer, j As Integer
            retstr = ""
            n = Len(Pstr)

            For i = 1 To n

                For j = 1 To Len(pCharachers)
                    RmCh = Mid(pCharachers, j, 1)
                    If Mid(Pstr, i, 1) = RmCh Then
                        GoTo NextI
                    End If
                Next j
                ch = Mid(Pstr, i, 1)
                retstr = retstr & ch
NextI:
            Next i

            Return retstr
        Catch ex As Exception
            Throw ex
        End Try
        Return ""
    End Function

    Public Shared Function WhiteSpaceReplace(ByVal Pstr As String) As String

        '---- Ends Common error handling routine.............

        Dim n As Integer
        Dim retstr As String
        Dim ch As String
        Dim i As Integer
        retstr = ""
        n = Len(Pstr)
        ' Remove White space and put space -----------
        For i = 1 To n
            If Asc(Mid(Pstr, i, 1)) = 0 Then
                ch = Chr(32)
            Else
                ch = Mid(Pstr, i, 1)
            End If
            retstr = retstr & ch
        Next i

        Return retstr
    End Function


    Public Shared Function Get_Ending_Postion_Of_RepeatingCharSet(ByVal inpString As String, ByVal inpRepeatingChr As String) As Integer
        Try
            ' input 2008$RM$?????/a , ? output 14
            Dim i2 As Integer
            i2 = InStr(StrReverse(inpString), inpRepeatingChr)
            i2 = Len(inpString) - i2 + 1
            Return i2
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function Substitute_Repeating_CharSet_BY_Number(ByVal inpString As String, ByVal InpRepeatingChar As String, ByVal inpReplecingNumber As Double) As String
        Try
            ' input 2008$RM$????? , ? , 301 output 2008$RM$00301
            Dim inkey_Chrs() As Char
            inkey_Chrs = inpString
            Dim i1 As Integer, i2 As Integer
            i1 = InStr(inpString, InpRepeatingChar)
            i2 = InStr(StrReverse(inpString), InpRepeatingChar)
            i2 = Len(inpString) - i2 + 1
            If i1 <= 0 Then
                Return inpString
            End If
            If i2 <= i1 Then
                Return inpString
            End If


            Dim RV As String
            RV = "000000000000000" & Trim(Convert.ToString(inpReplecingNumber))
            Return Replace(inpString, Repeate(InpRepeatingChar, i2 - i1 + 1), Right(RV, i2 - i1 + 1))
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Shared Function Repeate(ByVal ins As String, ByVal intimes As Integer) As String
        Try
            'Input $,4 Output $$$$
            Dim r As String
            Dim i As Integer
            i = 0
            r = ""
            Do While True
                i = i + 1
                If i > intimes Then
                    Exit Do
                End If
                r = r & ins
            Loop
            Return r
        Catch ex As Exception
            Throw ex
        End Try
    End Function





End Class

Public Class DataTable0
    Public Shared Sub AllowNulls(ByVal indt As DataTable)
        Dim c As DataColumn
        For Each c In indt.Columns
            c.AllowDBNull = True
            'c.MaxLength = 100
        Next
    End Sub
End Class

Public Class Date0

    Public Shared Function FA_Years_Start_Date(ByVal inYearCode_YYYY_YYYY As String, ByVal inMonth As Integer) As Date
        Dim Vyear As String
        If inMonth >= 4 And inMonth <= 12 Then
            Vyear = Mid(inYearCode_YYYY_YYYY, 1, 4)
        Else
            Vyear = Mid(inYearCode_YYYY_YYYY, 6, 4)
        End If
        Dim vstartDT As Date

        vstartDT = CDate("01/" & Mid(MonthName(inMonth), 1, 3) & "/" & Vyear)
        Return vstartDT
    End Function
    Public Shared Function FA_Year_End_Date(ByVal inYearCode_YYYY_YYYY As String, ByVal inMonth As Integer) As Date
        Dim Vyear As String
        If inMonth >= 4 And inMonth <= 12 Then
            Vyear = Mid(inYearCode_YYYY_YYYY, 1, 4)
        Else
            Vyear = Mid(inYearCode_YYYY_YYYY, 6, 4)
        End If
        Dim vstartDT As Date
        Dim vEndDT As Date
        vstartDT = CDate("01/" & Mid(MonthName(inMonth), 1, 3) & "/" & Vyear)
        Dim m As Integer

        m = vstartDT.Month
        Dim d As Date = vstartDT
        For i = 1 To 31
            d = DateAdd("d", i, vstartDT)
            If d.Month <> m Then
                Exit For
            End If
        Next
        vEndDT = DateAdd("d", -1, d)
        Return vEndDT
    End Function

    Public Shared Function DatetimePickers_String(ByVal inDtpickerControl As Object) As String
        Dim dt As String
        dt = (Date0.Format_DD_MON_YYYY(inDtpickerControl.Value))
        Return dt
    End Function
    Public Shared Function DatetimePickers_Date(ByVal inDtpickerControl As Object) As Date
        Dim dt As Date
        dt = CDate(Date0.Format_DD_MON_YYYY(inDtpickerControl.Value))
        Return dt
    End Function
    Public Shared Function Today_Date() As Date
        Dim x As String
        Dim d As Date
        Try
            x = Format(Now, "dd/MMM/yyyy")
            d = CDate(x)
            Return d

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function Today_Time() As String
        Dim x As String

        Try
            x = Format(Now, "HH:mm")

            Return x

        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function

    Public Shared Function Format_DD_MON_YYYY(ByVal indate As Date) As String
        'Return indate.ToLongDateString & " " & indate.ToLongTimeString
        Return Format(indate, "dd/MMM/yyyy") ' & " " & Format(indate, "hh:mm:ss tt")
    End Function

    Public Shared Function Format_DD_MON_YYYY__HH_MI_SS(ByVal indate As Date) As String
        'Return indate.ToLongDateString & " " & indate.ToLongTimeString
        Return Format(indate, "dd/MMM/yyyy") & " " & Format(indate, "hh:mm:ss tt")
    End Function
End Class


﻿
Public Class DOC_REPO_Log_Details
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.DOCID = DocID
        intbl.PO_DOCID = PO_DocId
        intbl.SR_NUMBER = Sr_number
        intbl.MATERIAL_CODE = Material_Code
        intbl.DESCRIPTION = Description
        intbl.QTY = Qty
        intbl.UOM = UOM
        intbl.BATCH_NUMBER = Batch_Number
        intbl.STOR_LOC = Stor_Loc
        intbl.PLANT_CODE = Plant_Code
        intbl.PO_QTY = PO_QTY
        intbl.GR_QTY = GR_QTY
        intbl.OPEN_QTY = OPEN_QTY
        intbl.SCHEDULED_QTY = SCHEDULED_QTY
        intbl.IN_TRANSIT_QTY = IN_TRANSIT_QTY
        intbl.ALLOWED_QTY = ALLOWED_QTY
        intbl.RATE_ACTUAL = RATE_ACTUAL
        intbl.RATE_EXPECTED = RATE_EXPECTED
        intbl.BASE_VALUE_ACTUAL = BASE_VALUE_ACTUAL
        intbl.BASE_VALUE_EXPECTED = BASE_VALUE_EXPECTED
        intbl.ASSESEBL_VALUE_ACTUAL = ASSESEBL_VALUE_ACTUAL
        intbl.ASSESEBL_VALUE_EXPECTED = ASSESEBL_VALUE_EXPECTED
        intbl.BED_ACTUAL = BED_ACTUAL
        intbl.BED_EXPECTED = BED_EXPECTED
        intbl.BED_PERCENTAGE = BED_PERCENTAGE
        intbl.ECESS_ACTUAL = ECESS_ACTUAL
        intbl.ECESS_EXPECTED = ECESS_EXPECTED
        intbl.ECESS_PERCENTAGE = ECESS_PERCENTAGE
        intbl.HCESS_ACTUAL = HCESS_ACTUAL
        intbl.HCESS_EXPECTED = HCESS_EXPECTED
        intbl.HCESS_PERCENTAGE = HCESS_PERCENTAGE
        intbl.AED_ACTUAL = AED_ACTUAL
        intbl.AED_EXPECTED = AED_EXPECTED
        intbl.AED_PERCENTAGE = AED_Percentage
        intbl.GRAND_TOTAL_ACTUAL = GRAND_TOTAL_ACTUAL
        intbl.GRAND_TOTAL_EXPECTED = Grand_Total_Expected
        intbl.VAT_EXPECTED = VAT_EXPECTED
        intbl.FREIGHT_EXPECTED = FREIGHT_EXPECTED
        intbl.PKG_FOR_EXPECTED = PKG_FOR_EXPECTED
        intbl.DISCOUNT_EXPECTED = DISCOUNT_Expected
        intbl.VAT_PERCENTAGE = VAT_PERCENTAGE
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        DocID = DBNulls.StringValue(intbl.DOCID)
        PO_DocId = DBNulls.StringValue(intbl.PO_DOCID)
        Sr_number = DBNulls.StringValue(intbl.SR_NUMBER)
        Material_Code = DBNulls.StringValue(intbl.MATERIAL_CODE)
        Description = DBNulls.StringValue(intbl.DESCRIPTION)
        Qty = DBNulls.NumberValue(intbl.QTY)
        UOM = DBNulls.StringValue(intbl.UOM)
        Batch_Number = DBNulls.StringValue(intbl.BATCH_NUMBER)
        Stor_Loc = DBNulls.StringValue(intbl.STOR_LOC)
        Plant_Code = DBNulls.StringValue(intbl.PLANT_CODE)
        PO_QTY = DBNulls.NumberValue(intbl.PO_QTY)
        GR_QTY = DBNulls.NumberValue(intbl.GR_QTY)
        OPEN_QTY = DBNulls.NumberValue(intbl.OPEN_QTY)
        SCHEDULED_QTY = DBNulls.NumberValue(intbl.SCHEDULED_QTY)
        IN_TRANSIT_QTY = DBNulls.NumberValue(intbl.IN_TRANSIT_QTY)
        ALLOWED_QTY = DBNulls.NumberValue(intbl.ALLOWED_QTY)
        RATE_ACTUAL = DBNulls.NumberValue(intbl.RATE_ACTUAL)
        RATE_EXPECTED = DBNulls.NumberValue(intbl.RATE_EXPECTED)
        BASE_VALUE_ACTUAL = DBNulls.NumberValue(intbl.BASE_VALUE_ACTUAL)
        BASE_VALUE_EXPECTED = DBNulls.NumberValue(intbl.BASE_VALUE_EXPECTED)
        ASSESEBL_VALUE_ACTUAL = DBNulls.NumberValue(intbl.ASSESEBL_VALUE_ACTUAL)
        ASSESEBL_VALUE_EXPECTED = DBNulls.NumberValue(intbl.ASSESEBL_VALUE_EXPECTED)
        BED_ACTUAL = DBNulls.NumberValue(intbl.BED_ACTUAL)
        BED_EXPECTED = DBNulls.NumberValue(intbl.BED_EXPECTED)
        BED_PERCENTAGE = DBNulls.NumberValue(intbl.BED_PERCENTAGE)
        ECESS_ACTUAL = DBNulls.NumberValue(intbl.ECESS_ACTUAL)
        ECESS_EXPECTED = DBNulls.NumberValue(intbl.ECESS_EXPECTED)
        ECESS_PERCENTAGE = DBNulls.NumberValue(intbl.ECESS_PERCENTAGE)
        HCESS_ACTUAL = DBNulls.NumberValue(intbl.HCESS_ACTUAL)
        HCESS_EXPECTED = DBNulls.NumberValue(intbl.HCESS_EXPECTED)
        HCESS_PERCENTAGE = DBNulls.NumberValue(intbl.HCESS_PERCENTAGE)
        AED_ACTUAL = DBNulls.NumberValue(intbl.AED_ACTUAL)
        AED_EXPECTED = DBNulls.NumberValue(intbl.AED_EXPECTED)
        AED_Percentage = DBNulls.NumberValue(intbl.AED_PERCENTAGE)
        GRAND_TOTAL_ACTUAL = DBNulls.NumberValue(intbl.GRAND_TOTAL_ACTUAL)
        Grand_Total_Expected = DBNulls.NumberValue(intbl.GRAND_TOTAL_EXPECTED)
        VAT_EXPECTED = DBNulls.NumberValue(intbl.VAT_EXPECTED)
        FREIGHT_EXPECTED = DBNulls.NumberValue(intbl.FREIGHT_EXPECTED)
        PKG_FOR_EXPECTED = DBNulls.NumberValue(intbl.PKG_FOR_EXPECTED)
        DISCOUNT_Expected = DBNulls.NumberValue(intbl.DISCOUNT_EXPECTED)
        VAT_PERCENTAGE = DBNulls.NumberValue(intbl.VAT_PERCENTAGE)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "PO_DOCID" Then
            Return PO_DocId
        End If
        If UCase(inFieldName) = "SR_NUMBER" Then
            Return Sr_number
        End If
        If UCase(inFieldName) = "MATERIAL_CODE" Then
            Return Material_Code
        End If
        If UCase(inFieldName) = "DESCRIPTION" Then
            Return Description
        End If
        If UCase(inFieldName) = "QTY" Then
            Return Qty
        End If
        If UCase(inFieldName) = "UOM" Then
            Return UOM
        End If
        If UCase(inFieldName) = "BATCH_NUMBER" Then
            Return Batch_Number
        End If
        If UCase(inFieldName) = "STOR_LOC" Then
            Return Stor_Loc
        End If
        If UCase(inFieldName) = "PLANT_CODE" Then
            Return Plant_Code
        End If
        If UCase(inFieldName) = "PO_QTY" Then
            Return PO_Qty
        End If
        If UCase(inFieldName) = "GR_QTY" Then
            Return GR_Qty
        End If
        If UCase(inFieldName) = "OPEN_QTY" Then
            Return Open_Qty
        End If
        If UCase(inFieldName) = "SCHEDULED_QTY" Then
            Return Scheduled_Qty
        End If
        If UCase(inFieldName) = "IN_TRANSIT_QTY" Then
            Return In_Transit_Qty
        End If
        If UCase(inFieldName) = "ALLOWED_QTY" Then
            Return Allowed_Qty
        End If
        If UCase(inFieldName) = "RATE_ACTUAL" Then
            Return Rate_Actual
        End If
        If UCase(inFieldName) = "RATE_EXPECTED" Then
            Return Rate_Expected
        End If
        If UCase(inFieldName) = "BASE_VALUE_ACTUAL" Then
            Return Base_Value_Actual
        End If
        If UCase(inFieldName) = "BASE_VALUE_EXPECTED" Then
            Return Base_Value_Expected
        End If
        If UCase(inFieldName) = "ASSESEBL_VALUE_ACTUAL" Then
            Return Assesebl_Value_Actual
        End If
        If UCase(inFieldName) = "ASSESEBL_VALUE_EXPECTED" Then
            Return Assesebl_Value_Expected
        End If
        If UCase(inFieldName) = "BED_ACTUAL" Then
            Return BED_Actual
        End If
        If UCase(inFieldName) = "BED_EXPECTED" Then
            Return BED_Expected
        End If
        If UCase(inFieldName) = "BED_PERCENTAGE" Then
            Return BED_Percentage
        End If
        If UCase(inFieldName) = "ECESS_ACTUAL" Then
            Return ECESS_Actual
        End If
        If UCase(inFieldName) = "ECESS_EXPECTED" Then
            Return ECESS_Expected
        End If
        If UCase(inFieldName) = "ECESS_PERCENTAGE" Then
            Return ECESS_Percentage
        End If
        If UCase(inFieldName) = "HCESS_ACTUAL" Then
            Return HCESS_Actual
        End If
        If UCase(inFieldName) = "HCESS_EXPECTED" Then
            Return HCESS_Expected
        End If
        If UCase(inFieldName) = "HCESS_PERCENTAGE" Then
            Return HCESS_Percentage
        End If
        If UCase(inFieldName) = "AED_ACTUAL" Then
            Return AED_Actual
        End If
        If UCase(inFieldName) = "AED_EXPECTED" Then
            Return AED_Expected
        End If
        If UCase(inFieldName) = "AED_PERCENTAGE" Then
            Return AED_Percentage
        End If
        If UCase(inFieldName) = "GRAND_TOTAL_ACTUAL" Then
            Return Grand_Total_Actual
        End If
        If UCase(inFieldName) = "GRAND_TOTAL_EXPECTED" Then
            Return Grand_Total_Expected
        End If
        If UCase(inFieldName) = "VAT_EXPECTED" Then
            Return VAT_Expected
        End If
        If UCase(inFieldName) = "FREIGHT_EXPECTED" Then
            Return FREIGHT_Expected
        End If
        If UCase(inFieldName) = "PKG_FOR_EXPECTED" Then
            Return PKG_FOR_Expected
        End If
        If UCase(inFieldName) = "DISCOUNT_EXPECTED" Then
            Return DISCOUNT_Expected
        End If
        If UCase(inFieldName) = "VAT_PERCENTAGE" Then
            Return VAT_Percentage
        End If
        Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_DocId As String
    Property PO_DocId() As String
        Get
            Return vPO_DocId
        End Get
        Set(ByVal value As String)
            vPO_DocId = value
            If vPO_DocId > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vSr_number As String
    Property Sr_number() As String
        Get
            Return vSr_number
        End Get
        Set(ByVal value As String)
            vSr_number = value
            If vSr_number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vMaterial_Code As String
    Property Material_Code() As String
        Get
            Return vMaterial_Code
        End Get
        Set(ByVal value As String)
            vMaterial_Code = value
            If vMaterial_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDescription As String
    Property Description() As String
        Get
            Return vDescription
        End Get
        Set(ByVal value As String)
            vDescription = value
            If vDescription > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vQty As Double
    Property Qty() As Double
        Get
            Return vQty
        End Get
        Set(ByVal value As Double)
            vQty = value
            If vQty > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vUOM As String
    Property UOM() As String
        Get
            Return vUOM
        End Get
        Set(ByVal value As String)
            vUOM = value
            If vUOM > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vBatch_Number As String
    Property Batch_Number() As String
        Get
            Return vBatch_Number
        End Get
        Set(ByVal value As String)
            vBatch_Number = value
            If vBatch_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vStor_Loc As String
    Property Stor_Loc() As String
        Get
            Return vStor_Loc
        End Get
        Set(ByVal value As String)
            vStor_Loc = value
            If vStor_Loc > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPlant_Code As String
    Property Plant_Code() As String
        Get
            Return vPlant_Code
        End Get
        Set(ByVal value As String)
            vPlant_Code = value
            If vPlant_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_Qty As Double
    Property PO_Qty() As Double
        Get
            Return vPO_Qty
        End Get
        Set(ByVal value As Double)
            vPO_Qty = Value
            If vPO_Qty > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vGR_Qty As Double
    Property GR_Qty() As Double
        Get
            Return vGR_Qty
        End Get
        Set(ByVal value As Double)
            vGR_Qty = Value
            If vGR_Qty > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vOpen_Qty As Double
    Property Open_Qty() As Double
        Get
            Return vOpen_Qty
        End Get
        Set(ByVal value As Double)
            vOpen_Qty = Value
            If vOpen_Qty > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vScheduled_Qty As Double
    Property Scheduled_Qty() As Double
        Get
            Return vScheduled_Qty
        End Get
        Set(ByVal value As Double)
            vScheduled_Qty = Value
            If vScheduled_Qty > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vIn_Transit_Qty As Double
    Property In_Transit_Qty() As Double
        Get
            Return vIn_Transit_Qty
        End Get
        Set(ByVal value As Double)
            vIn_Transit_Qty = Value
            If vIn_Transit_Qty > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vAllowed_Qty As Double
    Property Allowed_Qty() As Double
        Get
            Return vAllowed_Qty
        End Get
        Set(ByVal value As Double)
            vAllowed_Qty = Value
            If vAllowed_Qty > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vRate_Actual As Double
    Property Rate_Actual() As Double
        Get
            Return vRate_Actual
        End Get
        Set(ByVal value As Double)
            vRate_Actual = Value
            If vRate_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vRate_Expected As Double
    Property Rate_Expected() As Double
        Get
            Return vRate_Expected
        End Get
        Set(ByVal value As Double)
            vRate_Expected = Value
            If vRate_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vBase_Value_Actual As Double
    Property Base_Value_Actual() As Double
        Get
            Return vBase_Value_Actual
        End Get
        Set(ByVal value As Double)
            vBase_Value_Actual = Value
            If vBase_Value_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vBase_Value_Expected As Double
    Property Base_Value_Expected() As Double
        Get
            Return vBase_Value_Expected
        End Get
        Set(ByVal value As Double)
            vBase_Value_Expected = Value
            If vBase_Value_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vAssesebl_Value_Actual As Double
    Property Assesebl_Value_Actual() As Double
        Get
            Return vAssesebl_Value_Actual
        End Get
        Set(ByVal value As Double)
            vAssesebl_Value_Actual = Value
            If vAssesebl_Value_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vAssesebl_Value_Expected As Double
    Property Assesebl_Value_Expected() As Double
        Get
            Return vAssesebl_Value_Expected
        End Get
        Set(ByVal value As Double)
            vAssesebl_Value_Expected = Value
            If vAssesebl_Value_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vBED_Actual As Double
    Property BED_Actual() As Double
        Get
            Return vBED_Actual
        End Get
        Set(ByVal value As Double)
            vBED_Actual = Value
            If vBED_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vBED_Expected As Double
    Property BED_Expected() As Double
        Get
            Return vBED_Expected
        End Get
        Set(ByVal value As Double)
            vBED_Expected = Value
            If vBED_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vBED_Percentage As Double
    Property BED_Percentage() As Double
        Get
            Return vBED_Percentage
        End Get
        Set(ByVal value As Double)
            vBED_Percentage = Value
            If vBED_Percentage > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vECESS_Actual As Double
    Property ECESS_Actual() As Double
        Get
            Return vECESS_Actual
        End Get
        Set(ByVal value As Double)
            vECESS_Actual = Value
            If vECESS_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vECESS_Expected As Double
    Property ECESS_Expected() As Double
        Get
            Return vECESS_Expected
        End Get
        Set(ByVal value As Double)
            vECESS_Expected = Value
            If vECESS_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vECESS_Percentage As Double
    Property ECESS_Percentage() As Double
        Get
            Return vECESS_Percentage
        End Get
        Set(ByVal value As Double)
            vECESS_Percentage = Value
            If vECESS_Percentage > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vHCESS_Actual As Double
    Property HCESS_Actual() As Double
        Get
            Return vHCESS_Actual
        End Get
        Set(ByVal value As Double)
            vHCESS_Actual = Value
            If vHCESS_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vHCESS_Expected As Double
    Property HCESS_Expected() As Double
        Get
            Return vHCESS_Expected
        End Get
        Set(ByVal value As Double)
            vHCESS_Expected = Value
            If vHCESS_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vHCESS_Percentage As Double
    Property HCESS_Percentage() As Double
        Get
            Return vHCESS_Percentage
        End Get
        Set(ByVal value As Double)
            vHCESS_Percentage = Value
            If vHCESS_Percentage > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vAED_Actual As Double
    Property AED_Actual() As Double
        Get
            Return vAED_Actual
        End Get
        Set(ByVal value As Double)
            vAED_Actual = Value
            If vAED_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vAED_Expected As Double
    Property AED_Expected() As Double
        Get
            Return vAED_Expected
        End Get
        Set(ByVal value As Double)
            vAED_Expected = Value
            If vAED_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vAED_Percentage As Double
    Property AED_Percentage() As Double
        Get
            Return vAED_Percentage
        End Get
        Set(ByVal value As Double)
            vAED_Percentage = Value
            If vAED_Percentage > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vGrand_Total_Actual As Double
    Property Grand_Total_Actual() As Double
        Get
            Return vGrand_Total_Actual
        End Get
        Set(ByVal value As Double)
            vGrand_Total_Actual = Value
            If vGrand_Total_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vGrand_Total_Expected As Double
    Property Grand_Total_Expected() As Double
        Get
            Return vGrand_Total_Expected
        End Get
        Set(ByVal value As Double)
            vGrand_Total_Expected = Value
            If vGrand_Total_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vVAT_Expected As Double
    Property VAT_Expected() As Double
        Get
            Return vVAT_Expected
        End Get
        Set(ByVal value As Double)
            vVAT_Expected = Value
            If vVAT_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vFREIGHT_Expected As Double
    Property FREIGHT_Expected() As Double
        Get
            Return vFREIGHT_Expected
        End Get
        Set(ByVal value As Double)
            vFREIGHT_Expected = Value
            If vFREIGHT_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vPKG_FOR_Expected As Double
    Property PKG_FOR_Expected() As Double
        Get
            Return vPKG_FOR_Expected
        End Get
        Set(ByVal value As Double)
            vPKG_FOR_Expected = Value
            If vPKG_FOR_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vDISCOUNT_Expected As Double
    Property DISCOUNT_Expected() As Double
        Get
            Return vDISCOUNT_Expected
        End Get
        Set(ByVal value As Double)
            vDISCOUNT_Expected = Value
            If vDISCOUNT_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vVAT_Percentage As Double
    Property VAT_Percentage() As Double
        Get
            Return vVAT_Percentage
        End Get
        Set(ByVal value As Double)
            vVAT_Percentage = Value
            If vVAT_Percentage > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Public Function clone() As DOC_REPO_Log_Details
        Dim retval As New DOC_REPO_Log_Details
        Dim i As Integer
        i = 0
        retval.DocID = DocID
        retval.PO_DocId = PO_DocId
        retval.Sr_number = Sr_number
        retval.Material_Code = Material_Code
        retval.Description = Description
        retval.Qty = Qty
        retval.UOM = UOM
        retval.Batch_Number = Batch_Number
        retval.Stor_Loc = Stor_Loc
        retval.Plant_Code = Plant_Code
        retval.PO_Qty = PO_Qty
        retval.GR_Qty = GR_Qty
        retval.Open_Qty = Open_Qty
        retval.Scheduled_Qty = Scheduled_Qty
        retval.In_Transit_Qty = In_Transit_Qty
        retval.Allowed_Qty = Allowed_Qty
        retval.Rate_Actual = Rate_Actual
        retval.Rate_Expected = Rate_Expected
        retval.Base_Value_Actual = Base_Value_Actual
        retval.Base_Value_Expected = Base_Value_Expected
        retval.Assesebl_Value_Actual = Assesebl_Value_Actual
        retval.Assesebl_Value_Expected = Assesebl_Value_Expected
        retval.BED_Actual = BED_Actual
        retval.BED_Expected = BED_Expected
        retval.BED_Percentage = BED_Percentage
        retval.ECESS_Actual = ECESS_Actual
        retval.ECESS_Expected = ECESS_Expected
        retval.ECESS_Percentage = ECESS_Percentage
        retval.HCESS_Actual = HCESS_Actual
        retval.HCESS_Expected = HCESS_Expected
        retval.HCESS_Percentage = HCESS_Percentage
        retval.AED_Actual = AED_Actual
        retval.AED_Expected = AED_Expected
        retval.AED_Percentage = AED_Percentage
        retval.Grand_Total_Actual = Grand_Total_Actual
        retval.Grand_Total_Expected = Grand_Total_Expected
        retval.VAT_Expected = VAT_Expected
        retval.FREIGHT_Expected = FREIGHT_Expected
        retval.PKG_FOR_Expected = PKG_FOR_Expected
        retval.DISCOUNT_Expected = DISCOUNT_Expected
        retval.VAT_Percentage = VAT_Percentage
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        DocID = ""
        PO_DocId = ""
        Sr_number = ""
        Material_Code = ""
        Description = ""
        Qty = 0
        UOM = ""
        Batch_Number = ""
        Stor_Loc = ""
        Plant_Code = ""
        PO_Qty = 0
        GR_Qty = 0
        Open_Qty = 0
        Scheduled_Qty = 0
        In_Transit_Qty = 0
        Allowed_Qty = 0
        Rate_Actual = 0
        Rate_Expected = 0
        Base_Value_Actual = 0
        Base_Value_Expected = 0
        Assesebl_Value_Actual = 0
        Assesebl_Value_Expected = 0
        BED_Actual = 0
        BED_Expected = 0
        BED_Percentage = 0
        ECESS_Actual = 0
        ECESS_Expected = 0
        ECESS_Percentage = 0
        HCESS_Actual = 0
        HCESS_Expected = 0
        HCESS_Percentage = 0
        AED_Actual = 0
        AED_Expected = 0
        AED_Percentage = 0
        Grand_Total_Actual = 0
        Grand_Total_Expected = 0
        VAT_Expected = 0
        FREIGHT_Expected = 0
        PKG_FOR_Expected = 0
        DISCOUNT_Expected = 0
        VAT_Percentage = 0
    End Sub
End Class



Public Class DOC_REPO_Log_Master
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.DOCID = DocID
        intbl.VENDOR_CODE = Vendor_Code
        intbl.DOC_DATE = Doc_Date
        intbl.DOC_DATE_RTC = Doc_Date_RTC
        intbl.PO_NUMBER = PO_Number
        intbl.PO_DATE = PO_Date
        intbl.USER_CREATED = User_Created
        intbl.USER_REPRINTED = User_Reprinted
        intbl.CREATED_DATETIME = Created_DateTime
        intbl.MODIFIED_DATETIME = Modified_DateTime
        intbl.ASN_NUMBER = ASN_Number
        intbl.DELIVERY_NOTE_NUMBER = Delivery_note_number
        intbl.VENDOR_NAME = Vendor_Name
        intbl.IS_CANCELLED = Is_Cancelled
        intbl.CANCELLED_BY = Cancelled_By
        intbl.CANCELLATION_DATE = Cancellation_Date
        intbl.CANCELLATION_DATE_RTC = Cancellation_Date_RTC
        intbl.INVOICE_AMOUNT_ACTUAL = INVOICE_AMOUNT_ACTUAL
        intbl.INVOICE_AMOUNT_EXPECTED = Invoice_Amount_Expected
        intbl.VEHICLE_NO = VEHICLE_NO
        intbl.TRANSPORTER_NAME = Transporter_Name
        intbl.PACKING_DETAILS = PACKING_DETAILS
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        DocID = DBNulls.StringValue(intbl.DOCID)
        Vendor_Code = DBNulls.StringValue(intbl.VENDOR_CODE)
        Doc_Date = (intbl.DOC_DATE)
        Doc_Date_RTC = DBNulls.StringValue(intbl.DOC_DATE_RTC)
        PO_Number = DBNulls.StringValue(intbl.PO_NUMBER)
        PO_Date = (intbl.PO_DATE)
        User_Created = DBNulls.StringValue(intbl.USER_CREATED)
        User_Reprinted = DBNulls.StringValue(intbl.USER_REPRINTED)
        Created_DateTime = DBNulls.StringValue(intbl.CREATED_DATETIME)
        Modified_DateTime = DBNulls.StringValue(intbl.MODIFIED_DATETIME)
        ASN_Number = DBNulls.StringValue(intbl.ASN_NUMBER)
        Delivery_note_number = DBNulls.StringValue(intbl.DELIVERY_NOTE_NUMBER)
        Vendor_Name = DBNulls.StringValue(intbl.VENDOR_NAME)
        Is_Cancelled = DBNulls.StringValue(intbl.IS_CANCELLED)
        Cancelled_By = DBNulls.StringValue(intbl.CANCELLED_BY)
        Cancellation_Date = (intbl.CANCELLATION_DATE)
        Cancellation_Date_RTC = DBNulls.NumberValue(intbl.CANCELLATION_DATE_RTC)
        INVOICE_AMOUNT_ACTUAL = DBNulls.NumberValue(intbl.INVOICE_AMOUNT_ACTUAL)
        Invoice_Amount_Expected = DBNulls.NumberValue(intbl.INVOICE_AMOUNT_EXPECTED)
        VEHICLE_NO = DBNulls.StringValue(intbl.VEHICLE_NO)
        Transporter_Name = DBNulls.StringValue(intbl.TRANSPORTER_NAME)
        PACKING_DETAILS = DBNulls.StringValue(intbl.PACKING_DETAILS)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "VENDOR_CODE" Then
            Return Vendor_Code
        End If
        If UCase(inFieldName) = "DOC_DATE" Then
            Return Doc_Date
        End If
        If UCase(inFieldName) = "DOC_DATE_RTC" Then
            Return Doc_Date_RTC
        End If
        If UCase(inFieldName) = "PO_NUMBER" Then
            Return PO_Number
        End If
        If UCase(inFieldName) = "PO_DATE" Then
            Return PO_Date
        End If
        If UCase(inFieldName) = "USER_CREATED" Then
            Return User_Created
        End If
        If UCase(inFieldName) = "USER_REPRINTED" Then
            Return User_Reprinted
        End If
        If UCase(inFieldName) = "CREATED_DATETIME" Then
            Return Created_DateTime
        End If
        If UCase(inFieldName) = "MODIFIED_DATETIME" Then
            Return Modified_DateTime
        End If
        If UCase(inFieldName) = "ASN_NUMBER" Then
            Return ASN_Number
        End If
        If UCase(inFieldName) = "DELIVERY_NOTE_NUMBER" Then
            Return Delivery_note_number
        End If
        If UCase(inFieldName) = "VENDOR_NAME" Then
            Return Vendor_Name
        End If
        If UCase(inFieldName) = "IS_CANCELLED" Then
            Return Is_Cancelled
        End If
        If UCase(inFieldName) = "CANCELLED_BY" Then
            Return Cancelled_By
        End If
        If UCase(inFieldName) = "CANCELLATION_DATE" Then
            Return Cancellation_Date
        End If
        If UCase(inFieldName) = "CANCELLATION_DATE_RTC" Then
            Return Cancellation_Date_RTC
        End If
        If UCase(inFieldName) = "INVOICE_AMOUNT_ACTUAL" Then
            Return Invoice_Amount_Actual
        End If
        If UCase(inFieldName) = "INVOICE_AMOUNT_EXPECTED" Then
            Return Invoice_Amount_Expected
        End If
        If UCase(inFieldName) = "VEHICLE_NO" Then
            Return Vehicle_No
        End If
        If UCase(inFieldName) = "TRANSPORTER_NAME" Then
            Return Transporter_Name
        End If
        If UCase(inFieldName) = "PACKING_DETAILS" Then
            Return Packing_Details
          

            End If

            Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vVendor_Code As String
    Property Vendor_Code() As String
        Get
            Return vVendor_Code
        End Get
        Set(ByVal value As String)
            vVendor_Code = value
            If vVendor_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDoc_Date As Date
    Property Doc_Date() As Date
        Get
            Return vDoc_Date
        End Get
        Set(ByVal value As Date)
            vDoc_Date = value
        End Set
    End Property
    Dim vDoc_Date_RTC As String
    Property Doc_Date_RTC() As String
        Get
            Return vDoc_Date_RTC
        End Get
        Set(ByVal value As String)
            vDoc_Date_RTC = value
            If vDoc_Date_RTC > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_Number As String
    Property PO_Number() As String
        Get
            Return vPO_Number
        End Get
        Set(ByVal value As String)
            vPO_Number = value
            If vPO_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_Date As Date
    Property PO_Date() As Date
        Get
            Return vPO_Date
        End Get
        Set(ByVal value As Date)
            vPO_Date = value
        End Set
    End Property
    Dim vUser_Created As String
    Property User_Created() As String
        Get
            Return vUser_Created
        End Get
        Set(ByVal value As String)
            vUser_Created = value
            If vUser_Created > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vUser_Reprinted As String
    Property User_Reprinted() As String
        Get
            Return vUser_Reprinted
        End Get
        Set(ByVal value As String)
            vUser_Reprinted = value
            If vUser_Reprinted > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCreated_DateTime As String
    Property Created_DateTime() As String
        Get
            Return vCreated_DateTime
        End Get
        Set(ByVal value As String)
            vCreated_DateTime = value
            If vCreated_DateTime > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vModified_DateTime As String
    Property Modified_DateTime() As String
        Get
            Return vModified_DateTime
        End Get
        Set(ByVal value As String)
            vModified_DateTime = value
            If vModified_DateTime > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vASN_Number As String
    Property ASN_Number() As String
        Get
            Return vASN_Number
        End Get
        Set(ByVal value As String)
            vASN_Number = value
            If vASN_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDelivery_note_number As String
    Property Delivery_note_number() As String
        Get
            Return vDelivery_note_number
        End Get
        Set(ByVal value As String)
            vDelivery_note_number = value
            If vDelivery_note_number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vVendor_Name As String
    Property Vendor_Name() As String
        Get
            Return vVendor_Name
        End Get
        Set(ByVal value As String)
            vVendor_Name = value
            If vVendor_Name > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vIs_Cancelled As String
    Property Is_Cancelled() As String
        Get
            Return vIs_Cancelled
        End Get
        Set(ByVal value As String)
            vIs_Cancelled = value
            If vIs_Cancelled > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCancelled_By As String
    Property Cancelled_By() As String
        Get
            Return vCancelled_By
        End Get
        Set(ByVal value As String)
            vCancelled_By = value
            If vCancelled_By > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCancellation_Date As Date
    Property Cancellation_Date() As Date
        Get
            Return vCancellation_Date
        End Get
        Set(ByVal value As Date)
            vCancellation_Date = value
        End Set
    End Property
    Dim vCancellation_Date_RTC As Double
    Property Cancellation_Date_RTC() As Double
        Get
            Return vCancellation_Date_RTC
        End Get
        Set(ByVal value As Double)
            vCancellation_Date_RTC = value
            If vCancellation_Date_RTC > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vInvoice_Amount_Actual As Double
    Property Invoice_Amount_Actual() As Double
        Get
            Return vInvoice_Amount_Actual
        End Get
        Set(ByVal value As Double)
            vInvoice_Amount_Actual = Value
            If vInvoice_Amount_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vInvoice_Amount_Expected As Double
    Property Invoice_Amount_Expected() As Double
        Get
            Return vInvoice_Amount_Expected
        End Get
        Set(ByVal value As Double)
            vInvoice_Amount_Expected = Value
            If vInvoice_Amount_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vPacking_Details As String
    Property Packing_Details() As String
        Get
            Return vPacking_Details
        End Get
        Set(ByVal value As String)
            vPacking_Details = Value
            If vPacking_Details > " " Then
                is_Filled = True
            End If
        End Set
    End Property
    Public vtbl_REPO_Log_DetailsArray(50) As DOC_REPO_Log_Details
    Property tbl_REPO_Log_DetailsArray(ByVal i As Integer) As DOC_REPO_Log_Details
        Get
            If vtbl_REPO_Log_DetailsArray(i) Is Nothing Then
                vtbl_REPO_Log_DetailsArray(i) = New DOC_REPO_Log_Details
            End If
            Return vtbl_REPO_Log_DetailsArray(i)
        End Get
        Set(ByVal value As DOC_REPO_Log_Details)
            vtbl_REPO_Log_DetailsArray(i) = value
        End Set
    End Property
    Dim vVehicle_No As String
    Property Vehicle_No() As String
        Get
            Return vVehicle_No
        End Get
        Set(ByVal value As String)
            vVehicle_No = Value
            If vVehicle_No > " " Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vTransporter_Name As String
    Property Transporter_Name() As String
        Get
            Return vTransporter_Name
        End Get
        Set(ByVal value As String)
            vTransporter_Name = Value
            If vTransporter_Name > " " Then
                is_Filled = True
            End If
        End Set
    End Property
    Public Function clone() As DOC_REPO_Log_Master
        Dim retval As New DOC_REPO_Log_Master
        Dim i As Integer
        i = 0
        retval.DocID = DocID
        retval.Vendor_Code = Vendor_Code
        retval.Doc_Date = Doc_Date
        retval.Doc_Date_RTC = Doc_Date_RTC
        retval.PO_Number = PO_Number
        retval.PO_Date = PO_Date
        retval.User_Created = User_Created
        retval.User_Reprinted = User_Reprinted
        retval.Created_DateTime = Created_DateTime
        retval.Modified_DateTime = Modified_DateTime
        retval.ASN_Number = ASN_Number
        retval.Delivery_note_number = Delivery_note_number
        retval.Vendor_Name = Vendor_Name
        retval.Is_Cancelled = Is_Cancelled
        retval.Cancelled_By = Cancelled_By
        retval.Cancellation_Date = Cancellation_Date
        retval.Cancellation_Date_RTC = Cancellation_Date_RTC
        retval.Invoice_Amount_Actual = Invoice_Amount_Actual
        retval.Invoice_Amount_Expected = Invoice_Amount_Expected
        retval.Vehicle_No = Vehicle_No
        retval.Transporter_Name = Transporter_Name
        retval.Packing_Details = Packing_Details
        For i = 0 To 50 - 1
            retval.tbl_REPO_Log_DetailsArray(i) = tbl_REPO_Log_DetailsArray(i).clone
        Next i
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        DocID = ""
        Vendor_Code = ""
        Doc_Date = CDate("01/Jan/1950")
        Doc_Date_RTC = ""
        PO_Number = ""
        PO_Date = CDate("01/Jan/1950")
        User_Created = ""
        User_Reprinted = ""
        Created_DateTime = ""
        Modified_DateTime = ""
        ASN_Number = ""
        Delivery_note_number = ""
        Vendor_Name = ""
        Is_Cancelled = ""
        Cancelled_By = ""
        Cancellation_Date = CDate("01/Jan/1950")
        Cancellation_Date_RTC = 0
        Invoice_Amount_Actual = 0
        Invoice_Amount_Expected = 0
        Vehicle_No = ""
        Transporter_Name = ""
        Packing_Details = ""
        For i = 0 To 50 - 1
            tbl_REPO_Log_DetailsArray(i).Blank()
        Next i
    End Sub
    Public Sub Remove_tbl_REPO_Log_DetailsArray(ByVal In_Index As Integer)
        tbl_REPO_Log_DetailsArray(In_Index).Blank()
        For i = In_Index To 50 - 2
            tbl_REPO_Log_DetailsArray(i) = tbl_REPO_Log_DetailsArray(i + 1).clone
        Next i
        tbl_REPO_Log_DetailsArray(50 - 1).Blank()
    End Sub
    Public Function tbl_REPO_Log_Details_Count() As Integer
        For I = 0 To 50 - 1
            If tbl_REPO_Log_DetailsArray(I).Is_Filled Then
            Else
                Return I
            End If
        Next
        Return 50
    End Function
    Public Function tbl_REPO_Log_Details_SUM(ByVal inField As String) As Double
        Dim vvAL As Double
        vvAL = 0
        For i = 0 To tbl_REPO_Log_Details_Count() - 1
            vvAL = vvAL + Val(tbl_REPO_Log_DetailsArray(i).FieldValue(inField))
        Next i
        Return vvAL
    End Function

    Public Function tbl_REPO_Log_Details_Array_ByFieldValue(ByVal inDoc As DOC_REPO_Log_Master, ByVal inFieldName As String, ByVal inValue As Object) As DOC_REPO_Log_Details
        For i = 0 To 50 - 1
            If inDoc.tbl_REPO_Log_DetailsArray(i).FieldValue(inFieldName).ToString.Trim.ToUpper = inValue.ToString.Trim.ToUpper Then
                Return inDoc.tbl_REPO_Log_DetailsArray(i)
            End If
        Next
        Return Nothing
    End Function

End Class



Public Class DOC_SCPO_Log_Child_Details
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.DOCID = DocID
        intbl.PARENT_DOCID = Parent_DocId
        intbl.SR_NUMBER = Sr_number
        intbl.MATERIAL_CODE = Material_Code
        intbl.DESCRIPTION = Description
        intbl.QTY = Qty
        intbl.UOM = UOM
        intbl.BATCH_NUMBER = Batch_Number
        intbl.STOR_LOC = Stor_Loc
        intbl.PLANT_CODE = Plant_Code
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        DocID = DBNulls.StringValue(intbl.DOCID)
        Parent_DocId = DBNulls.StringValue(intbl.PARENT_DOCID)
        Sr_number = DBNulls.StringValue(intbl.SR_NUMBER)
        Material_Code = DBNulls.StringValue(intbl.MATERIAL_CODE)
        Description = DBNulls.StringValue(intbl.DESCRIPTION)
        Qty = DBNulls.NumberValue(intbl.QTY)
        UOM = DBNulls.StringValue(intbl.UOM)
        Batch_Number = DBNulls.StringValue(intbl.BATCH_NUMBER)
        Stor_Loc = DBNulls.StringValue(intbl.STOR_LOC)
        Plant_Code = DBNulls.StringValue(intbl.PLANT_CODE)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "PARENT_DOCID" Then
            Return Parent_DocId
        End If
        If UCase(inFieldName) = "SR_NUMBER" Then
            Return Sr_number
        End If
        If UCase(inFieldName) = "MATERIAL_CODE" Then
            Return Material_Code
        End If
        If UCase(inFieldName) = "DESCRIPTION" Then
            Return Description
        End If
        If UCase(inFieldName) = "QTY" Then
            Return Qty
        End If
        If UCase(inFieldName) = "UOM" Then
            Return UOM
        End If
        If UCase(inFieldName) = "BATCH_NUMBER" Then
            Return Batch_Number
        End If
        If UCase(inFieldName) = "STOR_LOC" Then
            Return Stor_Loc
        End If
        If UCase(inFieldName) = "PLANT_CODE" Then
            Return Plant_Code
        End If
        Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vParent_DocId As String
    Property Parent_DocId() As String
        Get
            Return vParent_DocId
        End Get
        Set(ByVal value As String)
            vParent_DocId = value
            If vParent_DocId > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vSr_number As String
    Property Sr_number() As String
        Get
            Return vSr_number
        End Get
        Set(ByVal value As String)
            vSr_number = value
            If vSr_number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vMaterial_Code As String
    Property Material_Code() As String
        Get
            Return vMaterial_Code
        End Get
        Set(ByVal value As String)
            vMaterial_Code = value
            If vMaterial_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDescription As String
    Property Description() As String
        Get
            Return vDescription
        End Get
        Set(ByVal value As String)
            vDescription = value
            If vDescription > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vQty As Double
    Property Qty() As Double
        Get
            Return vQty
        End Get
        Set(ByVal value As Double)
            vQty = value
            If vQty > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vUOM As String
    Property UOM() As String
        Get
            Return vUOM
        End Get
        Set(ByVal value As String)
            vUOM = value
            If vUOM > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vBatch_Number As String
    Property Batch_Number() As String
        Get
            Return vBatch_Number
        End Get
        Set(ByVal value As String)
            vBatch_Number = value
            If vBatch_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vStor_Loc As String
    Property Stor_Loc() As String
        Get
            Return vStor_Loc
        End Get
        Set(ByVal value As String)
            vStor_Loc = value
            If vStor_Loc > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPlant_Code As String
    Property Plant_Code() As String
        Get
            Return vPlant_Code
        End Get
        Set(ByVal value As String)
            vPlant_Code = value
            If vPlant_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Public Function clone() As DOC_SCPO_Log_Child_Details
        Dim retval As New DOC_SCPO_Log_Child_Details
        Dim i As Integer
        i = 0
        retval.DocID = DocID
        retval.Parent_DocId = Parent_DocId
        retval.Sr_number = Sr_number
        retval.Material_Code = Material_Code
        retval.Description = Description
        retval.Qty = Qty
        retval.UOM = UOM
        retval.Batch_Number = Batch_Number
        retval.Stor_Loc = Stor_Loc
        retval.Plant_Code = Plant_Code
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        DocID = ""
        Parent_DocId = ""
        Sr_number = ""
        Material_Code = ""
        Description = ""
        Qty = 0
        UOM = ""
        Batch_Number = ""
        Stor_Loc = ""
        Plant_Code = ""
    End Sub
End Class



Public Class DOC_SCPO_Log_Master
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.DOCID = DocID
        intbl.VENDOR_CODE = Vendor_Code
        intbl.DOC_DATE = Doc_Date
        intbl.DOC_DATE_RTC = Doc_Date_RTC
        intbl.PO_NUMBER = PO_Number
        intbl.PO_DATE = PO_Date
        intbl.USER_CREATED = User_Created
        intbl.USER_REPRINTED = User_Reprinted
        intbl.CREATED_DATETIME = Created_DateTime
        intbl.MODIFIED_DATETIME = Modified_DateTime
        intbl.ASN_NUMBER = ASN_Number
        intbl.DELIVERY_NOTE_NUMBER = Delivery_note_number
        intbl.VENDOR_NAME = Vendor_Name
        intbl.IS_CANCELLED = Is_Cancelled
        intbl.CANCELLED_BY = Cancelled_By
        intbl.CANCELLATION_DATE = Cancellation_Date
        intbl.CANCELLATION_DATE_RTC = Cancellation_Date_RTC
        intbl.VEHICLE_NO = VEHICLE_NO
        intbl.TRANSPORTER_NAME = Transporter_Name
        intbl.PACKING_DETAILS = PACKING_DETAILS
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        DocID = DBNulls.StringValue(intbl.DOCID)
        Vendor_Code = DBNulls.StringValue(intbl.VENDOR_CODE)
        Doc_Date = (intbl.DOC_DATE)
        Doc_Date_RTC = DBNulls.StringValue(intbl.DOC_DATE_RTC)
        PO_Number = DBNulls.StringValue(intbl.PO_NUMBER)
        PO_Date = (intbl.PO_DATE)
        User_Created = DBNulls.StringValue(intbl.USER_CREATED)
        User_Reprinted = DBNulls.StringValue(intbl.USER_REPRINTED)
        Created_DateTime = DBNulls.StringValue(intbl.CREATED_DATETIME)
        Modified_DateTime = DBNulls.StringValue(intbl.MODIFIED_DATETIME)
        ASN_Number = DBNulls.StringValue(intbl.ASN_NUMBER)
        Delivery_note_number = DBNulls.StringValue(intbl.DELIVERY_NOTE_NUMBER)
        Vendor_Name = DBNulls.StringValue(intbl.VENDOR_NAME)
        Is_Cancelled = DBNulls.StringValue(intbl.IS_CANCELLED)
        Cancelled_By = DBNulls.StringValue(intbl.CANCELLED_BY)
        Cancellation_Date = (intbl.CANCELLATION_DATE)
        Cancellation_Date_RTC = DBNulls.NumberValue(intbl.CANCELLATION_DATE_RTC)
        VEHICLE_NO = DBNulls.StringValue(intbl.VEHICLE_NO)
        Transporter_Name = DBNulls.StringValue(intbl.TRANSPORTER_NAME)
        PACKING_DETAILS = DBNulls.StringValue(intbl.PACKING_DETAILS)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "VENDOR_CODE" Then
            Return Vendor_Code
        End If
        If UCase(inFieldName) = "DOC_DATE" Then
            Return Doc_Date
        End If
        If UCase(inFieldName) = "DOC_DATE_RTC" Then
            Return Doc_Date_RTC
        End If
        If UCase(inFieldName) = "PO_NUMBER" Then
            Return PO_Number
        End If
        If UCase(inFieldName) = "PO_DATE" Then
            Return PO_Date
        End If
        If UCase(inFieldName) = "USER_CREATED" Then
            Return User_Created
        End If
        If UCase(inFieldName) = "USER_REPRINTED" Then
            Return User_Reprinted
        End If
        If UCase(inFieldName) = "CREATED_DATETIME" Then
            Return Created_DateTime
        End If
        If UCase(inFieldName) = "MODIFIED_DATETIME" Then
            Return Modified_DateTime
        End If
        If UCase(inFieldName) = "ASN_NUMBER" Then
            Return ASN_Number
        End If
        If UCase(inFieldName) = "DELIVERY_NOTE_NUMBER" Then
            Return Delivery_note_number
        End If
        If UCase(inFieldName) = "VENDOR_NAME" Then
            Return Vendor_Name
        End If
        If UCase(inFieldName) = "IS_CANCELLED" Then
            Return Is_Cancelled
        End If
        If UCase(inFieldName) = "CANCELLED_BY" Then
            Return Cancelled_By
        End If
        If UCase(inFieldName) = "CANCELLATION_DATE" Then
            Return Cancellation_Date
        End If
        If UCase(inFieldName) = "CANCELLATION_DATE_RTC" Then
            Return Cancellation_Date_RTC
        End If
        If UCase(inFieldName) = "VEHICLE_NO" Then
            Return Vehicle_No
        End If
        If UCase(inFieldName) = "TRANSPORTER_NAME" Then
            Return Transporter_Name
        End If
        If UCase(inFieldName) = "PACKING_DETAILS" Then
            Return Packing_Details
        End If
        Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vVendor_Code As String
    Property Vendor_Code() As String
        Get
            Return vVendor_Code
        End Get
        Set(ByVal value As String)
            vVendor_Code = value
            If vVendor_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDoc_Date As Date
    Property Doc_Date() As Date
        Get
            Return vDoc_Date
        End Get
        Set(ByVal value As Date)
            vDoc_Date = value
        End Set
    End Property
    Dim vDoc_Date_RTC As String
    Property Doc_Date_RTC() As String
        Get
            Return vDoc_Date_RTC
        End Get
        Set(ByVal value As String)
            vDoc_Date_RTC = value
            If vDoc_Date_RTC > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_Number As String
    Property PO_Number() As String
        Get
            Return vPO_Number
        End Get
        Set(ByVal value As String)
            vPO_Number = value
            If vPO_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_Date As Date
    Property PO_Date() As Date
        Get
            Return vPO_Date
        End Get
        Set(ByVal value As Date)
            vPO_Date = value
        End Set
    End Property
    Dim vUser_Created As String
    Property User_Created() As String
        Get
            Return vUser_Created
        End Get
        Set(ByVal value As String)
            vUser_Created = value
            If vUser_Created > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vUser_Reprinted As String
    Property User_Reprinted() As String
        Get
            Return vUser_Reprinted
        End Get
        Set(ByVal value As String)
            vUser_Reprinted = value
            If vUser_Reprinted > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCreated_DateTime As String
    Property Created_DateTime() As String
        Get
            Return vCreated_DateTime
        End Get
        Set(ByVal value As String)
            vCreated_DateTime = value
            If vCreated_DateTime > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vModified_DateTime As String
    Property Modified_DateTime() As String
        Get
            Return vModified_DateTime
        End Get
        Set(ByVal value As String)
            vModified_DateTime = value
            If vModified_DateTime > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vASN_Number As String
    Property ASN_Number() As String
        Get
            Return vASN_Number
        End Get
        Set(ByVal value As String)
            vASN_Number = value
            If vASN_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDelivery_note_number As String
    Property Delivery_note_number() As String
        Get
            Return vDelivery_note_number
        End Get
        Set(ByVal value As String)
            vDelivery_note_number = value
            If vDelivery_note_number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vVendor_Name As String
    Property Vendor_Name() As String
        Get
            Return vVendor_Name
        End Get
        Set(ByVal value As String)
            vVendor_Name = value
            If vVendor_Name > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vIs_Cancelled As String
    Property Is_Cancelled() As String
        Get
            Return vIs_Cancelled
        End Get
        Set(ByVal value As String)
            vIs_Cancelled = value
            If vIs_Cancelled > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCancelled_By As String
    Property Cancelled_By() As String
        Get
            Return vCancelled_By
        End Get
        Set(ByVal value As String)
            vCancelled_By = value
            If vCancelled_By > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCancellation_Date As Date
    Property Cancellation_Date() As Date
        Get
            Return vCancellation_Date
        End Get
        Set(ByVal value As Date)
            vCancellation_Date = value
        End Set
    End Property
    Dim vCancellation_Date_RTC As Double
    Property Cancellation_Date_RTC() As Double
        Get
            Return vCancellation_Date_RTC
        End Get
        Set(ByVal value As Double)
            vCancellation_Date_RTC = value
            If vCancellation_Date_RTC > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vVehicle_No As String
    Property Vehicle_No() As String
        Get
            Return vVehicle_No
        End Get
        Set(ByVal value As String)
            vVehicle_No = Value
            If vVehicle_No > " " Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vTransporter_Name As String
    Property Transporter_Name() As String
        Get
            Return vTransporter_Name
        End Get
        Set(ByVal value As String)
            vTransporter_Name = Value
            If vTransporter_Name > " " Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vPacking_Details As String
    Property Packing_Details() As String
        Get
            Return vPacking_Details
        End Get
        Set(ByVal value As String)
            vPacking_Details = Value
            If vPacking_Details > " " Then
                is_Filled = True
            End If
        End Set
    End Property
    Public vtbl_SCPO_Log_Parent_DetailsArray(50) As DOC_SCPO_Log_Parent_Details
    Property tbl_SCPO_Log_Parent_DetailsArray(ByVal i As Integer) As DOC_SCPO_Log_Parent_Details
        Get
            If vtbl_SCPO_Log_Parent_DetailsArray(i) Is Nothing Then
                vtbl_SCPO_Log_Parent_DetailsArray(i) = New DOC_SCPO_Log_Parent_Details
            End If
            Return vtbl_SCPO_Log_Parent_DetailsArray(i)
        End Get
        Set(ByVal value As DOC_SCPO_Log_Parent_Details)
            vtbl_SCPO_Log_Parent_DetailsArray(i) = value
        End Set
    End Property
    Public Function clone() As DOC_SCPO_Log_Master
        Dim retval As New DOC_SCPO_Log_Master
        Dim i As Integer
        i = 0
        retval.DocID = DocID
        retval.Vendor_Code = Vendor_Code
        retval.Doc_Date = Doc_Date
        retval.Doc_Date_RTC = Doc_Date_RTC
        retval.PO_Number = PO_Number
        retval.PO_Date = PO_Date
        retval.User_Created = User_Created
        retval.User_Reprinted = User_Reprinted
        retval.Created_DateTime = Created_DateTime
        retval.Modified_DateTime = Modified_DateTime
        retval.ASN_Number = ASN_Number
        retval.Delivery_note_number = Delivery_note_number
        retval.Vendor_Name = Vendor_Name
        retval.Is_Cancelled = Is_Cancelled
        retval.Cancelled_By = Cancelled_By
        retval.Cancellation_Date = Cancellation_Date
        retval.Cancellation_Date_RTC = Cancellation_Date_RTC
        retval.Vehicle_No = Vehicle_No
        retval.Transporter_Name = Transporter_Name
        retval.Packing_Details = Packing_Details
        For i = 0 To 50 - 1
            retval.tbl_SCPO_Log_Parent_DetailsArray(i) = tbl_SCPO_Log_Parent_DetailsArray(i).clone
        Next i
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        DocID = ""
        Vendor_Code = ""
        Doc_Date = CDate("01/Jan/1950")
        Doc_Date_RTC = ""
        PO_Number = ""
        PO_Date = CDate("01/Jan/1950")
        User_Created = ""
        User_Reprinted = ""
        Created_DateTime = ""
        Modified_DateTime = ""
        ASN_Number = ""
        Delivery_note_number = ""
        Vendor_Name = ""
        Is_Cancelled = ""
        Cancelled_By = ""
        Cancellation_Date = CDate("01/Jan/1950")
        Cancellation_Date_RTC = 0
        Vehicle_No = ""
        Transporter_Name = ""
        For i = 0 To 50 - 1
            tbl_SCPO_Log_Parent_DetailsArray(i).Blank()
        Next i
    End Sub
    Public Sub Remove_tbl_SCPO_Log_Parent_DetailsArray(ByVal In_Index As Integer)
        tbl_SCPO_Log_Parent_DetailsArray(In_Index).Blank()
        For i = In_Index To 50 - 2
            tbl_SCPO_Log_Parent_DetailsArray(i) = tbl_SCPO_Log_Parent_DetailsArray(i + 1).clone
        Next i
        tbl_SCPO_Log_Parent_DetailsArray(50 - 1).Blank()
    End Sub
    Public Function tbl_SCPO_Log_Parent_Details_Count() As Integer
        For I = 0 To 50 - 1
            If tbl_SCPO_Log_Parent_DetailsArray(I).Is_Filled Then
            Else
                Return I
            End If
        Next
        Return 50
    End Function
    Public Function tbl_SCPO_Log_Parent_Details_SUM(ByVal inField As String) As Double
        Dim vvAL As Double
        vvAL = 0
        For i = 0 To tbl_SCPO_Log_Parent_Details_Count() - 1
            vvAL = vvAL + Val(tbl_SCPO_Log_Parent_DetailsArray(i).FieldValue(inField))
        Next i
        Return vvAL
    End Function

    Public Function tbl_SCPO_Log_Parent_Details_Array_ByFieldValue(ByVal inDoc As DOC_SCPO_Log_Master, ByVal inFieldName As String, ByVal inValue As Object) As DOC_SCPO_Log_Parent_Details
        For i = 0 To 50 - 1
            If inDoc.tbl_SCPO_Log_Parent_DetailsArray(i).FieldValue(inFieldName).ToString.Trim.ToUpper = inValue.ToString.Trim.ToUpper Then
                Return inDoc.tbl_SCPO_Log_Parent_DetailsArray(i)
            End If
        Next
        Return Nothing
    End Function

End Class



Public Class DOC_SCPO_Log_Parent_Details
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.DOCID = DocID
        intbl.PO_DOCID = PO_DocId
        intbl.SR_NUMBER = Sr_number
        intbl.MATERIAL_CODE = Material_Code
        intbl.DESCRIPTION = Description
        intbl.QTY = Qty
        intbl.UOM = UOM
        intbl.BATCH_NUMBER = Batch_Number
        intbl.STOR_LOC = Stor_Loc
        intbl.PLANT_CODE = Plant_Code
        intbl.RATE_ACTUAL = RATE_ACTUAL
        intbl.RATE_EXPECTED = Rate_Expected
        intbl.GRAND_TOTAL_ACTUAL = GRAND_TOTAL_ACTUAL
        intbl.GRAND_TOTAL_EXPECTED = Grand_Total_Expected
        intbl.VAT_EXPECTED = VAT_EXPECTED
        intbl.DISCOUNT_EXPECTED = DISCOUNT_EXPECTED
        intbl.PKG_FOR_EXPECTED = PKG_FOR_EXPECTED
        intbl.FREIGHT_EXPECTED = FREIGHT_EXPECTED
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        DocID = DBNulls.StringValue(intbl.DOCID)
        PO_DocId = DBNulls.StringValue(intbl.PO_DOCID)
        Sr_number = DBNulls.StringValue(intbl.SR_NUMBER)
        Material_Code = DBNulls.StringValue(intbl.MATERIAL_CODE)
        Description = DBNulls.StringValue(intbl.DESCRIPTION)
        Qty = DBNulls.NumberValue(intbl.QTY)
        UOM = DBNulls.StringValue(intbl.UOM)
        Batch_Number = DBNulls.StringValue(intbl.BATCH_NUMBER)
        Stor_Loc = DBNulls.StringValue(intbl.STOR_LOC)
        Plant_Code = DBNulls.StringValue(intbl.PLANT_CODE)
        RATE_ACTUAL = DBNulls.NumberValue(intbl.RATE_ACTUAL)
        Rate_Expected = DBNulls.NumberValue(intbl.RATE_EXPECTED)
        GRAND_TOTAL_ACTUAL = DBNulls.NumberValue(intbl.GRAND_TOTAL_ACTUAL)
        Grand_Total_Expected = DBNulls.NumberValue(intbl.GRAND_TOTAL_EXPECTED)
        VAT_EXPECTED = DBNulls.NumberValue(intbl.VAT_EXPECTED)
        DISCOUNT_EXPECTED = DBNulls.NumberValue(intbl.DISCOUNT_EXPECTED)
        PKG_FOR_EXPECTED = DBNulls.NumberValue(intbl.PKG_FOR_EXPECTED)
        FREIGHT_EXPECTED = DBNulls.NumberValue(intbl.FREIGHT_EXPECTED)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "PO_DOCID" Then
            Return PO_DocId
        End If
        If UCase(inFieldName) = "SR_NUMBER" Then
            Return Sr_number
        End If
        If UCase(inFieldName) = "MATERIAL_CODE" Then
            Return Material_Code
        End If
        If UCase(inFieldName) = "DESCRIPTION" Then
            Return Description
        End If
        If UCase(inFieldName) = "QTY" Then
            Return Qty
        End If
        If UCase(inFieldName) = "UOM" Then
            Return UOM
        End If
        If UCase(inFieldName) = "BATCH_NUMBER" Then
            Return Batch_Number
        End If
        If UCase(inFieldName) = "STOR_LOC" Then
            Return Stor_Loc
        End If
        If UCase(inFieldName) = "PLANT_CODE" Then
            Return Plant_Code
        End If
        If UCase(inFieldName) = "RATE_ACTUAL" Then
            Return Rate_Actual
        End If
        If UCase(inFieldName) = "RATE_EXPECTED" Then
            Return Rate_Expected
        End If
        If UCase(inFieldName) = "GRAND_TOTAL_ACTUAL" Then
            Return Grand_Total_Actual
        End If
        If UCase(inFieldName) = "GRAND_TOTAL_EXPECTED" Then
            Return Grand_Total_Expected
        End If
        If UCase(inFieldName) = "VAT_EXPECTED" Then
            Return VAT_Expected
        End If
        If UCase(inFieldName) = "DISCOUNT_EXPECTED" Then
            Return DISCOUNT_Expected
        End If
        If UCase(inFieldName) = "PKG_FOR_EXPECTED" Then
            Return PKG_FOR_Expected
        End If
        If UCase(inFieldName) = "FREIGHT_EXPECTED" Then
            Return FREIGHT_Expected
        End If
        Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_DocId As String
    Property PO_DocId() As String
        Get
            Return vPO_DocId
        End Get
        Set(ByVal value As String)
            vPO_DocId = value
            If vPO_DocId > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vSr_number As String
    Property Sr_number() As String
        Get
            Return vSr_number
        End Get
        Set(ByVal value As String)
            vSr_number = value
            If vSr_number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vMaterial_Code As String
    Property Material_Code() As String
        Get
            Return vMaterial_Code
        End Get
        Set(ByVal value As String)
            vMaterial_Code = value
            If vMaterial_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDescription As String
    Property Description() As String
        Get
            Return vDescription
        End Get
        Set(ByVal value As String)
            vDescription = value
            If vDescription > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vQty As Double
    Property Qty() As Double
        Get
            Return vQty
        End Get
        Set(ByVal value As Double)
            vQty = value
            If vQty > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vUOM As String
    Property UOM() As String
        Get
            Return vUOM
        End Get
        Set(ByVal value As String)
            vUOM = value
            If vUOM > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vBatch_Number As String
    Property Batch_Number() As String
        Get
            Return vBatch_Number
        End Get
        Set(ByVal value As String)
            vBatch_Number = value
            If vBatch_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vStor_Loc As String
    Property Stor_Loc() As String
        Get
            Return vStor_Loc
        End Get
        Set(ByVal value As String)
            vStor_Loc = value
            If vStor_Loc > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPlant_Code As String
    Property Plant_Code() As String
        Get
            Return vPlant_Code
        End Get
        Set(ByVal value As String)
            vPlant_Code = value
            If vPlant_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vGrand_Total_Actual As Double
    Property Grand_Total_Actual() As Double
        Get
            Return vGrand_Total_Actual
        End Get
        Set(ByVal value As Double)
            vGrand_Total_Actual = Value
            If vGrand_Total_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vGrand_Total_Expected As Double
    Property Grand_Total_Expected() As Double
        Get
            Return vGrand_Total_Expected
        End Get
        Set(ByVal value As Double)
            vGrand_Total_Expected = Value
            If vGrand_Total_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vVAT_Expected As Double
    Property VAT_Expected() As Double
        Get
            Return vVAT_Expected
        End Get
        Set(ByVal value As Double)
            vVAT_Expected = Value
            If vVAT_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vDISCOUNT_Expected As Double
    Property DISCOUNT_Expected() As Double
        Get
            Return vDISCOUNT_Expected
        End Get
        Set(ByVal value As Double)
            vDISCOUNT_Expected = Value
            If vDISCOUNT_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vPKG_FOR_Expected As Double
    Property PKG_FOR_Expected() As Double
        Get
            Return vPKG_FOR_Expected
        End Get
        Set(ByVal value As Double)
            vPKG_FOR_Expected = Value
            If vPKG_FOR_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vFREIGHT_Expected As Double
    Property FREIGHT_Expected() As Double
        Get
            Return vFREIGHT_Expected
        End Get
        Set(ByVal value As Double)
            vFREIGHT_Expected = Value
            If vFREIGHT_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Public vtbl_SCPO_Log_Child_DetailsArray(50) As DOC_SCPO_Log_Child_Details
    Property tbl_SCPO_Log_Child_DetailsArray(ByVal i As Integer) As DOC_SCPO_Log_Child_Details
        Get
            If vtbl_SCPO_Log_Child_DetailsArray(i) Is Nothing Then
                vtbl_SCPO_Log_Child_DetailsArray(i) = New DOC_SCPO_Log_Child_Details
            End If
            Return vtbl_SCPO_Log_Child_DetailsArray(i)
        End Get
        Set(ByVal value As DOC_SCPO_Log_Child_Details)
            vtbl_SCPO_Log_Child_DetailsArray(i) = value
        End Set
    End Property
    Dim vRate_Actual As Double
    Property Rate_Actual() As Double
        Get
            Return vRate_Actual
        End Get
        Set(ByVal value As Double)
            vRate_Actual = Value
            If vRate_Actual > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Dim vRate_Expected As Double
    Property Rate_Expected() As Double
        Get
            Return vRate_Expected
        End Get
        Set(ByVal value As Double)
            vRate_Expected = Value
            If vRate_Expected > 0 Then
                is_Filled = True
            End If
        End Set
    End Property
    Public Function clone() As DOC_SCPO_Log_Parent_Details
        Dim retval As New DOC_SCPO_Log_Parent_Details
        Dim i As Integer
        i = 0
        retval.DocID = DocID
        retval.PO_DocId = PO_DocId
        retval.Sr_number = Sr_number
        retval.Material_Code = Material_Code
        retval.Description = Description
        retval.Qty = Qty
        retval.UOM = UOM
        retval.Batch_Number = Batch_Number
        retval.Stor_Loc = Stor_Loc
        retval.Plant_Code = Plant_Code
        retval.Rate_Actual = Rate_Actual
        retval.Rate_Expected = Rate_Expected
        retval.Grand_Total_Actual = Grand_Total_Actual
        retval.Grand_Total_Expected = Grand_Total_Expected
        retval.VAT_Expected = VAT_Expected
        retval.DISCOUNT_Expected = DISCOUNT_Expected
        retval.PKG_FOR_Expected = PKG_FOR_Expected
        retval.FREIGHT_Expected = FREIGHT_Expected
        For i = 0 To 50 - 1
            retval.tbl_SCPO_Log_Child_DetailsArray(i) = tbl_SCPO_Log_Child_DetailsArray(i).clone
        Next i
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        DocID = ""
        PO_DocId = ""
        Sr_number = ""
        Material_Code = ""
        Description = ""
        Qty = 0
        UOM = ""
        Batch_Number = ""
        Stor_Loc = ""
        Plant_Code = ""
        Rate_Actual = 0
        Rate_Expected = 0
        Grand_Total_Actual = 0
        Grand_Total_Expected = 0
        VAT_Expected = 0
        DISCOUNT_Expected = 0
        PKG_FOR_Expected = 0
        FREIGHT_Expected = 0
        For i = 0 To 50 - 1
            tbl_SCPO_Log_Child_DetailsArray(i).Blank()
        Next i
    End Sub
    Public Sub Remove_tbl_SCPO_Log_Child_DetailsArray(ByVal In_Index As Integer)
        tbl_SCPO_Log_Child_DetailsArray(In_Index).Blank()
        For i = In_Index To 50 - 2
            tbl_SCPO_Log_Child_DetailsArray(i) = tbl_SCPO_Log_Child_DetailsArray(i + 1).clone
        Next i
        tbl_SCPO_Log_Child_DetailsArray(50 - 1).Blank()
    End Sub
    Public Function tbl_SCPO_Log_Child_Details_Count() As Integer
        For I = 0 To 50 - 1
            If tbl_SCPO_Log_Child_DetailsArray(I).Is_Filled Then
            Else
                Return I
            End If
        Next
        Return 50
    End Function
    Public Function tbl_SCPO_Log_Child_Details_SUM(ByVal inField As String) As Double
        Dim vvAL As Double
        vvAL = 0
        For i = 0 To tbl_SCPO_Log_Child_Details_Count() - 1
            vvAL = vvAL + Val(tbl_SCPO_Log_Child_DetailsArray(i).FieldValue(inField))
        Next i
        Return vvAL
    End Function

    Public Function tbl_SCPO_Log_Child_Details_Array_ByFieldValue(ByVal inDoc As DOC_SCPO_Log_Parent_Details, ByVal inFieldName As String, ByVal inValue As Object) As DOC_SCPO_Log_Child_Details
        For i = 0 To 50 - 1
            If inDoc.tbl_SCPO_Log_Child_DetailsArray(i).FieldValue(inFieldName).ToString.Trim.ToUpper = inValue.ToString.Trim.ToUpper Then
                Return inDoc.tbl_SCPO_Log_Child_DetailsArray(i)
            End If
        Next
        Return Nothing
    End Function

End Class



Public Class DOC_SEPO_Log_Child_Details
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.DOCID = DocID
        intbl.PARENT_DOCID = Parent_DocId
        intbl.SR_NUMBER = Sr_number
        intbl.MATERIAL_CODE = Material_Code
        intbl.DESCRIPTION = Description
        intbl.QTY = Qty
        intbl.PLANT_CODE = Plant_Code
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        DocID = DBNulls.StringValue(intbl.DOCID)
        Parent_DocId = DBNulls.StringValue(intbl.PARENT_DOCID)
        Sr_number = DBNulls.StringValue(intbl.SR_NUMBER)
        Material_Code = DBNulls.StringValue(intbl.MATERIAL_CODE)
        Description = DBNulls.StringValue(intbl.DESCRIPTION)
        Qty = DBNulls.NumberValue(intbl.QTY)
        Plant_Code = DBNulls.StringValue(intbl.PLANT_CODE)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "PARENT_DOCID" Then
            Return Parent_DocId
        End If
        If UCase(inFieldName) = "SR_NUMBER" Then
            Return Sr_number
        End If
        If UCase(inFieldName) = "MATERIAL_CODE" Then
            Return Material_Code
        End If
        If UCase(inFieldName) = "DESCRIPTION" Then
            Return Description
        End If
        If UCase(inFieldName) = "QTY" Then
            Return Qty
        End If
        If UCase(inFieldName) = "PLANT_CODE" Then
            Return Plant_Code
        End If
        Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vParent_DocId As String
    Property Parent_DocId() As String
        Get
            Return vParent_DocId
        End Get
        Set(ByVal value As String)
            vParent_DocId = value
            If vParent_DocId > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vSr_number As String
    Property Sr_number() As String
        Get
            Return vSr_number
        End Get
        Set(ByVal value As String)
            vSr_number = value
            If vSr_number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vMaterial_Code As String
    Property Material_Code() As String
        Get
            Return vMaterial_Code
        End Get
        Set(ByVal value As String)
            vMaterial_Code = value
            If vMaterial_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDescription As String
    Property Description() As String
        Get
            Return vDescription
        End Get
        Set(ByVal value As String)
            vDescription = value
            If vDescription > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vQty As Double
    Property Qty() As Double
        Get
            Return vQty
        End Get
        Set(ByVal value As Double)
            vQty = value
            If vQty > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPlant_Code As String
    Property Plant_Code() As String
        Get
            Return vPlant_Code
        End Get
        Set(ByVal value As String)
            vPlant_Code = value
            If vPlant_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Public Function clone() As DOC_SEPO_Log_Child_Details
        Dim retval As New DOC_SEPO_Log_Child_Details
        Dim i As Integer
        i = 0
        retval.DocID = DocID
        retval.Parent_DocId = Parent_DocId
        retval.Sr_number = Sr_number
        retval.Material_Code = Material_Code
        retval.Description = Description
        retval.Qty = Qty
        retval.Plant_Code = Plant_Code
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        DocID = ""
        Parent_DocId = ""
        Sr_number = ""
        Material_Code = ""
        Description = ""
        Qty = 0
        Plant_Code = ""
    End Sub
End Class



Public Class DOC_SEPO_Log_Master
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.DOCID = DocID
        intbl.VENDOR_CODE = Vendor_Code
        intbl.DOC_DATE = Doc_Date
        intbl.DOC_DATE_RTC = Doc_Date_RTC
        intbl.PO_NUMBER = PO_Number
        intbl.PO_DATE = PO_Date
        intbl.USER_CREATED = User_Created
        intbl.USER_REPRINTED = User_Reprinted
        intbl.CREATED_DATETIME = Created_DateTime
        intbl.MODIFIED_DATETIME = Modified_DateTime
        intbl.ASN_NUMBER = ASN_Number
        intbl.DELIVERY_NOTE_NUMBER = Delivery_note_number
        intbl.VENDOR_NAME = Vendor_Name
        intbl.IS_CANCELLED = Is_Cancelled
        intbl.CANCELLED_BY = Cancelled_By
        intbl.CANCELLATION_DATE = Cancellation_Date
        intbl.CANCELLATION_DATE_RTC = Cancellation_Date_RTC
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        DocID = DBNulls.StringValue(intbl.DOCID)
        Vendor_Code = DBNulls.StringValue(intbl.VENDOR_CODE)
        Doc_Date = (intbl.DOC_DATE)
        Doc_Date_RTC = DBNulls.StringValue(intbl.DOC_DATE_RTC)
        PO_Number = DBNulls.StringValue(intbl.PO_NUMBER)
        PO_Date = (intbl.PO_DATE)
        User_Created = DBNulls.StringValue(intbl.USER_CREATED)
        User_Reprinted = DBNulls.StringValue(intbl.USER_REPRINTED)
        Created_DateTime = DBNulls.StringValue(intbl.CREATED_DATETIME)
        Modified_DateTime = DBNulls.StringValue(intbl.MODIFIED_DATETIME)
        ASN_Number = DBNulls.StringValue(intbl.ASN_NUMBER)
        Delivery_note_number = DBNulls.StringValue(intbl.DELIVERY_NOTE_NUMBER)
        Vendor_Name = DBNulls.StringValue(intbl.VENDOR_NAME)
        Is_Cancelled = DBNulls.StringValue(intbl.IS_CANCELLED)
        Cancelled_By = DBNulls.StringValue(intbl.CANCELLED_BY)
        Cancellation_Date = (intbl.CANCELLATION_DATE)
        Cancellation_Date_RTC = DBNulls.NumberValue(intbl.CANCELLATION_DATE_RTC)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "VENDOR_CODE" Then
            Return Vendor_Code
        End If
        If UCase(inFieldName) = "DOC_DATE" Then
            Return Doc_Date
        End If
        If UCase(inFieldName) = "DOC_DATE_RTC" Then
            Return Doc_Date_RTC
        End If
        If UCase(inFieldName) = "PO_NUMBER" Then
            Return PO_Number
        End If
        If UCase(inFieldName) = "PO_DATE" Then
            Return PO_Date
        End If
        If UCase(inFieldName) = "USER_CREATED" Then
            Return User_Created
        End If
        If UCase(inFieldName) = "USER_REPRINTED" Then
            Return User_Reprinted
        End If
        If UCase(inFieldName) = "CREATED_DATETIME" Then
            Return Created_DateTime
        End If
        If UCase(inFieldName) = "MODIFIED_DATETIME" Then
            Return Modified_DateTime
        End If
        If UCase(inFieldName) = "ASN_NUMBER" Then
            Return ASN_Number
        End If
        If UCase(inFieldName) = "DELIVERY_NOTE_NUMBER" Then
            Return Delivery_note_number
        End If
        If UCase(inFieldName) = "VENDOR_NAME" Then
            Return Vendor_Name
        End If
        If UCase(inFieldName) = "IS_CANCELLED" Then
            Return Is_Cancelled
        End If
        If UCase(inFieldName) = "CANCELLED_BY" Then
            Return Cancelled_By
        End If
        If UCase(inFieldName) = "CANCELLATION_DATE" Then
            Return Cancellation_Date
        End If
        If UCase(inFieldName) = "CANCELLATION_DATE_RTC" Then
            Return Cancellation_Date_RTC
        End If
        Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vVendor_Code As String
    Property Vendor_Code() As String
        Get
            Return vVendor_Code
        End Get
        Set(ByVal value As String)
            vVendor_Code = value
            If vVendor_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDoc_Date As Date
    Property Doc_Date() As Date
        Get
            Return vDoc_Date
        End Get
        Set(ByVal value As Date)
            vDoc_Date = value
        End Set
    End Property
    Dim vDoc_Date_RTC As String
    Property Doc_Date_RTC() As String
        Get
            Return vDoc_Date_RTC
        End Get
        Set(ByVal value As String)
            vDoc_Date_RTC = value
            If vDoc_Date_RTC > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_Number As String
    Property PO_Number() As String
        Get
            Return vPO_Number
        End Get
        Set(ByVal value As String)
            vPO_Number = value
            If vPO_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_Date As Date
    Property PO_Date() As Date
        Get
            Return vPO_Date
        End Get
        Set(ByVal value As Date)
            vPO_Date = value
        End Set
    End Property
    Dim vUser_Created As String
    Property User_Created() As String
        Get
            Return vUser_Created
        End Get
        Set(ByVal value As String)
            vUser_Created = value
            If vUser_Created > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vUser_Reprinted As String
    Property User_Reprinted() As String
        Get
            Return vUser_Reprinted
        End Get
        Set(ByVal value As String)
            vUser_Reprinted = value
            If vUser_Reprinted > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCreated_DateTime As String
    Property Created_DateTime() As String
        Get
            Return vCreated_DateTime
        End Get
        Set(ByVal value As String)
            vCreated_DateTime = value
            If vCreated_DateTime > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vModified_DateTime As String
    Property Modified_DateTime() As String
        Get
            Return vModified_DateTime
        End Get
        Set(ByVal value As String)
            vModified_DateTime = value
            If vModified_DateTime > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vASN_Number As String
    Property ASN_Number() As String
        Get
            Return vASN_Number
        End Get
        Set(ByVal value As String)
            vASN_Number = value
            If vASN_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDelivery_note_number As String
    Property Delivery_note_number() As String
        Get
            Return vDelivery_note_number
        End Get
        Set(ByVal value As String)
            vDelivery_note_number = value
            If vDelivery_note_number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vVendor_Name As String
    Property Vendor_Name() As String
        Get
            Return vVendor_Name
        End Get
        Set(ByVal value As String)
            vVendor_Name = value
            If vVendor_Name > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vIs_Cancelled As String
    Property Is_Cancelled() As String
        Get
            Return vIs_Cancelled
        End Get
        Set(ByVal value As String)
            vIs_Cancelled = value
            If vIs_Cancelled > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCancelled_By As String
    Property Cancelled_By() As String
        Get
            Return vCancelled_By
        End Get
        Set(ByVal value As String)
            vCancelled_By = value
            If vCancelled_By > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCancellation_Date As Date
    Property Cancellation_Date() As Date
        Get
            Return vCancellation_Date
        End Get
        Set(ByVal value As Date)
            vCancellation_Date = value
        End Set
    End Property
    Dim vCancellation_Date_RTC As Double
    Property Cancellation_Date_RTC() As Double
        Get
            Return vCancellation_Date_RTC
        End Get
        Set(ByVal value As Double)
            vCancellation_Date_RTC = value
            If vCancellation_Date_RTC > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Public vtbl_SEPO_Log_Parent_DetailsArray(50) As DOC_SEPO_Log_Parent_Details
    Property tbl_SEPO_Log_Parent_DetailsArray(ByVal i As Integer) As DOC_SEPO_Log_Parent_Details
        Get
            If vtbl_SEPO_Log_Parent_DetailsArray(i) Is Nothing Then
                vtbl_SEPO_Log_Parent_DetailsArray(i) = New DOC_SEPO_Log_Parent_Details
            End If
            Return vtbl_SEPO_Log_Parent_DetailsArray(i)
        End Get
        Set(ByVal value As DOC_SEPO_Log_Parent_Details)
            vtbl_SEPO_Log_Parent_DetailsArray(i) = value
        End Set
    End Property
    Public Function clone() As DOC_SEPO_Log_Master
        Dim retval As New DOC_SEPO_Log_Master
        Dim i As Integer
        i = 0
        retval.DocID = DocID
        retval.Vendor_Code = Vendor_Code
        retval.Doc_Date = Doc_Date
        retval.Doc_Date_RTC = Doc_Date_RTC
        retval.PO_Number = PO_Number
        retval.PO_Date = PO_Date
        retval.User_Created = User_Created
        retval.User_Reprinted = User_Reprinted
        retval.Created_DateTime = Created_DateTime
        retval.Modified_DateTime = Modified_DateTime
        retval.ASN_Number = ASN_Number
        retval.Delivery_note_number = Delivery_note_number
        retval.Vendor_Name = Vendor_Name
        retval.Is_Cancelled = Is_Cancelled
        retval.Cancelled_By = Cancelled_By
        retval.Cancellation_Date = Cancellation_Date
        retval.Cancellation_Date_RTC = Cancellation_Date_RTC
        For i = 0 To 50 - 1
            retval.tbl_SEPO_Log_Parent_DetailsArray(i) = tbl_SEPO_Log_Parent_DetailsArray(i).clone
        Next i
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        DocID = ""
        Vendor_Code = ""
        Doc_Date = CDate("01/Jan/1950")
        Doc_Date_RTC = ""
        PO_Number = ""
        PO_Date = CDate("01/Jan/1950")
        User_Created = ""
        User_Reprinted = ""
        Created_DateTime = ""
        Modified_DateTime = ""
        ASN_Number = ""
        Delivery_note_number = ""
        Vendor_Name = ""
        Is_Cancelled = ""
        Cancelled_By = ""
        Cancellation_Date = CDate("01/Jan/1950")
        Cancellation_Date_RTC = 0
        For i = 0 To 50 - 1
            tbl_SEPO_Log_Parent_DetailsArray(i).Blank()
        Next i
    End Sub
    Public Sub Remove_tbl_SEPO_Log_Parent_DetailsArray(ByVal In_Index As Integer)
        tbl_SEPO_Log_Parent_DetailsArray(In_Index).Blank()
        For i = In_Index To 50 - 2
            tbl_SEPO_Log_Parent_DetailsArray(i) = tbl_SEPO_Log_Parent_DetailsArray(i + 1).clone
        Next i
        tbl_SEPO_Log_Parent_DetailsArray(50 - 1).Blank()
    End Sub
    Public Function tbl_SEPO_Log_Parent_Details_Count() As Integer
        For I = 0 To 50 - 1
            If tbl_SEPO_Log_Parent_DetailsArray(I).Is_Filled Then
            Else
                Return I
            End If
        Next
        Return 50
    End Function
    Public Function tbl_SEPO_Log_Parent_Details_SUM(ByVal inField As String) As Double
        Dim vvAL As Double
        vvAL = 0
        For i = 0 To tbl_SEPO_Log_Parent_Details_Count() - 1
            vvAL = vvAL + Val(tbl_SEPO_Log_Parent_DetailsArray(i).FieldValue(inField))
        Next i
        Return vvAL
    End Function

    Public Function tbl_SEPO_Log_Parent_Details_Array_ByFieldValue(ByVal inDoc As DOC_SEPO_Log_Master, ByVal inFieldName As String, ByVal inValue As Object) As DOC_SEPO_Log_Parent_Details
        For i = 0 To 50 - 1
            If inDoc.tbl_SEPO_Log_Parent_DetailsArray(i).FieldValue(inFieldName).ToString.Trim.ToUpper = inValue.ToString.Trim.ToUpper Then
                Return inDoc.tbl_SEPO_Log_Parent_DetailsArray(i)
            End If
        Next
        Return Nothing
    End Function

End Class



Public Class DOC_SEPO_Log_Parent_Details
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.DOCID = DocID
        intbl.PO_DOCID = PO_DocId
        intbl.SR_NUMBER = Sr_number
        intbl.SERVICETEXT = ServiceText
        intbl.QTY = Qty
        intbl.PLANT_CODE = Plant_Code
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        DocID = DBNulls.StringValue(intbl.DOCID)
        PO_DocId = DBNulls.StringValue(intbl.PO_DOCID)
        Sr_number = DBNulls.StringValue(intbl.SR_NUMBER)
        ServiceText = DBNulls.StringValue(intbl.SERVICETEXT)
        Qty = DBNulls.NumberValue(intbl.QTY)
        Plant_Code = DBNulls.StringValue(intbl.PLANT_CODE)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "PO_DOCID" Then
            Return PO_DocId
        End If
        If UCase(inFieldName) = "SR_NUMBER" Then
            Return Sr_number
        End If
        If UCase(inFieldName) = "SERVICETEXT" Then
            Return ServiceText
        End If
        If UCase(inFieldName) = "QTY" Then
            Return Qty
        End If
        If UCase(inFieldName) = "PLANT_CODE" Then
            Return Plant_Code
        End If
        Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPO_DocId As String
    Property PO_DocId() As String
        Get
            Return vPO_DocId
        End Get
        Set(ByVal value As String)
            vPO_DocId = value
            If vPO_DocId > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vSr_number As String
    Property Sr_number() As String
        Get
            Return vSr_number
        End Get
        Set(ByVal value As String)
            vSr_number = value
            If vSr_number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vServiceText As String
    Property ServiceText() As String
        Get
            Return vServiceText
        End Get
        Set(ByVal value As String)
            vServiceText = value
            If vServiceText > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vQty As Double
    Property Qty() As Double
        Get
            Return vQty
        End Get
        Set(ByVal value As Double)
            vQty = value
            If vQty > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPlant_Code As String
    Property Plant_Code() As String
        Get
            Return vPlant_Code
        End Get
        Set(ByVal value As String)
            vPlant_Code = value
            If vPlant_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Public vtbl_SEPO_Log_Child_DetailsArray(50) As DOC_SEPO_Log_Child_Details
    Property tbl_SEPO_Log_Child_DetailsArray(ByVal i As Integer) As DOC_SEPO_Log_Child_Details
        Get
            If vtbl_SEPO_Log_Child_DetailsArray(i) Is Nothing Then
                vtbl_SEPO_Log_Child_DetailsArray(i) = New DOC_SEPO_Log_Child_Details
            End If
            Return vtbl_SEPO_Log_Child_DetailsArray(i)
        End Get
        Set(ByVal value As DOC_SEPO_Log_Child_Details)
            vtbl_SEPO_Log_Child_DetailsArray(i) = value
        End Set
    End Property
    Public Function clone() As DOC_SEPO_Log_Parent_Details
        Dim retval As New DOC_SEPO_Log_Parent_Details
        Dim i As Integer
        i = 0
        retval.DocID = DocID
        retval.PO_DocId = PO_DocId
        retval.Sr_number = Sr_number
        retval.ServiceText = ServiceText
        retval.Qty = Qty
        retval.Plant_Code = Plant_Code
        For i = 0 To 50 - 1
            retval.tbl_SEPO_Log_Child_DetailsArray(i) = tbl_SEPO_Log_Child_DetailsArray(i).clone
        Next i
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        DocID = ""
        PO_DocId = ""
        Sr_number = ""
        ServiceText = ""
        Qty = 0
        Plant_Code = ""
        For i = 0 To 50 - 1
            tbl_SEPO_Log_Child_DetailsArray(i).Blank()
        Next i
    End Sub
    Public Sub Remove_tbl_SEPO_Log_Child_DetailsArray(ByVal In_Index As Integer)
        tbl_SEPO_Log_Child_DetailsArray(In_Index).Blank()
        For i = In_Index To 50 - 2
            tbl_SEPO_Log_Child_DetailsArray(i) = tbl_SEPO_Log_Child_DetailsArray(i + 1).clone
        Next i
        tbl_SEPO_Log_Child_DetailsArray(50 - 1).Blank()
    End Sub
    Public Function tbl_SEPO_Log_Child_Details_Count() As Integer
        For I = 0 To 50 - 1
            If tbl_SEPO_Log_Child_DetailsArray(I).Is_Filled Then
            Else
                Return I
            End If
        Next
        Return 50
    End Function
    Public Function tbl_SEPO_Log_Child_Details_SUM(ByVal inField As String) As Double
        Dim vvAL As Double
        vvAL = 0
        For i = 0 To tbl_SEPO_Log_Child_Details_Count() - 1
            vvAL = vvAL + Val(tbl_SEPO_Log_Child_DetailsArray(i).FieldValue(inField))
        Next i
        Return vvAL
    End Function

    Public Function tbl_SEPO_Log_Child_Details_Array_ByFieldValue(ByVal inDoc As DOC_SEPO_Log_Parent_Details, ByVal inFieldName As String, ByVal inValue As Object) As DOC_SEPO_Log_Child_Details
        For i = 0 To 50 - 1
            If inDoc.tbl_SEPO_Log_Child_DetailsArray(i).FieldValue(inFieldName).ToString.Trim.ToUpper = inValue.ToString.Trim.ToUpper Then
                Return inDoc.tbl_SEPO_Log_Child_DetailsArray(i)
            End If
        Next
        Return Nothing
    End Function

End Class


Public Class DOC_GEDetail
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.GE_DOCID = GE_DocID
        intbl.DOCID = DocID
        intbl.SRNO = SrNo
        intbl.MATERIAL_CODE = Material_Code
        intbl.DESCRIPTION = Description
        intbl.UOM = UOM
        intbl.QUANTITY = Quantity
        intbl.BATCH_NUMBER = Batch_Number
        intbl.PLANT_CODE = Plant_Code
        intbl.STORAGE_LOCATION = Storage_Location
        intbl.DELIVERY_DATE = Delivery_Date
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        GE_DocID = DBNulls.StringValue(intbl.GE_DOCID)
        DocID = DBNulls.StringValue(intbl.DOCID)
        SrNo = DBNulls.NumberValue(intbl.SRNO)
        Material_Code = DBNulls.StringValue(intbl.MATERIAL_CODE)
        Description = DBNulls.StringValue(intbl.DESCRIPTION)
        UOM = DBNulls.StringValue(intbl.UOM)
        Quantity = DBNulls.NumberValue(intbl.QUANTITY)
        Batch_Number = DBNulls.StringValue(intbl.BATCH_NUMBER)
        Plant_Code = DBNulls.StringValue(intbl.PLANT_CODE)
        Storage_Location = DBNulls.StringValue(intbl.STORAGE_LOCATION)
        Delivery_Date = (intbl.DELIVERY_DATE)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "GE_DOCID" Then
            Return GE_DocID
        End If
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "SRNO" Then
            Return SrNo
        End If
        If UCase(inFieldName) = "MATERIAL_CODE" Then
            Return Material_Code
        End If
        If UCase(inFieldName) = "DESCRIPTION" Then
            Return Description
        End If
        If UCase(inFieldName) = "UOM" Then
            Return UOM
        End If
        If UCase(inFieldName) = "QUANTITY" Then
            Return Quantity
        End If
        If UCase(inFieldName) = "BATCH_NUMBER" Then
            Return Batch_Number
        End If
        If UCase(inFieldName) = "PLANT_CODE" Then
            Return Plant_Code
        End If
        If UCase(inFieldName) = "STORAGE_LOCATION" Then
            Return Storage_Location
        End If
        If UCase(inFieldName) = "DELIVERY_DATE" Then
            Return Delivery_Date
        End If
        Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vGE_DocID As String
    Property GE_DocID() As String
        Get
            Return vGE_DocID
        End Get
        Set(ByVal value As String)
            vGE_DocID = value
            If vGE_DocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vSrNo As Double
    Property SrNo() As Double
        Get
            Return vSrNo
        End Get
        Set(ByVal value As Double)
            vSrNo = value
            If vSrNo > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vMaterial_Code As String
    Property Material_Code() As String
        Get
            Return vMaterial_Code
        End Get
        Set(ByVal value As String)
            vMaterial_Code = value
            If vMaterial_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDescription As String
    Property Description() As String
        Get
            Return vDescription
        End Get
        Set(ByVal value As String)
            vDescription = value
            If vDescription > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vUOM As String
    Property UOM() As String
        Get
            Return vUOM
        End Get
        Set(ByVal value As String)
            vUOM = value
            If vUOM > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vQuantity As Double
    Property Quantity() As Double
        Get
            Return vQuantity
        End Get
        Set(ByVal value As Double)
            vQuantity = value
            If vQuantity > 0 Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vBatch_Number As String
    Property Batch_Number() As String
        Get
            Return vBatch_Number
        End Get
        Set(ByVal value As String)
            vBatch_Number = value
            If vBatch_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vPlant_Code As String
    Property Plant_Code() As String
        Get
            Return vPlant_Code
        End Get
        Set(ByVal value As String)
            vPlant_Code = value
            If vPlant_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vStorage_Location As String
    Property Storage_Location() As String
        Get
            Return vStorage_Location
        End Get
        Set(ByVal value As String)
            vStorage_Location = value
            If vStorage_Location > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vDelivery_Date As Date
    Property Delivery_Date() As Date
        Get
            Return vDelivery_Date
        End Get
        Set(ByVal value As Date)
            vDelivery_Date = value
        End Set
    End Property
    Public Function clone() As DOC_GEDetail
        Dim retval As New DOC_GEDetail
        Dim i As Integer
        i = 0
        retval.GE_DocID = GE_DocID
        retval.DocID = DocID
        retval.SrNo = SrNo
        retval.Material_Code = Material_Code
        retval.Description = Description
        retval.UOM = UOM
        retval.Quantity = Quantity
        retval.Batch_Number = Batch_Number
        retval.Plant_Code = Plant_Code
        retval.Storage_Location = Storage_Location
        retval.Delivery_Date = Delivery_Date
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        GE_DocID = ""
        DocID = ""
        SrNo = 0
        Material_Code = ""
        Description = ""
        UOM = ""
        Quantity = 0
        Batch_Number = ""
        Plant_Code = ""
        Storage_Location = ""
        Delivery_Date = CDate("01/Jan/1950")
    End Sub
End Class

Public Class DOC_GEMaster
    Public Sub Doc_to_Table(ByVal intbl As Object)
        intbl.DOCID = DocID
        intbl.GE_NUMBER = GE_Number
        intbl.GE_DATE = GE_Date
        intbl.VENDOR_CODE = Vendor_Code
        intbl.USER_CREATED = User_Created
        intbl.USER_MODIFIED = User_Modified
        intbl.CREATED_DATETIME = Created_DateTime
        intbl.MODIFIED_DATETIME = Modified_DateTime
        intbl.PLANT_CODE = Plant_Code
    End Sub
    Public Sub Table_To_Doc(ByVal intbl As Object)
        DocID = DBNulls.StringValue(intbl.DOCID)
        GE_Number = DBNulls.StringValue(intbl.GE_NUMBER)
        GE_Date = (intbl.GE_DATE)
        Vendor_Code = DBNulls.StringValue(intbl.VENDOR_CODE)
        User_Created = DBNulls.StringValue(intbl.USER_CREATED)
        User_Modified = DBNulls.StringValue(intbl.USER_MODIFIED)
        Created_DateTime = (intbl.CREATED_DATETIME)
        Modified_DateTime = (intbl.MODIFIED_DATETIME)
        Plant_Code = DBNulls.StringValue(intbl.PLANT_CODE)
    End Sub
    Public Function FieldValue(ByVal inFieldName) As Object
        If UCase(inFieldName) = "DOCID" Then
            Return DocID
        End If
        If UCase(inFieldName) = "GE_NUMBER" Then
            Return GE_Number
        End If
        If UCase(inFieldName) = "GE_DATE" Then
            Return GE_Date
        End If
        If UCase(inFieldName) = "VENDOR_CODE" Then
            Return Vendor_Code
        End If
        If UCase(inFieldName) = "USER_CREATED" Then
            Return User_Created
        End If
        If UCase(inFieldName) = "USER_MODIFIED" Then
            Return User_Modified
        End If
        If UCase(inFieldName) = "CREATED_DATETIME" Then
            Return Created_DateTime
        End If
        If UCase(inFieldName) = "MODIFIED_DATETIME" Then
            Return Modified_DateTime
        End If
        If UCase(inFieldName) = "PLANT_CODE" Then
            Return Plant_Code
        End If
        Return Nothing
    End Function
    Public Is_Filled As Boolean
    Dim vDocID As String
    Property DocID() As String
        Get
            Return vDocID
        End Get
        Set(ByVal value As String)
            vDocID = value
            If vDocID > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vGE_Number As String
    Property GE_Number() As String
        Get
            Return vGE_Number
        End Get
        Set(ByVal value As String)
            vGE_Number = value
            If vGE_Number > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vGE_Date As Date
    Property GE_Date() As Date
        Get
            Return vGE_Date
        End Get
        Set(ByVal value As Date)
            vGE_Date = value
        End Set
    End Property
    Dim vVendor_Code As String
    Property Vendor_Code() As String
        Get
            Return vVendor_Code
        End Get
        Set(ByVal value As String)
            vVendor_Code = value
            If vVendor_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vUser_Created As String
    Property User_Created() As String
        Get
            Return vUser_Created
        End Get
        Set(ByVal value As String)
            vUser_Created = value
            If vUser_Created > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vUser_Modified As String
    Property User_Modified() As String
        Get
            Return vUser_Modified
        End Get
        Set(ByVal value As String)
            vUser_Modified = value
            If vUser_Modified > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Dim vCreated_DateTime As Date
    Property Created_DateTime() As Date
        Get
            Return vCreated_DateTime
        End Get
        Set(ByVal value As Date)
            vCreated_DateTime = value
        End Set
    End Property
    Dim vModified_DateTime As Date
    Property Modified_DateTime() As Date
        Get
            Return vModified_DateTime
        End Get
        Set(ByVal value As Date)
            vModified_DateTime = value
        End Set
    End Property
    Dim vPlant_Code As String
    Property Plant_Code() As String
        Get
            Return vPlant_Code
        End Get
        Set(ByVal value As String)
            vPlant_Code = value
            If vPlant_Code > " " Then
                Is_Filled = True
            End If
        End Set
    End Property
    Public vtbl_GEDetailArray(50) As DOC_GEDetail
    Property tbl_GEDetailArray(ByVal i As Integer) As DOC_GEDetail
        Get
            If vtbl_GEDetailArray(i) Is Nothing Then
                vtbl_GEDetailArray(i) = New DOC_GEDetail
            End If
            Return vtbl_GEDetailArray(i)
        End Get
        Set(ByVal value As DOC_GEDetail)
            vtbl_GEDetailArray(i) = value
        End Set
    End Property
    Public Function clone() As DOC_GEMaster
        Dim retval As New DOC_GEMaster
        Dim i As Integer
        i = 0
        retval.DocID = DocID
        retval.GE_Number = GE_Number
        retval.GE_Date = GE_Date
        retval.Vendor_Code = Vendor_Code
        retval.User_Created = User_Created
        retval.User_Modified = User_Modified
        retval.Created_DateTime = Created_DateTime
        retval.Modified_DateTime = Modified_DateTime
        retval.Plant_Code = Plant_Code
        For i = 0 To 50 - 1
            retval.tbl_GEDetailArray(i) = tbl_GEDetailArray(i).clone
        Next i
        Return retval
    End Function
    Public Sub Blank()
        Is_Filled = False
        Dim i As Integer
        i = 0
        DocID = ""
        GE_Number = ""
        GE_Date = CDate("01/Jan/1950")
        Vendor_Code = ""
        User_Created = ""
        User_Modified = ""
        Created_DateTime = CDate("01/Jan/1950")
        Modified_DateTime = CDate("01/Jan/1950")
        Plant_Code = ""
        For i = 0 To 50 - 1
            tbl_GEDetailArray(i).Blank()
        Next i
    End Sub
    Public Sub Remove_tbl_GEDetailArray(ByVal In_Index As Integer)
        tbl_GEDetailArray(In_Index).Blank()
        For i = In_Index To 50 - 2
            tbl_GEDetailArray(i) = tbl_GEDetailArray(i + 1).clone
        Next i
        tbl_GEDetailArray(50 - 1).Blank()
    End Sub
    Public Function tbl_GEDetail_Count() As Integer
        For I = 0 To 50 - 1
            If tbl_GEDetailArray(I).Is_Filled Then
            Else
                Return I
            End If
        Next
        Return 50
    End Function
    Public Function tbl_GEDetail_SUM(ByVal inField As String) As Double
        Dim vvAL As Double
        vvAL = 0
        For i = 0 To tbl_GEDetail_Count() - 1
            vvAL = vvAL + Val(tbl_GEDetailArray(i).FieldValue(inField))
        Next i
        Return vvAL
    End Function

    Public Function tbl_GEDetail_Array_ByFieldValue(ByVal inDoc As DOC_GEMaster, ByVal inFieldName As String, ByVal inValue As Object) As DOC_GEDetail
        For i = 0 To 50 - 1
            If inDoc.tbl_GEDetailArray(i).FieldValue(inFieldName).ToString.Trim.ToUpper = inValue.ToString.Trim.ToUpper Then
                Return inDoc.tbl_GEDetailArray(i)
            End If
        Next
        Return Nothing
    End Function

End Class
﻿Imports System.Data.SqlClient

Public Class SqlObj
    Public Shared Sql As New SqlClass()
End Class



'======================
Public Class tblEmail_ContentMaster
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tblEmail_ContentMaster"
        vKeyFieldsString = "Email_Code"
        vKeyElementCount = 0
    End Sub

    Property Email_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Email_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Email_Code") = value
        End Set

    End Property

    Property Emal_Body As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Emal_Body"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Emal_Body") = value
        End Set

    End Property

    Property Email_Subject As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Email_Subject"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Email_Subject") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inEmail_Code As String)
        Clear(con)
        Try
            vDr.Item("Email_Code") = inEmail_Code

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                 Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

'====================================
Public Class tblEmail_SettingsMaster
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "Email_SettingsMaster"
        vKeyFieldsString = "SMTP_Host"
        vKeyElementCount = 0
    End Sub

    Property SMTP_Host As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("SMTP_Host"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("SMTP_Host") = value
        End Set

    End Property

    Property SMTP_Port As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("SMTP_Port"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("SMTP_Port") = value
        End Set

    End Property

    Property Email_Id As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Email_Id"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Email_Id") = value
        End Set

    End Property

    Property Email_Password As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Email_Password"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Email_Password") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inSMTP_Host As String)
        Clear(con)
        Try
            vDr.Item("SMTP_Host") = inSMTP_Host

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_Block_ASN_Log
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_Block_ASN_Log"
        vKeyFieldsString = "Cancelled_By"
        vKeyElementCount = 0
    End Sub

    Property Cancelled_By As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Cancelled_By"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Cancelled_By") = value
        End Set

    End Property

    Property Cancellation_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Cancellation_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Cancellation_Date") = value
        End Set

    End Property

    Property ASN_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ASN_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ASN_Number") = value
        End Set

    End Property

    Property Cancellation_Date_RTC As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Cancellation_Date_RTC"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Cancellation_Date_RTC") = value
        End Set

    End Property

    Property PO_Type As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PO_Type"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PO_Type") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inCancelled_By As String)
        Clear(con)
        Try
            vDr.Item("Cancelled_By") = inCancelled_By

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_News
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_News"
        vKeyFieldsString = "Sequence_Id"
        vKeyElementCount = 0
    End Sub

    Property Sequence_Id As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Sequence_Id"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Sequence_Id") = value
        End Set

    End Property

    Property NewsText As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("NewsText"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("NewsText") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inSequence_Id As Int32)
        Clear(con)
        Try
            vDr.Item("Sequence_Id") = inSequence_Id

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'====================================
Public Class tbl_LogEmail
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_LogEmail"
        vKeyFieldsString = "EmailId"
        vKeyElementCount = 0
    End Sub

    Property EmailId As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("EmailId"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("EmailId") = value
        End Set

    End Property

    Property EmailBody As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("EmailBody"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("EmailBody") = value
        End Set

    End Property

    Property EmailSender As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("EmailSender"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("EmailSender") = value
        End Set

    End Property

    Property EmailReceiver As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("EmailReceiver"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("EmailReceiver") = value
        End Set

    End Property

    Property LastSentDateTime As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("LastSentDateTime"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("LastSentDateTime") = value
        End Set

    End Property

    Property LastSentDateTimeRTC As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("LastSentDateTimeRTC"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("LastSentDateTimeRTC") = value
        End Set

    End Property

    Property TransactionDateTime As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("TransactionDateTime"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("TransactionDateTime") = value
        End Set

    End Property

    Property TransactionDateTimeRTC As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("TransactionDateTimeRTC"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("TransactionDateTimeRTC") = value
        End Set

    End Property

    Property Status As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Status"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Status") = value
        End Set

    End Property

    Property ErrorLog As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ErrorLog"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ErrorLog") = value
        End Set

    End Property

    Property NoOfAttempts As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("NoOfAttempts"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("NoOfAttempts") = value
        End Set

    End Property

    Property EmailSubject As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("EmailSubject"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("EmailSubject") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inEmailId As Decimal)
        Clear(con)
        Try
            vDr.Item("EmailId") = inEmailId

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'====================================
Public Class tbl_REPO_Log_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_REPO_Log_Details"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property PO_DocId As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PO_DocId"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PO_DocId") = value
        End Set

    End Property

    Property Sr_number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Sr_number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Sr_number") = value
        End Set

    End Property

    Property Material_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material_Code") = value
        End Set

    End Property

    Property Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Description") = value
        End Set

    End Property

    Property Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Qty") = value
        End Set

    End Property

    Property UOM As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("UOM"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("UOM") = value
        End Set

    End Property

    Property Batch_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Batch_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Batch_Number") = value
        End Set

    End Property

    Property Stor_Loc As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Stor_Loc"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Stor_Loc") = value
        End Set

    End Property

    Property Plant_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Code") = value
        End Set

    End Property

    Property PO_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("PO_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("PO_Qty") = value
        End Set

    End Property

    Property GR_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("GR_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("GR_Qty") = value
        End Set

    End Property

    Property Open_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Open_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Open_Qty") = value
        End Set

    End Property

    Property Scheduled_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Scheduled_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Scheduled_Qty") = value
        End Set

    End Property

    Property In_Transit_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("In_Transit_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("In_Transit_Qty") = value
        End Set

    End Property

    Property Allowed_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Allowed_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Allowed_Qty") = value
        End Set

    End Property

    Property Rate_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Rate_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Rate_Actual") = value
        End Set

    End Property

    Property Rate_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Rate_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Rate_Expected") = value
        End Set

    End Property

    Property Base_Value_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Base_Value_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Base_Value_Actual") = value
        End Set

    End Property

    Property Base_Value_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Base_Value_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Base_Value_Expected") = value
        End Set

    End Property

    Property Assesebl_Value_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Assesebl_Value_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Assesebl_Value_Actual") = value
        End Set

    End Property

    Property Assesebl_Value_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Assesebl_Value_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Assesebl_Value_Expected") = value
        End Set

    End Property

    Property BED_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("BED_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("BED_Actual") = value
        End Set

    End Property

    Property BED_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("BED_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("BED_Expected") = value
        End Set

    End Property

    Property BED_Percentage As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("BED_Percentage"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("BED_Percentage") = value
        End Set

    End Property

    Property ECESS_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("ECESS_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("ECESS_Actual") = value
        End Set

    End Property

    Property ECESS_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("ECESS_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("ECESS_Expected") = value
        End Set

    End Property

    Property ECESS_Percentage As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("ECESS_Percentage"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("ECESS_Percentage") = value
        End Set

    End Property

    Property HCESS_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("HCESS_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("HCESS_Actual") = value
        End Set

    End Property

    Property HCESS_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("HCESS_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("HCESS_Expected") = value
        End Set

    End Property

    Property HCESS_Percentage As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("HCESS_Percentage"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("HCESS_Percentage") = value
        End Set

    End Property

    Property AED_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("AED_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("AED_Actual") = value
        End Set

    End Property

    Property AED_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("AED_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("AED_Expected") = value
        End Set

    End Property

    Property AED_Percentage As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("AED_Percentage"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("AED_Percentage") = value
        End Set

    End Property
    Property Grand_Total_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Grand_Total_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Grand_Total_Actual") = value
        End Set

    End Property

    Property Grand_Total_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Grand_Total_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Grand_Total_Expected") = value
        End Set

    End Property
    Property VAT_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("VAT_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("VAT_Expected") = value
        End Set

    End Property

    Property FREIGHT_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("FREIGHT_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("FREIGHT_Expected") = value
        End Set

    End Property

    Property PKG_FOR_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("PKG_FOR_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("PKG_FOR_Expected") = value
        End Set

    End Property
    Property VAT_Percentage As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("VAT_Percentage"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("VAT_Percentage") = value
        End Set

    End Property
    Property DISCOUNT_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("DISCOUNT_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("DISCOUNT_Expected") = value
        End Set

    End Property

   
    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inDocID As String)
        Clear(con)
        Try
            vDr.Item("DocID") = inDocID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function


End Class


'======================================
Public Class tbl_REPO_Log_Master
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_REPO_Log_Master"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property Vendor_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Code") = value
        End Set

    End Property

    Property Doc_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Doc_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Doc_Date") = value
        End Set

    End Property

    Property Doc_Date_RTC As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Doc_Date_RTC"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Doc_Date_RTC") = value
        End Set

    End Property

    Property PO_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PO_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PO_Number") = value
        End Set

    End Property

    Property PO_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("PO_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("PO_Date") = value
        End Set

    End Property

    Property User_Created As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("User_Created"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("User_Created") = value
        End Set

    End Property

    Property User_Reprinted As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("User_Reprinted"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("User_Reprinted") = value
        End Set

    End Property

    Property Created_DateTime As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Created_DateTime"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Created_DateTime") = value
        End Set

    End Property

    Property Modified_DateTime As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Modified_DateTime"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Modified_DateTime") = value
        End Set

    End Property

    Property ASN_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ASN_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ASN_Number") = value
        End Set

    End Property

    Property Delivery_note_number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Delivery_note_number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Delivery_note_number") = value
        End Set

    End Property

    Property Vendor_Name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Name") = value
        End Set

    End Property

    Property Is_Cancelled As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Is_Cancelled"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Is_Cancelled") = value
        End Set

    End Property

    Property Cancelled_By As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Cancelled_By"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Cancelled_By") = value
        End Set

    End Property

    Property Cancellation_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Cancellation_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Cancellation_Date") = value
        End Set

    End Property

    Property Cancellation_Date_RTC As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Cancellation_Date_RTC"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Cancellation_Date_RTC") = value
        End Set

    End Property

    Property Invoice_Amount_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Invoice_Amount_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Invoice_Amount_Actual") = value
        End Set

    End Property

    Property Invoice_Amount_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Invoice_Amount_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Invoice_Amount_Expected") = value
        End Set

    End Property
    Property Vehicle_No As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vehicle_No"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vehicle_No") = value
        End Set

    End Property
    Property Transporter_Name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Transporter_Name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Transporter_Name") = value
        End Set

    End Property
    Property Packing_Details As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Packing_Details"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Packing_Details") = value
        End Set

    End Property
    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereclause() As String
        Dim vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve vary(j + 2)
                    vary(j) = vcol.ColumnName
                    vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereclause = Sql.WhereClause(vary)
    End Function
    Private Function tbl_setclause() As String
        Dim vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve vary(j + 2)
            vary(j) = vDr.Table.Columns(i).ColumnName
            vary(j + 1) = vDr.Table.Columns(i).DataType
            vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vary(j + 1).ToString))
            vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_setclause = Sql.SetClause(vary)
    End Function

    Private Function tbl_setclause_nonpk()
        Dim vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve vary(j + 2)
                vary(j) = vDr.Table.Columns(i).ColumnName
                vary(j + 1) = vDr.Table.Columns(i).DataType
                vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vary(j + 1).ToString))
                vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setclause_nonpk = Sql.SetClause(vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal indocid As String)
        Clear(con)
        Try
            vDr.Item("docid") = indocid

            Dim vsql As String
            vsql = "select * from " & tbl_TableName() & " where " & tbl_whereclause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim vsql As String
            vsql = "update " & vTableName & " set " & tbl_setclause_nonpk() & " where " & tbl_whereclause()
            Sql.ExecuteSql(con, vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim vcolscommaseperated As String = ""
        Dim vcolvaluescomaseperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                vcolscommaseperated = vcolscommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolvaluescomaseperated = vcolvaluescomaseperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            vcolscommaseperated = Mid(vcolscommaseperated, 2)
            vcolvaluescomaseperated = Mid(vcolvaluescomaseperated, 2)
            Dim vsql As String
            vsql = "insert into  " & vTableName & _
            " ( " & vcolscommaseperated & ")" & " values (" & vcolvaluescomaseperated & " )"
            Sql.ExecuteSql(con, vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim vsql As String
            vsql = "delete from " & vTableName & " where " & tbl_whereclause()
            Sql.ExecuteSql(con, vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fparametersinfo() As String
        Return tbl_whereclause()
    End Function

End Class


'===========================================
Public Class tbl_SCPO_Log_Child_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_SCPO_Log_Child_Details"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property Parent_DocId As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Parent_DocId"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Parent_DocId") = value
        End Set

    End Property

    Property Sr_number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Sr_number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Sr_number") = value
        End Set

    End Property

    Property Material_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material_Code") = value
        End Set

    End Property

    Property Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Description") = value
        End Set

    End Property

    Property Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Qty") = value
        End Set

    End Property

    Property UOM As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("UOM"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("UOM") = value
        End Set

    End Property

    Property Batch_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Batch_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Batch_Number") = value
        End Set

    End Property

    Property Stor_Loc As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Stor_Loc"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Stor_Loc") = value
        End Set

    End Property

    Property Plant_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Code") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inDocID As String)
        Clear(con)
        Try
            vDr.Item("DocID") = inDocID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_SCPO_Log_Master
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_SCPO_Log_Master"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property Vendor_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Code") = value
        End Set

    End Property

    Property Doc_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Doc_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Doc_Date") = value
        End Set

    End Property

    Property Doc_Date_RTC As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Doc_Date_RTC"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Doc_Date_RTC") = value
        End Set

    End Property

    Property PO_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PO_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PO_Number") = value
        End Set

    End Property

    Property PO_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("PO_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("PO_Date") = value
        End Set

    End Property

    Property User_Created As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("User_Created"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("User_Created") = value
        End Set

    End Property

    Property User_Reprinted As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("User_Reprinted"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("User_Reprinted") = value
        End Set

    End Property

    Property Created_DateTime As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Created_DateTime"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Created_DateTime") = value
        End Set

    End Property

    Property Modified_DateTime As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Modified_DateTime"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Modified_DateTime") = value
        End Set

    End Property

    Property ASN_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ASN_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ASN_Number") = value
        End Set

    End Property

    Property Delivery_note_number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Delivery_note_number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Delivery_note_number") = value
        End Set

    End Property

    Property Vendor_Name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Name") = value
        End Set

    End Property

    Property Is_Cancelled As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Is_Cancelled"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Is_Cancelled") = value
        End Set

    End Property

    Property Cancelled_By As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Cancelled_By"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Cancelled_By") = value
        End Set

    End Property

    Property Cancellation_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Cancellation_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Cancellation_Date") = value
        End Set

    End Property

    Property Cancellation_Date_RTC As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Cancellation_Date_RTC"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Cancellation_Date_RTC") = value
        End Set

    End Property
    Property Vehicle_No As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vehicle_No"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vehicle_No") = value
        End Set

    End Property
    Property Transporter_Name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Transporter_Name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Transporter_Name") = value
        End Set

    End Property
    Property Packing_Details As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Packing_Details"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Packing_Details") = value
        End Set

    End Property
    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inDocID As String)
        Clear(con)
        Try
            vDr.Item("DocID") = inDocID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_SCPO_Log_Parent_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_SCPO_Log_Parent_Details"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property PO_DocId As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PO_DocId"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PO_DocId") = value
        End Set

    End Property

    Property Sr_number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Sr_number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Sr_number") = value
        End Set

    End Property

    Property Material_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material_Code") = value
        End Set

    End Property

    Property Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Description") = value
        End Set

    End Property

    Property Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Qty") = value
        End Set

    End Property

    Property UOM As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("UOM"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("UOM") = value
        End Set

    End Property

    Property Batch_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Batch_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Batch_Number") = value
        End Set

    End Property

    Property Stor_Loc As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Stor_Loc"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Stor_Loc") = value
        End Set

    End Property

    Property Plant_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Code") = value
        End Set

    End Property

    Property Rate_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Rate_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Rate_Actual") = value
        End Set

    End Property

    Property Rate_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Rate_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Rate_Expected") = value
        End Set

    End Property

    Property Grand_Total_Actual As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Grand_Total_Actual"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Grand_Total_Actual") = value
        End Set

    End Property

    Property Grand_Total_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Grand_Total_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Grand_Total_Expected") = value
        End Set

    End Property
    Property VAT_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("VAT_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("VAT_Expected") = value
        End Set

    End Property

    Property DISCOUNT_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("DISCOUNT_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("DISCOUNT_Expected") = value
        End Set

    End Property

    Property PKG_FOR_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("PKG_FOR_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("PKG_FOR_Expected") = value
        End Set

    End Property

    Property FREIGHT_Expected As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("FREIGHT_Expected"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("FREIGHT_Expected") = value
        End Set

    End Property
    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inDocID As String)
        Clear(con)
        Try
            vDr.Item("DocID") = inDocID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_SEPO_Log_Child_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_SEPO_Log_Child_Details"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property Parent_DocId As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Parent_DocId"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Parent_DocId") = value
        End Set

    End Property

    Property Sr_number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Sr_number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Sr_number") = value
        End Set

    End Property

    Property Material_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material_Code") = value
        End Set

    End Property

    Property Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Description") = value
        End Set

    End Property

    Property Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Qty") = value
        End Set

    End Property

    Property Plant_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Code") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inDocID As String)
        Clear(con)
        Try
            vDr.Item("DocID") = inDocID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_SEPO_Log_Master
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_SEPO_Log_Master"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property Vendor_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Code") = value
        End Set

    End Property

    Property Doc_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Doc_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Doc_Date") = value
        End Set

    End Property

    Property Doc_Date_RTC As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Doc_Date_RTC"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Doc_Date_RTC") = value
        End Set

    End Property

    Property PO_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PO_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PO_Number") = value
        End Set

    End Property

    Property PO_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("PO_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("PO_Date") = value
        End Set

    End Property

    Property User_Created As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("User_Created"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("User_Created") = value
        End Set

    End Property

    Property User_Reprinted As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("User_Reprinted"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("User_Reprinted") = value
        End Set

    End Property

    Property Created_DateTime As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Created_DateTime"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Created_DateTime") = value
        End Set

    End Property

    Property Modified_DateTime As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Modified_DateTime"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Modified_DateTime") = value
        End Set

    End Property

    Property ASN_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ASN_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ASN_Number") = value
        End Set

    End Property

    Property Delivery_note_number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Delivery_note_number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Delivery_note_number") = value
        End Set

    End Property

    Property Vendor_Name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Name") = value
        End Set

    End Property

    Property Is_Cancelled As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Is_Cancelled"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Is_Cancelled") = value
        End Set

    End Property

    Property Cancelled_By As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Cancelled_By"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Cancelled_By") = value
        End Set

    End Property

    Property Cancellation_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Cancellation_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Cancellation_Date") = value
        End Set

    End Property

    Property Cancellation_Date_RTC As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Cancellation_Date_RTC"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Cancellation_Date_RTC") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inDocID As String)
        Clear(con)
        Try
            vDr.Item("DocID") = inDocID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_SEPO_Log_Parent_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_SEPO_Log_Parent_Details"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property PO_DocId As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PO_DocId"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PO_DocId") = value
        End Set

    End Property

    Property Sr_number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Sr_number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Sr_number") = value
        End Set

    End Property

    Property ServiceText As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ServiceText"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ServiceText") = value
        End Set

    End Property

    Property Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Qty") = value
        End Set

    End Property

    Property Plant_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Code") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inDocID As String)
        Clear(con)
        Try
            vDr.Item("DocID") = inDocID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_UnBlock_ASN_Log
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_UnBlock_ASN_Log"
        vKeyFieldsString = "Unblocked_By"
        vKeyElementCount = 0
    End Sub

    Property Unblocked_By As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Unblocked_By"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Unblocked_By") = value
        End Set

    End Property

    Property Unblock_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Unblock_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Unblock_Date") = value
        End Set

    End Property

    Property ASN_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ASN_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ASN_Number") = value
        End Set

    End Property

    Property Unblock_Date_RTC As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Unblock_Date_RTC"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Unblock_Date_RTC") = value
        End Set

    End Property

    Property PO_Type As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PO_Type"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PO_Type") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inUnblocked_By As String)
        Clear(con)
        Try
            vDr.Item("Unblocked_By") = inUnblocked_By

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_Vendor_BankDetails
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_Vendor_BankDetails"
        vKeyFieldsString = "VendorId,Bank_Account"
        vKeyElementCount = 2
    End Sub

    Property VendorId As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("VendorId"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("VendorId") = value
        End Set

    End Property

    Property Bank_Account As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Bank_Account"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Bank_Account") = value
        End Set

    End Property

    Property Bank_Name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Bank_Name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Bank_Name") = value
        End Set

    End Property

    Property Account_Holder_Name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Account_Holder_Name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Account_Holder_Name") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inVendorId As Decimal, ByVal inBank_Account As String)
        Clear(con)
        Try
            vDr.Item("VendorId") = inVendorId
            vDr.Item("Bank_Account") = inBank_Account

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_Vendor_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_Vendor_Details"
        vKeyFieldsString = "VendId"
        vKeyElementCount = 0
    End Sub

    Property VendId As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("VendId"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("VendId") = value
        End Set

    End Property

    Property VendorCode As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorCode"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorCode") = value
        End Set

    End Property

    Property VendorName As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorName"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorName") = value
        End Set

    End Property

    Property VendorEmail As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorEmail"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorEmail") = value
        End Set

    End Property

    Property VendorPhone As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorPhone"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorPhone") = value
        End Set

    End Property

    Property ContactPerson As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ContactPerson"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ContactPerson") = value
        End Set

    End Property

    Property FaxNo As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("FaxNo"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("FaxNo") = value
        End Set

    End Property

    Property Address As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Address"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Address") = value
        End Set

    End Property

    Property VATNo As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VATNo"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VATNo") = value
        End Set

    End Property

    Property PANNo As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PANNo"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PANNo") = value
        End Set

    End Property

    Property Country As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Country"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Country") = value
        End Set

    End Property

    Property VendorContact As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorContact"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorContact") = value
        End Set

    End Property

    Property RegDetails As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("RegDetails"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("RegDetails") = value
        End Set

    End Property

    Property RegReason As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("RegReason"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("RegReason") = value
        End Set

    End Property

    Property VendorPCName As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorPCName"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorPCName") = value
        End Set

    End Property

    Property VendorPassword As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorPassword"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorPassword") = value
        End Set

    End Property

    Property IsApproved As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("IsApproved"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("IsApproved") = value
        End Set

    End Property

    Property ResetPassword As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ResetPassword"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ResetPassword") = value
        End Set

    End Property

    Property ResetStatus As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ResetStatus"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ResetStatus") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function
    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inVendId As Decimal)
        Clear(con)
        Try
            vDr.Item("VendId") = inVendId

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'===============================
Public Class tblET_VEND_DETAIL
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "ET_VEND_DETAIL"
        vKeyFieldsString = "VEND_NO"
        vKeyElementCount = 0
    End Sub

    Property VEND_NO As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VEND_NO"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VEND_NO") = value
        End Set

    End Property

    Property NAME As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("NAME"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("NAME") = value
        End Set

    End Property

    Property STREET As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("STREET"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("STREET") = value
        End Set

    End Property

    Property CITY As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("CITY"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("CITY") = value
        End Set

    End Property

    Property POST_CODE As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("POST_CODE"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("POST_CODE") = value
        End Set

    End Property

    Property VendorPhone As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorPhone"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorPhone") = value
        End Set

    End Property

    Property ContactPerson As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ContactPerson"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ContactPerson") = value
        End Set

    End Property

    Property FaxNo As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("FaxNo"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("FaxNo") = value
        End Set

    End Property

    Property VATNo As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VATNo"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VATNo") = value
        End Set

    End Property

    Property PANNo As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PANNo"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PANNo") = value
        End Set

    End Property

    Property Country As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Country"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Country") = value
        End Set

    End Property

    Property VendorContact As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorContact"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorContact") = value
        End Set

    End Property

    Property RegDetails As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("RegDetails"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("RegDetails") = value
        End Set

    End Property

    Property RegReason As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("RegReason"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("RegReason") = value
        End Set

    End Property

    Property VendorEmail As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorEmail"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorEmail") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inVEND_NO As String)
        Clear(con)
        Try
            vDr.Item("VEND_NO") = inVEND_NO

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'==================================
Public Class tblET_VEND_OTHERS
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "ET_VEND_OTHERS"
        vKeyFieldsString = "LIFNR"
        vKeyElementCount = 0
    End Sub

    Property LIFNR As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("LIFNR"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("LIFNR") = value
        End Set

    End Property

    Property VendorPassword As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VendorPassword"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VendorPassword") = value
        End Set

    End Property

    Property IsApproved As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("IsApproved"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("IsApproved") = value
        End Set

    End Property

    Property ResetPassword As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ResetPassword"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ResetPassword") = value
        End Set

    End Property

    Property ResetStatus As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ResetStatus"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ResetStatus") = value
        End Set

    End Property
    Property IsManufacturer As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("IsManufacturer"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("IsManufacturer") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inLIFNR As String)
        Clear(con)
        Try
            vDr.Item("LIFNR") = inLIFNR

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'===============================
Public Class tblET_VEND_BANK
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "ET_VEND_BANK"
        vKeyFieldsString = "LIFNR"
        vKeyElementCount = 0
    End Sub

    Property LIFNR As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("LIFNR"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("LIFNR") = value
        End Set

    End Property

    Property BANK_NAME As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("BANK_NAME"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("BANK_NAME") = value
        End Set

    End Property

    Property BANK_ACC As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("BANK_ACC"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("BANK_ACC") = value
        End Set

    End Property

    Property ACC_NAME As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ACC_NAME"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ACC_NAME") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inLIFNR As String)
        Clear(con)
        Try
            vDr.Item("LIFNR") = inLIFNR

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_Vendor_Plant_Relation
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_Vendor_Plant_Relation"
        vKeyFieldsString = "Vendor_Code,Plant_Code"
        vKeyElementCount = 2
    End Sub

    Property Vendor_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Code") = value
        End Set

    End Property

    Property Plant_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Code") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inVendor_Code As String, ByVal inPlant_Code As String)
        Clear(con)
        Try
            vDr.Item("Vendor_Code") = inVendor_Code
            vDr.Item("Plant_Code") = inPlant_Code

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


'======================
Public Class tbl_GEDetail
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_GEDetail"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property GE_DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("GE_DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("GE_DocID") = value
        End Set

    End Property

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property SrNo As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("SrNo"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("SrNo") = value
        End Set

    End Property

    Property Material_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material_Code") = value
        End Set

    End Property

    Property Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Description") = value
        End Set

    End Property

    Property UOM As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("UOM"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("UOM") = value
        End Set

    End Property

    Property Quantity As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Quantity"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Quantity") = value
        End Set

    End Property

    Property Batch_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Batch_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Batch_Number") = value
        End Set

    End Property

    Property Plant_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Code") = value
        End Set

    End Property

    Property Storage_Location As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Storage_Location"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Storage_Location") = value
        End Set

    End Property

    Property Delivery_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Delivery_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Delivery_Date") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inDocID As String)
        Clear(con)
        Try
            vDr.Item("DocID") = inDocID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

'===========================
Public Class tbl_MM_TAX_CALC_PROC_New
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_MM_TAX_CALC_PROC_New"
        vKeyFieldsString = "DocId"
        vKeyElementCount = 0
    End Sub

    Property DocId As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocId"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocId") = value
        End Set

    End Property

    Property EBELN As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("EBELN"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("EBELN") = value
        End Set

    End Property

    Property EBELP As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("EBELP"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("EBELP") = value
        End Set

    End Property

    Property ASN_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ASN_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ASN_Number") = value
        End Set

    End Property

    Property XPROC As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("XPROC"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("XPROC") = value
        End Set

    End Property
    Property [COUNTER] As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("COUNTER"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("COUNTER") = value
        End Set

    End Property

    Property [STEP] As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("STEP"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("STEP") = value
        End Set

    End Property

    Property CONDTYP As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("CONDTYP"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("CONDTYP") = value
        End Set

    End Property

    Property COND_DESC As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("COND_DESC"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("COND_DESC") = value
        End Set

    End Property

    Property STATICS As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("STATICS"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("STATICS") = value
        End Set

    End Property

    Property STEP_FROM As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("STEP_FROM"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("STEP_FROM") = value
        End Set

    End Property

    Property STEP_TO As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("STEP_TO"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("STEP_TO") = value
        End Set

    End Property

    Property CALC_BASE As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("CALC_BASE"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("CALC_BASE") = value
        End Set

    End Property
    Property UNIT_PRICE As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("UNIT_PRICE"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("UNIT_PRICE") = value
        End Set

    End Property
    Property QUANTITY As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("QUANTITY"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("QUANTITY") = value
        End Set

    End Property
    Property PERCENTAGE_AMT As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("PERCENTAGE_AMT"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("PERCENTAGE_AMT") = value
        End Set

    End Property

    Property AMOUNT As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return val(Convert.ToString(DBNulls.NumberValue(vDr.Item("AMOUNT"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("AMOUNT") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inEmail_Code As String)
        Clear(con)
        Try
            vDr.Item("DocId") = inEmail_Code

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                 Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

'======================
Public Class tbl_GEMaster
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_GEMaster"
        vKeyFieldsString = "DocID"
        vKeyElementCount = 0
    End Sub

    Property DocID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocID") = value
        End Set

    End Property

    Property GE_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("GE_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("GE_Number") = value
        End Set

    End Property

    Property GE_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("GE_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("GE_Date") = value
        End Set

    End Property

    Property Vendor_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Code") = value
        End Set

    End Property

    Property User_Created As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("User_Created"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("User_Created") = value
        End Set

    End Property

    Property User_Modified As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("User_Modified"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("User_Modified") = value
        End Set

    End Property

    Property Created_DateTime As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Created_DateTime"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Created_DateTime") = value
        End Set

    End Property

    Property Modified_DateTime As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Modified_DateTime"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Modified_DateTime") = value
        End Set

    End Property

    Property Plant_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Code") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, ByVal inDocID As String)
        Clear(con)
        Try
            vDr.Item("DocID") = inDocID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(Con, vsql)
            If vDr Is Nothing Then
                Clear(Con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_SetClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

Public Class tbl_Vendor_Invoice_Information
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_Vendor_Invoice_Information"
        vKeyFieldsString = "Vendor_Code"
        vKeyElementCount = 0
    End Sub

    Property Vendor_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Code") = value
        End Set

    End Property

    Property ECC_No As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("ECC_No"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("ECC_No") = value
        End Set

    End Property

    Property Excise_Reg_No As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Excise_Reg_No"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Excise_Reg_No") = value
        End Set

    End Property

    Property Excise_Range As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Excise_Range"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Excise_Range") = value
        End Set

    End Property

    Property Excise_Division As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Excise_Division"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Excise_Division") = value
        End Set

    End Property

    Property CST_No As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("CST_No"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("CST_No") = value
        End Set

    End Property

    Property LST_No As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("LST_No"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("LST_No") = value
        End Set

    End Property

    Property PAN_No As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PAN_No"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PAN_No") = value
        End Set

    End Property

    Property Commission_Rate As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Commission_Rate"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Commission_Rate") = value
        End Set

    End Property

    Property Exc_Ind_Ve As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Exc_Ind_Ve"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Exc_Ind_Ve") = value
        End Set

    End Property

    Property SSI_Status As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("SSI_Status"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("SSI_Status") = value
        End Set

    End Property

    Property Vendor_Type As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Type"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Type") = value
        End Set

    End Property

    Property Vendor_Type_Desc As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Type_Desc"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Type_Desc") = value
        End Set

    End Property

    Property IsCenvat As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("IsCenvat"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("IsCenvat") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inVendor_Code As String)
        Clear(con)
        Try
            vDr.Item("Vendor_Code") = inVendor_Code

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


Public Class tbl_Activity
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_Activity"
        vKeyFieldsString = "DocId"
        vKeyElementCount = 0
    End Sub

    Property DocId As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DocId"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DocId") = value
        End Set

    End Property

    Property PO_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("PO_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("PO_Number") = value
        End Set

    End Property

    Property Transaction_Type As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Transaction_Type"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Transaction_Type") = value
        End Set

    End Property

    Property Transaction_Number As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Transaction_Number"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Transaction_Number") = value
        End Set

    End Property

    Property Line_Item_No As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Line_Item_No"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Line_Item_No") = value
        End Set

    End Property

    Property Old_Value As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Old_Value"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Old_Value") = value
        End Set

    End Property

    Property New_Value As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("New_Value"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("New_Value") = value
        End Set

    End Property
    Property Vendor_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Code") = value
        End Set

    End Property
    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inVendor_Code As String)
        Clear(con)
        Try
            vDr.Item("DocId") = inVendor_Code

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function


End Class

Public Class tbl_material_chapter_relation
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_material_chapter_relation"
        vKeyFieldsString = "material_ID,plant_ID"
        vKeyElementCount = 2
    End Sub

    Property material_ID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("material_ID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("material_ID") = value
        End Set

    End Property

    Property plant_ID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("plant_ID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("plant_ID") = value
        End Set

    End Property

    Property chapter_ID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("chapter_ID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("chapter_ID") = value
        End Set

    End Property

    Property material_desc As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("material_desc"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("material_desc") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inmaterial_ID As String, inplant_ID As String)
        Clear(con)
        Try
            vDr.Item("material_ID") = inmaterial_ID
            vDr.Item("plant_ID") = inplant_ID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

Public Class tblDocument_Manager
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tblDocument_Manager"
        vKeyFieldsString = "Doc_ID"
        vKeyElementCount = 0
    End Sub

    Property Doc_ID As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Doc_ID"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Doc_ID") = value
        End Set

    End Property

    Property Vendor_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Code") = value
        End Set

    End Property

    Property Upload_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Upload_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Upload_Date") = value
        End Set

    End Property

    Property File_Url As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("File_Url"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("File_Url") = value
        End Set

    End Property

    Property Doc_Type As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Doc_Type"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Doc_Type") = value
        End Set

    End Property

    Property File_Display_Name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("File_Display_Name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("File_Display_Name") = value
        End Set

    End Property
    Property Remark As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Remark"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Remark") = value
        End Set

    End Property

    Property Reference_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Reference_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Reference_Date") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inmaterial_ID As String)
        Clear(con)
        Try
            vDr.Item("Doc_ID") = inmaterial_ID


            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

Public Class tblDoc_GRHistory
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tblDoc_GRHistory"
        vKeyFieldsString = "Plant"
        vKeyElementCount = 0
    End Sub

    Property Plant As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant") = value
        End Set

    End Property

    Property Material As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material") = value
        End Set

    End Property

    Property Material_Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material_Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material_Description") = value
        End Set

    End Property

    Property Qty_in_Un_of_Entry As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Qty_in_Un_of_Entry"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Qty_in_Un_of_Entry") = value
        End Set

    End Property

    Property Material_Document As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material_Document"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material_Document") = value
        End Set

    End Property

    Property Posting_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Posting_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Posting_Date") = value
        End Set

    End Property

    Property Vendor As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Vendor"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Vendor") = value
        End Set

    End Property

    Property Unit_of_Entry As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Unit_of_Entry"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Unit_of_Entry") = value
        End Set

    End Property

    Property Purchase_Order As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Purchase_Order"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Purchase_Order") = value
        End Set

    End Property

    Property Reference As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Reference"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Reference") = value
        End Set

    End Property

    Property Reservation As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Reservation"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Reservation") = value
        End Set

    End Property

    Property Movement_Type As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Movement_Type"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Movement_Type") = value
        End Set

    End Property

    Property User_name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("User_name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("User_name") = value
        End Set

    End Property

    Property Storage_Location As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Storage_Location"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Storage_Location") = value
        End Set

    End Property

    Property Amount_in_LC As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Amount_in_LC"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Amount_in_LC") = value
        End Set

    End Property

    Property Document_Header_Text As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Document_Header_Text"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Document_Header_Text") = value
        End Set

    End Property

    Property Cost_Center As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Cost_Center"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Cost_Center") = value
        End Set

    End Property

    Property Company_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Company_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Company_Code") = value
        End Set

    End Property

    Property Movement_Type_Text As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Movement_Type_Text"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Movement_Type_Text") = value
        End Set

    End Property

    Property Subnumber As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Subnumber"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Subnumber") = value
        End Set

    End Property

    Property Order_Price_Unit As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Order_Price_Unit"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Order_Price_Unit") = value
        End Set

    End Property

    Property Entry_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Entry_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Entry_Date") = value
        End Set

    End Property

    Property Smart_Number As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Smart_Number"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Smart_Number") = value
        End Set

    End Property

    Property Reason_for_Movement As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Reason_for_Movement"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Reason_for_Movement") = value
        End Set

    End Property

    Property Material_Doc_Item As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material_Doc_Item"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material_Doc_Item") = value
        End Set

    End Property

    Property Material_Doc_Year As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Material_Doc_Year"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Material_Doc_Year") = value
        End Set

    End Property

    Property Base_Unit_of_Measure As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Base_Unit_of_Measure"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Base_Unit_of_Measure") = value
        End Set

    End Property

    Property Document_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Document_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Document_Date") = value
        End Set

    End Property
    Property Remark As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Remark"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Remark") = value
        End Set

    End Property

    Property Reference_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Reference_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Reference_Date") = value
        End Set

    End Property

    Property Upload_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Upload_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Upload_Date") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inPlant As String, inMaterial As String, inMaterial_Document As String, inVendor As Decimal)
        Clear(con)
        Try
            vDr.Item("Plant") = inPlant
            vDr.Item("Material") = inMaterial
            vDr.Item("Material_Document") = inMaterial_Document
            vDr.Item("Vendor") = inVendor

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class


Public Class tblDoc_Delivery_Schedule
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tblDoc_Delivery_Schedule"
        vKeyFieldsString = "Id"
        vKeyElementCount = 0
    End Sub

    Property Type As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Type"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Type") = value
        End Set

    End Property

    Property Purch_Doc As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Purch_Doc"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Purch_Doc") = value
        End Set

    End Property

    Property Item As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Item"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Item") = value
        End Set

    End Property

    Property Schd As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Schd"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Schd") = value
        End Set

    End Property

    Property Sched_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Sched_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Sched_Qty") = value
        End Set

    End Property

    Property GR_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("GR_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("GR_Qty") = value
        End Set

    End Property

    Property Balance_Qu As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Balance_Qu"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Balance_Qu") = value
        End Set

    End Property

    Property Del_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Del_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Del_Date") = value
        End Set

    End Property

    Property Material As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Material"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Material") = value
        End Set

    End Property

    Property Matl_no As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Matl_no"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Matl_no") = value
        End Set

    End Property

    Property Material_Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Material_Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Material_Description") = value
        End Set

    End Property

    Property Vendor As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Vendor"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Vendor") = value
        End Set

    End Property

    Property Name1 As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Name1"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Name1") = value
        End Set

    End Property

    Property PTm As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("PTm"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("PTm") = value
        End Set

    End Property

    Property GRT As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("GRT"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("GRT") = value
        End Set

    End Property

    Property FZ As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("FZ"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("FZ") = value
        End Set

    End Property
    Property Remark As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Remark"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Remark") = value
        End Set

    End Property

    Property Reference_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Reference_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Reference_Date") = value
        End Set

    End Property

    Property Upload_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Upload_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Upload_Date") = value
        End Set

    End Property


    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inPurch_Doc As Decimal, inDel_Date As DateTime, inMatl_no As Decimal, inVendor As Decimal)
        Clear(con)
        Try
            vDr.Item("Purch_Doc") = inPurch_Doc
            vDr.Item("Del_Date") = inDel_Date
            vDr.Item("Matl_no") = inMatl_no
            vDr.Item("Vendor") = inVendor

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

Public Class tblDoc_GR_Quality_Rating_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tblDoc_GR_Quality_Rating_Details"
        vKeyFieldsString = "Id"
        vKeyElementCount = 0
    End Sub

    Property Plant As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Plant"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Plant") = value
        End Set

    End Property

    Property Plant_Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Description") = value
        End Set

    End Property

    Property Vendor As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Vendor"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Vendor") = value
        End Set

    End Property

    Property Vendor_Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Description") = value
        End Set

    End Property

    Property DUNs_Number As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("DUNs_Number"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("DUNs_Number") = value
        End Set

    End Property

    Property Rejection_Quantity As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Rejection_Quantity"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Rejection_Quantity") = value
        End Set

    End Property

    Property Supplied_Quantity As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Supplied_Quantity"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Supplied_Quantity") = value
        End Set

    End Property

    Property Actual_PPM As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Actual_PPM"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Actual_PPM") = value
        End Set

    End Property

    Property Target_PPM As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Target_PPM"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Target_PPM") = value
        End Set

    End Property

    Property PPM_Marks As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("PPM_Marks"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("PPM_Marks") = value
        End Set

    End Property

    Property Incoming_Or_Line_Complaint As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Incoming_Or_Line_Complaint"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Incoming_Or_Line_Complaint") = value
        End Set

    End Property

    Property Per_CAPA_Closed As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Per_CAPA_Closed"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Per_CAPA_Closed") = value
        End Set

    End Property

    Property CAPA_Closure As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("CAPA_Closure"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("CAPA_Closure") = value
        End Set

    End Property

    Property Warranty_Failure_Or_SIL_Customer_Complain As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Warranty_Failure_Or_SIL_Customer_Complain"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Warranty_Failure_Or_SIL_Customer_Complain") = value
        End Set

    End Property

    Property DOL As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("DOL"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("DOL") = value
        End Set

    End Property

    Property Quality_Rating As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Quality_Rating"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Quality_Rating") = value
        End Set

    End Property

    Property Quality_Rating_in_Per As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Quality_Rating_in_Per"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Quality_Rating_in_Per") = value
        End Set

    End Property

    Property Sync_Ratio As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Sync_Ratio"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Sync_Ratio") = value
        End Set

    End Property

    Property Window_Time_Adherence As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Window_Time_Adherence"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Window_Time_Adherence") = value
        End Set

    End Property

    Property Window_Qty_Adherence As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Window_Qty_Adherence"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Window_Qty_Adherence") = value
        End Set

    End Property

    Property Line_Stoppages_No_of_incidence As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Line_Stoppages_No_of_incidence"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Line_Stoppages_No_of_incidence") = value
        End Set

    End Property

    Property Premium_Freight_Incidences As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Premium_Freight_Incidences"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Premium_Freight_Incidences") = value
        End Set

    End Property

    Property Delivery_Rating As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Delivery_Rating"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Delivery_Rating") = value
        End Set

    End Property

    Property Delivery_Rating_In_Per As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Delivery_Rating_In_Per"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Delivery_Rating_In_Per") = value
        End Set

    End Property

    Property Overall_Rating As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Overall_Rating"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Overall_Rating") = value
        End Set

    End Property
    Property Remark As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Remark"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Remark") = value
        End Set

    End Property

    Property Reference_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Reference_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Reference_Date") = value
        End Set

    End Property

    Property Upload_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Upload_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Upload_Date") = value
        End Set

    End Property


    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inPlant As Decimal, inVendor As Decimal)
        Clear(con)
        Try
            vDr.Item("Plant") = inPlant
            vDr.Item("Vendor") = inVendor

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

Public Class tblDoc_Line_Rejection_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tblDoc_Line_Rejection_Details"
        vKeyFieldsString = "Id"
        vKeyElementCount = 0
    End Sub

    Property Plant As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Plant"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Plant") = value
        End Set

    End Property

    Property Plant_Description As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Plant_Description"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Plant_Description") = value
        End Set

    End Property

    Property Vendor As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Vendor"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Vendor") = value
        End Set

    End Property

    Property Vendor_Description As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Vendor_Description"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Vendor_Description") = value
        End Set

    End Property

    Property Material As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Material"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Material") = value
        End Set

    End Property

    Property Material_Description As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Material_Description"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Material_Description") = value
        End Set

    End Property

    Property Sup_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Sup_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Sup_Qty") = value
        End Set

    End Property

    Property GR_Rej As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("GR_Rej"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("GR_Rej") = value
        End Set

    End Property

    Property Dev_Reje As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Dev_Reje"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Dev_Reje") = value
        End Set

    End Property

    Property Dest_Rej As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Dest_Rej"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Dest_Rej") = value
        End Set

    End Property

    Property GR_Rej_PPM As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("GR_Rej_PPM"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("GR_Rej_PPM") = value
        End Set

    End Property

    Property Line_Rej As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Line_Rej"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Line_Rej") = value
        End Set

    End Property

    Property Warr_Ret As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Warr_Ret"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Warr_Ret") = value
        End Set

    End Property

    Property Total_Rej As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Total_Rej"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Total_Rej") = value
        End Set

    End Property

    Property Total_PPM As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Total_PPM"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Total_PPM") = value
        End Set

    End Property

    Property Short_Text As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Short_Text"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Short_Text") = value
        End Set

    End Property
    Property Remark As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Remark"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Remark") = value
        End Set

    End Property

    Property Reference_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Reference_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Reference_Date") = value
        End Set

    End Property

    Property Upload_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Upload_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Upload_Date") = value
        End Set

    End Property


    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inPlant As Decimal, inVendor As Decimal, inMaterial As Decimal)
        Clear(con)
        Try
            vDr.Item("Plant") = inPlant
            vDr.Item("Vendor") = inVendor
            vDr.Item("Material") = inMaterial

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

Public Class tblDoc_Share_Of_Business_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tblDoc_Share_Of_Business_Details"
        vKeyFieldsString = "Id"
        vKeyElementCount = 0
    End Sub

    Property Quota_Arrangement As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Quota_Arrangement"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Quota_Arrangement") = value
        End Set

    End Property

    Property Quota_Arr_Item As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Quota_Arr_Item"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Quota_Arr_Item") = value
        End Set

    End Property

    Property Procurement_Type As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Procurement_Type"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Procurement_Type") = value
        End Set

    End Property

    Property Special_Procurement As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Special_Procurement"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Special_Procurement") = value
        End Set

    End Property

    Property Vendor As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Vendor"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Vendor") = value
        End Set

    End Property

    Property Procurement_Plant As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Procurement_Plant"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Procurement_Plant") = value
        End Set

    End Property

    Property Quota As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Quota"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Quota") = value
        End Set

    End Property

    Property Quota_Base_Quantity As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Quota_Base_Quantity"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Quota_Base_Quantity") = value
        End Set

    End Property

    Property Allocated_Quantity As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Allocated_Quantity"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Allocated_Quantity") = value
        End Set

    End Property

    Property Maximum_Quantity As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Maximum_Quantity"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Maximum_Quantity") = value
        End Set

    End Property

    Property Production_Version As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Production_Version"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Production_Version") = value
        End Set

    End Property

    Property Maximum_Lot_Size As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Maximum_Lot_Size"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Maximum_Lot_Size") = value
        End Set

    End Property

    Property Minimum_Lot_size As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Minimum_Lot_size"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Minimum_Lot_size") = value
        End Set

    End Property

    Property Rounding_Profile As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Rounding_Profile"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Rounding_Profile") = value
        End Set

    End Property

    Property Indicator_Once_Only As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Indicator_Once_Only"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Indicator_Once_Only") = value
        End Set

    End Property

    Property Max_Release_Qty As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Max_Release_Qty"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Max_Release_Qty") = value
        End Set

    End Property

    Property Period_For_Release_Quantity As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Period_For_Release_Quantity"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Period_For_Release_Quantity") = value
        End Set

    End Property

    Property Number_Of_Periods As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Number_Of_Periods"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Number_Of_Periods") = value
        End Set

    End Property

    Property Priority As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Priority"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Priority") = value
        End Set

    End Property

    Property MPN_Material As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("MPN_Material"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("MPN_Material") = value
        End Set

    End Property

    Property Planned_Deliv_Time As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("Planned_Deliv_Time"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("Planned_Deliv_Time") = value
        End Set

    End Property
    Property Remark As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Remark"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Remark") = value
        End Set

    End Property

    Property Reference_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Reference_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Reference_Date") = value
        End Set

    End Property

    Property Upload_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Upload_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Upload_Date") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inVendor As Decimal)
        Clear(con)
        Try
            vDr.Item("Vendor") = inVendor

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

Public Class tblTBL_CHANGE_LOG
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "TBL_CHANGE_LOG"
        vKeyFieldsString = "ID"
        vKeyElementCount = 0
    End Sub

    Property ID As Double
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return Val(Convert.ToString(DBNulls.NumberValue(vDr.Item("ID"))))
        End Get

        Set(ByVal value As Double)

            vDr.Item("ID") = value
        End Set

    End Property

    Property [DATE] As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("DATE"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("DATE") = value
        End Set

    End Property

    Property VENDOR As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("VENDOR"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("VENDOR") = value
        End Set

    End Property

    Property REMARK As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("REMARK"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("REMARK") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inID As Int32)
        Clear(con)
        Try
            vDr.Item("ID") = inID

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                If vDr.Table.Columns(i).ColumnName = "ID" Then

                Else
                    VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                    vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                    Strings0.PaddedString( _
                    vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
                End If
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

Public Class tblDoc_Vendor8D_Details
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tblDoc_Vendor8D_Details"
        vKeyFieldsString = "Doc_Id"
        vKeyElementCount = 0
    End Sub

    Property Doc_Id As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Doc_Id"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Doc_Id") = value
        End Set

    End Property

    Property Vendor_Code As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Vendor_Code"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Vendor_Code") = value
        End Set

    End Property

    Property Reference_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Reference_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Reference_Date") = value
        End Set

    End Property

    Property File_Url As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("File_Url"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("File_Url") = value
        End Set

    End Property

    Property Doc_Type As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Doc_Type"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Doc_Type") = value
        End Set

    End Property

    Property File_Display_Name As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("File_Display_Name"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("File_Display_Name") = value
        End Set

    End Property

    Property Record_Date As DateTime
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (((vDr.Item("Record_Date"))))
        End Get

        Set(ByVal value As DateTime)

            vDr.Item("Record_Date") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inVendor_Code As String)
        Clear(con)
        Try
            vDr.Item("Doc_Id") = inVendor_Code

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
               Strings0.PaddedString( _
                    vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class

Public Class tbl_Settings
    Inherits SqlObj
    Private vKeyNameArray() As String
    Private vKeyTypeArray() As String
    Private vNonKeyNameArray() As String
    Private vTableName As String
    Private vKeyFieldsString As String

    Private vKeyValueArray() As Object
    Private vNonKeyValueArray() As Object

    Private vDr As DataRow

    Private vKeyElementCount As Integer
    Public RowsAffected As Integer
    Private i As Integer

    Public Exists As Boolean

    Public Sub New()
        vTableName = "tbl_Settings"
        vKeyFieldsString = "IE_Settngs_Enabled"
        vKeyElementCount = 0
    End Sub

    Property IE_Settngs_Enabled As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("IE_Settngs_Enabled"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("IE_Settngs_Enabled") = value
        End Set

    End Property

    Property Ratio As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Ratio"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Ratio") = value
        End Set

    End Property

    Property Restrict_Qty_REPO As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Restrict_Qty_REPO"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Restrict_Qty_REPO") = value
        End Set

    End Property

    Property Restrict_Qty_SCPO As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Restrict_Qty_SCPO"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Restrict_Qty_SCPO") = value
        End Set

    End Property

    Property Restrict_Qty_SEPO As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Restrict_Qty_SEPO"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Restrict_Qty_SEPO") = value
        End Set

    End Property

    Property Restrict_UnBlockASN As String
        Get
            If vDr Is Nothing Then
                Return ""
            End If
            Return (Convert.ToString(DBNulls.StringValue(vDr.Item("Restrict_UnBlockASN"))))
        End Get

        Set(ByVal value As String)

            vDr.Item("Restrict_UnBlockASN") = value
        End Set

    End Property

    Private Function tbl_TableName() As String
        Return vTableName
    End Function

    Private Function tbl_whereClause() As String
        Dim Vary() As Object = Nothing
        Dim j As Integer
        Dim testary() As String = Split(UCase(vKeyFieldsString), ",")
        Dim vcol As DataColumn
        With vDr.Table
            j = 0
            For Each vcol In .Columns
                If Array.IndexOf(testary, UCase(vcol.ColumnName)) > -1 Then
                    ReDim Preserve Vary(j + 2)
                    Vary(j) = vcol.ColumnName
                    Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(vcol.DataType.ToString))
                    Vary(j + 2) = vDr.Item(vcol.ColumnName).ToString
                    j = j + 3
                End If
            Next
        End With
        tbl_whereClause = Sql.WhereClause(Vary)
    End Function
    Private Function tbl_SetClause() As String
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            j = i * 3
            ReDim Preserve Vary(j + 2)
            Vary(j) = vDr.Table.Columns(i).ColumnName
            Vary(j + 1) = vDr.Table.Columns(i).DataType
            Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
            Vary(j + 2) = vDr.Item(i).ToString
        Next
        tbl_SetClause = Sql.SetClause(Vary)
    End Function

    Private Function tbl_setClause_NonPk()
        Dim Vary() As Object = Nothing
        Dim i As Integer
        Dim i_used As Integer
        Dim j As Integer
        For i = 0 To vDr.ItemArray.Length - 1
            If InStr(vKeyFieldsString, vDr.Table.Columns(i).ColumnName) = 0 Then
                j = i_used * 3
                ReDim Preserve Vary(j + 2)
                Vary(j) = vDr.Table.Columns(i).ColumnName
                Vary(j + 1) = vDr.Table.Columns(i).DataType
                Vary(j + 1) = Sql.Datatype_S_N_D_B(UCase(Vary(j + 1).ToString))
                Vary(j + 2) = vDr.Item(i).ToString
                i_used = i_used + 1
            End If
        Next
        tbl_setClause_NonPk = Sql.SetClause(Vary)
    End Function
    Public Sub read(ByRef con As SqlConnection, inIE_Settngs_Enabled As String)
        Clear(con)
        Try
            vDr.Item("IE_Settngs_Enabled") = inIE_Settngs_Enabled

            Dim vsql As String
            vsql = "Select * from " & tbl_TableName() & " WHERE " & tbl_whereClause()
            vDr = Sql.getDataRow(con, vsql)
            If vDr Is Nothing Then
                Clear(con)
                Exists = False
            Else
                Exists = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Update(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "Update " & vTableName & " set " & tbl_setClause_NonPk() & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Insert(ByRef con As SqlConnection)
        Dim VcolsCommaseperated As String = ""
        Dim vcolValuesComaSeperated As String = ""
        Dim i As Integer
        Try
            For i = 0 To vDr.ItemArray.Length - 1
                VcolsCommaseperated = VcolsCommaseperated & "," & vDr.Table.Columns(i).ColumnName
                vcolValuesComaSeperated = vcolValuesComaSeperated & "," & _
                Strings0.PaddedString( _
                vDr.Item(i).ToString, Sql.Datatype_S_N_D_B(vDr.Item(i).GetType.ToString), Sql.DateWithTimeRequired)
            Next
            VcolsCommaseperated = Mid(VcolsCommaseperated, 2)
            vcolValuesComaSeperated = Mid(vcolValuesComaSeperated, 2)
            Dim Vsql As String
            Vsql = "insert into  " & vTableName & _
            " ( " & VcolsCommaseperated & ")" & " values (" & vcolValuesComaSeperated & " )"
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Delete(ByRef con As SqlConnection)
        Try
            Dim Vsql As String
            Vsql = "delete from " & vTableName & " where " & tbl_whereClause()
            Sql.ExecuteSql(con, Vsql)
            RowsAffected = Sql.RowsAffected
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Clear(ByRef con As SqlConnection)
        Dim dt As New DataTable
        dt = Sql.getDatatable(con, "Select * from " & tbl_TableName() & " where 1< 0")
        vDr = dt.NewRow()
        RowsAffected = 0
    End Sub
    Private Function fParametersInfo() As String
        Return tbl_whereClause()
    End Function

End Class
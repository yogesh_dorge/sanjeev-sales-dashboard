﻿Imports System.Data.SqlClient


Public Class Methods
    Inherits SqlObj
    Public CONDTYP_BASE As String = "BASB"
    Public CONDTYP_GROSS1 As String = "P002"
    Public CONDTYP_GROSS2 As String = "P005"
    Public Function fnCalculateTaxStructure(ByRef con As SqlConnection, ByVal EBELN As String, ByVal EBELP As String, ByVal Qty As Double, ByVal linteItemRate As Double, ByVal linteItemTaxCode As String) As DataTable
        Dim ET_MM_TAXSTRUCTURE As New DataTable
        Dim ET_MM_POBASEAMT As New DataTable
        Dim ET_MM_TAX_CALC_PROC As New DataTable
        Dim qry As String = String.Empty

        qry = "SELECT DISTINCT [CTYPE],[RATE],[CURR_PER],[PRICEUNIT] FROM [ET_MM_TAXSTRUCTURE] WHERE TAXCODE = '" & linteItemTaxCode & "' AND FROMDATE <= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "' AND TODATE >= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "'"
        ET_MM_TAXSTRUCTURE = Sql.getDatatable(con, qry)

        qry = "SELECT DISTINCT [CTYPE],[RATE],[CURR_PER],[PRICEUNIT],[CURR_PER],[PRIC_PROC],[RATE_IND],[BASE_ON] FROM [ET_MM_POBASEAMT] WHERE EBELN = '" & EBELN & "' AND EBELP = '" & EBELP & "' AND FROMDATE <= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "' AND TODATE >= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "'"
        ET_MM_POBASEAMT = Sql.getDatatable(con, qry)

        Return getTaxTable(con, ET_MM_TAXSTRUCTURE, ET_MM_POBASEAMT, Qty, linteItemRate, EBELN, EBELP)
        'Return ET_MM_TAX_CALC_PROC
    End Function

    Public Function fnCalculateTaxStructure(ByRef con As SqlConnection, ByVal EBELN As String, ByVal EBELP As String, ByVal Qty As Double, ByVal linteItemRate As Double, ByVal Plant As String, ByVal Vendor As String, ByVal Material As String) As DataTable
        Dim ET_MM_TAXSTRUCTURE As New DataTable
        Dim ET_MM_POBASEAMT As New DataTable
        Dim ET_MM_TAX_CALC_PROC As New DataTable
        Dim linteItemTaxCode As String = String.Empty

        ET_MM_TAXSTRUCTURE = Sql.getDatatable(con, "SELECT [CTYPE],[RATE],[CURR_PER],[PRICEUNIT] FROM [ET_MM_TAXSTRUCTURE] WHERE PLANT = '" & Plant & "' AND LIFNR= '" & Vendor & "' AND MATNR='" & Material & "' AND FROMDATE <= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "' AND TODATE >= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "'")

        ET_MM_POBASEAMT = Sql.getDatatable(con, "SELECT [CTYPE],[RATE],[CURR_PER],[PRICEUNIT],[CURR_PER],[PRIC_PROC],[RATE_IND],[BASE_ON] FROM [ET_MM_POBASEAMT] WHERE EBELN = '" & EBELN & "' AND EBELP = '" & EBELP & "' AND FROMDATE <= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "' AND TODATE >= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "'")

        Return getTaxTable(con, ET_MM_TAXSTRUCTURE, ET_MM_POBASEAMT, Qty, linteItemRate, EBELN, EBELP)
        'Return ET_MM_TAX_CALC_PROC
    End Function

    Public Function fnCalculateTaxStructure(ByRef con As SqlConnection, ByVal EBELN As String, ByVal EBELP As String, ByVal Qty As Double, ByVal linteItemRate As Double, ByVal Plant As String, ByVal MaterialGroup As String) As DataTable
        Dim ET_MM_TAXSTRUCTURE As New DataTable
        Dim ET_MM_POBASEAMT As New DataTable
        Dim ET_MM_TAX_CALC_PROC As New DataTable
        Dim linteItemTaxCode As String = String.Empty

        ET_MM_TAXSTRUCTURE = Sql.getDatatable(con, "SELECT [CTYPE],[RATE],[CURR_PER],[PRICEUNIT] FROM [ET_MM_TAXSTRUCTURE] WHERE PLANT = '" & Plant & "' AND MATGRP='" & MaterialGroup & "' AND FROMDATE <= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "' AND TODATE >= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "'")

        ET_MM_POBASEAMT = Sql.getDatatable(con, "SELECT [CTYPE],[RATE],[CURR_PER],[PRICEUNIT],[CURR_PER],[PRIC_PROC],[RATE_IND],[BASE_ON] FROM [ET_MM_POBASEAMT] WHERE EBELN = '" & EBELN & "' AND EBELP = '" & EBELP & "' AND FROMDATE <= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "' AND TODATE >= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "'")

        Return getTaxTable(con, ET_MM_TAXSTRUCTURE, ET_MM_POBASEAMT, Qty, linteItemRate, EBELN, EBELP)
        'Return ET_MM_TAX_CALC_PROC
    End Function

    Public Function getTaxTable(ByRef con As SqlConnection, ByVal ET_MM_TAXSTRUCTURE As DataTable, ByVal ET_MM_POBASEAMT As DataTable, ByVal Qty As Double, ByVal LineItemRate As Double, ByVal DocNumber As String, ByVal LineItemNo As String) As DataTable
        'Dim ET_MM_TAXSTRUCTURE As New DataTable
        Dim ET_MM_TAX_CALC_PROC As New DataTable

        'Load TaxProcedures
        ET_MM_TAX_CALC_PROC = Sql.getDatatable(con, "SELECT * FROM ET_MM_TAX_CALC_PROC ORDER BY STEP")
        ET_MM_TAX_CALC_PROC.Columns.Add("VALUE_PER")

        Dim DICTIONARY_TAX_STRUCTURE As New Dictionary(Of String, String)


        For Each row As DataRow In ET_MM_POBASEAMT.Rows
            If DICTIONARY_TAX_STRUCTURE.ContainsKey(DBNulls.StringValue(row("CTYPE"))) = False Then
                Dim ctype1, rate1, curr_per1, priceunit1 As String
                ctype1 = DBNulls.StringValue(row("CTYPE"))
                rate1 = DBNulls.StringValue(row("RATE"))
                curr_per1 = DBNulls.StringValue(row("CURR_PER"))
                priceunit1 = DBNulls.StringValue(row("PRICEUNIT"))
                DICTIONARY_TAX_STRUCTURE.Add(ctype1, rate1 & "#" & curr_per1 & "#" & priceunit1)
            End If
        Next


        'SET PO BASED TAX PART
        For Each row As DataRow In ET_MM_TAXSTRUCTURE.Rows
            If DICTIONARY_TAX_STRUCTURE.ContainsKey(DBNulls.StringValue(row("CTYPE"))) = False Then
                Dim ctype1, rate1, curr_per1, priceunit1 As String
                ctype1 = DBNulls.StringValue(row("CTYPE"))
                rate1 = DBNulls.StringValue(row("RATE"))
                curr_per1 = DBNulls.StringValue(row("CURR_PER"))
                priceunit1 = DBNulls.StringValue(row("PRICEUNIT"))
                DICTIONARY_TAX_STRUCTURE.Add(ctype1, rate1 & "#" & curr_per1 & "#" & priceunit1)
            End If
        Next

        'SET COMMON TAX PART
        For i = 0 To ET_MM_TAX_CALC_PROC.Rows.Count - 1
            If DICTIONARY_TAX_STRUCTURE.ContainsKey(DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP"))) Then
                ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT") = DICTIONARY_TAX_STRUCTURE(DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP"))).Split("#").ElementAt(0)
                ET_MM_TAX_CALC_PROC.Rows(i)("VALUE_PER") = DICTIONARY_TAX_STRUCTURE(DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP"))).Split("#").ElementAt(1)
                ET_MM_TAX_CALC_PROC.Rows(i)("UNIT_PRICE") = DICTIONARY_TAX_STRUCTURE(DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP"))).Split("#").ElementAt(2)
            Else
                ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT") = "0"
                ET_MM_TAX_CALC_PROC.Rows(i)("VALUE_PER") = "%"
                ET_MM_TAX_CALC_PROC.Rows(i)("UNIT_PRICE") = "0"
            End If
        Next

        'Assign BASIC value to Initia step
        Dim minStep As String = Sql.GetColumnValue(con, "SELECT MIN(STEP) as STEP FROM ET_MM_TAX_CALC_PROC")
        Dim basicStep As String = Sql.GetColumnValue(con, "SELECT STEP as STEP FROM ET_MM_TAX_CALC_PROC WHERE CONDTYP = 'BASB'")
        'Dictornory will contain each step and its value
        Dim DICTIONARY_STEP_VALUE As New Dictionary(Of Integer, Double)


        'SET Min step to its BASIC Value
        For i = 0 To ET_MM_TAX_CALC_PROC.Rows.Count - 1
            If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("STEP")) = minStep Then
                Dim startingrate As Double
                startingrate = DBNulls.NumberValue(Sql.GetColumnValue(con, "SELECT RATE FROM ET_MM_POBASEAMT WHERE EBELN = '" & DocNumber & "' AND EBELP = '" & LineItemNo & "' AND FROMDATE <= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "' AND TODATE >= '" & Date0.Format_DD_MON_YYYY(DateTime.Now) & "'"))

                ET_MM_TAX_CALC_PROC.Rows(i)("CALCULATED_AMOUNT") = startingrate
                ET_MM_TAX_CALC_PROC.Rows(i)("QUANTITY") = Qty
                DICTIONARY_STEP_VALUE.Add(minStep, startingrate)
                Exit For
            End If
        Next

        For i = 0 To ET_MM_TAX_CALC_PROC.Rows.Count - 1
            'Skip basic value
            If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("STEP")) = minStep Then
                Continue For
            End If

            Dim finalVal As Double
            If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("VALUE_PER")) = "%" Then
                'Execute for percentage
                'Execute same for all next steps
                Dim formula As String = DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CALC_BASE"))
                Dim steps As String()
                steps = formula.Split("+")

                'BELOW "IF" IS FOR TESTING
                If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = "FRBE" Then
                    Debug.Print("TEST")
                End If

                'BELOW "IF" IS FOR TESTING
                If DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT")) <> 0 Or DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = "" Then
                    Dim perAmt As String
                    Dim condt As String
                    perAmt = DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT"))
                    condt = DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP"))
                    Debug.Print("TEST")
                End If

                Dim CalculatedVal As Double = 0
                For Each step1 In steps
                    'BASB Base price then multiply with quantity

                    If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = CONDTYP_BASE Then
                        CalculatedVal = DICTIONARY_STEP_VALUE(DBNulls.NumberValue(step1)) * Qty
                        Exit For
                    Else
                        If DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("STEP")) = DBNulls.NumberValue(step1) Then
                            CalculatedVal = CalculatedVal + DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("CALCULATED_AMOUNT"))
                        Else
                            CalculatedVal = CalculatedVal + DICTIONARY_STEP_VALUE(DBNulls.NumberValue(step1))
                        End If
                    End If

                Next

                If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = "" Or DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = "BASB" Then
                    'No tax is applicable and value is for line calculation
                    'If CalculatedVal < 0 Then
                    '    finalVal = CalculatedVal * -1
                    'Else
                    finalVal = CalculatedVal
                    'End If
                Else
                    'Tax is applicable and value is for tax calculation
                    finalVal = (CalculatedVal / 100) * DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT"))
                End If

                ET_MM_TAX_CALC_PROC.Rows(i)("CALCULATED_AMOUNT") = finalVal

            Else 'EXECUTE AS PER VALUE CALCULATION

                If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = "PKG" Then
                    Debug.Print("TEST")
                End If


                'UNIT PRICE IS BASE UNIT OF CALCULATION, E.G. QTY IS 100, RATE 5 AND "PRICE UNIT" IS 2 THEN RATE IS FOR (100/2)*5
                If DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("UNIT_PRICE")) <> 0 Then
                    'IF STEP = CALCULATION_BASE then do not multiply with quantity in case of PRICE UNIT is <> 0

                    Dim rateInd, baseOn As String

                    'search for rate_ind and base_on
                    For Each row As DataRow In ET_MM_POBASEAMT.Rows
                        If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = DBNulls.StringValue(row("CTYPE")) Then
                            rateInd = DBNulls.StringValue(row("RATE_IND"))
                            baseOn = DBNulls.StringValue(row("BASE_ON"))
                            Exit For
                        End If
                    Next

                    'If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = CONDTYP_GROSS1 Or DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = CONDTYP_GROSS2 Then
                    'If rateind <> "" indicates rate applies on price only no need to multply by qty
                    If rateInd <> "" Then
                        finalVal = (DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT")) / DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("UNIT_PRICE")))
                    Else
                        If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = "R001" Then
                            'Discount based on quantity but substract from total rate e.g. -1rs discount for 100 quantity should deduct -1 only not -100 from rate 115
                            'Final BASB expected (115 - 1)
                            finalVal = (DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT")) / DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("UNIT_PRICE")))
                        Else
                            'UNIT_PRICE has some quantity hence we need to multply same by total line item quantity
                            finalVal = (DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT")) / DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("UNIT_PRICE"))) * Qty
                        End If
                    End If
                Else
                    If DBNulls.StringValue(ET_MM_TAX_CALC_PROC.Rows(i)("CONDTYP")) = "PKG" Then
                        finalVal = DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT")) / Qty
                    Else
                        finalVal = DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("PERCENTAGE_AMT"))
                    End If
                End If

                ET_MM_TAX_CALC_PROC.Rows(i)("CALCULATED_AMOUNT") = finalVal

            End If

            DICTIONARY_STEP_VALUE.Add(DBNulls.NumberValue(ET_MM_TAX_CALC_PROC.Rows(i)("STEP")), finalVal)

        Next

        Return ET_MM_TAX_CALC_PROC


    End Function

End Class

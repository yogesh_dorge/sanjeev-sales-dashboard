﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using Telerik;
using System.Data;

namespace WebApplication1.sales
{
    public partial class frmHolidayMaster : System.Web.UI.Page
    {
        SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        SqlDataAdapter adapt;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowData();
            }
        }

        private void ShowData()
        {
            try
            {
                dt = new DataTable();
                constr.Open();
                adapt = new SqlDataAdapter("select Ydate,Day,IsHoliday from tblHolidays where Ydate between (select CONVERT(DATE,dateadd(dd,-(day(getdate())-1),getdate()))) and (select dateadd(s,-1,dateadd(mm,datediff(m,0,getdate())+1,0)))", constr);
                adapt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            ShowData();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            ShowData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            constr.Open();
            try
            {

                Label date = GridView1.Rows[e.RowIndex].FindControl("lbl_Id") as Label;
                TextBox Day = GridView1.Rows[e.RowIndex].FindControl("Day") as TextBox;
                TextBox Isholiday = GridView1.Rows[e.RowIndex].FindControl("txt_Target") as TextBox;

                SqlCommand cmd = new SqlCommand("Update tblHolidays set IsHoliday='" + Isholiday.Text + "' where Ydate='" + date.Text + "'", constr);
                cmd.ExecuteNonQuery();
                GridView1.EditIndex = -1;
                constr.Close();
                ShowData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }
    }
}
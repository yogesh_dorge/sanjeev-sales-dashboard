﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using Telerik;


namespace WebApplication1.sales
{
    public partial class Yest_Sales : System.Web.UI.Page
    {
        String Monthly = string.Empty;
        String Apr = string.Empty;
        Int32 i = 0;
        string Query1 = "";
        string Query2 = "";
        SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());

        public static DataTable GetInversedDataTable(DataTable table, string columnX, params string[] columnsToIgnore)
        {
            //Create a DataTable to Return
            DataTable returnTable = new DataTable();

            if (columnX == "")
                columnX = table.Columns[0].ColumnName;

            //Add a Column at the beginning of the table

            returnTable.Columns.Add(columnX);

            //Read all DISTINCT values from columnX Column in the provided DataTale
            List<string> columnXValues = new List<string>();

            //Creates list of columns to ignore
            List<string> listColumnsToIgnore = new List<string>();
            if (columnsToIgnore.Length > 0)
                listColumnsToIgnore.AddRange(columnsToIgnore);

            if (!listColumnsToIgnore.Contains(columnX))
                listColumnsToIgnore.Add(columnX);

            foreach (DataRow dr in table.Rows)
            {
                string columnXTemp = dr[columnX].ToString();
                //Verify if the value was already listed
                if (!columnXValues.Contains(columnXTemp))
                {
                    //if the value id different from others provided, add to the list of 
                    //values and creates a new Column with its value.
                    columnXValues.Add(columnXTemp);
                    returnTable.Columns.Add(columnXTemp);
                }
                else
                {
                    //Throw exception for a repeated value
                    throw new Exception("The inversion used must have " +
                                        "unique values for column " + columnX);
                }
            }

            //Add a line for each column of the DataTable

            foreach (DataColumn dc in table.Columns)
            {
                if (!columnXValues.Contains(dc.ColumnName) &&
                    !listColumnsToIgnore.Contains(dc.ColumnName))
                {
                    DataRow dr = returnTable.NewRow();
                    dr[0] = dc.ColumnName;
                    returnTable.Rows.Add(dr);
                }
            }

            //Complete the datatable with the values
            for (int i = 0; i < returnTable.Rows.Count; i++)
            {
                for (int j = 1; j < returnTable.Columns.Count; j++)
                {
                    returnTable.Rows[i][j] =
                      table.Rows[j - 1][returnTable.Rows[i][0].ToString()].ToString();
                }
            }
            return returnTable;
        }

        private void LoadIfHoliday()
        {
            string date = "";
            try
            {
                string Query = "select * from YesterdayHolidaySum";
                string Query2 = "";
                SqlCommand cmd = new SqlCommand(Query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    date = dr.GetValue(0).ToString();
                }

                if (date == "")
                {
                    Query1 = "CAST(dbo.Final_Yest.Y_Sale/ 100000 AS decimal(10, 2))";
                    Query2 = "Col13";
                }
                else
                {
                    Query1 = "cast(dbo.Final_Yest.Y_Sale/ 100000+dbo.Final_Yest.Y_1_Sale/ 100000 as numeric(18,2))";
                    Query2 = "CAST(Col13 as numeric(18,0))+CAST(Col16 as numeric(18,0))";
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadPlantSaleSummaary()
        {
            DataTable dt = new DataTable();
            try
            {
                string dateNow = "";

                dateNow = DateTime.Now.ToShortDateString();
                string query = "SELECT tblPlantMaster.plantName AS Plant, tblSaleMaster_15_16.sale AS [FY 15-16], tblSaleMaster_16_17.sale AS [FY 16-17], tblBudgetMaster.Budget AS [Bud FY 17-18],CAST(tblBudgetMaster.Budget/12 as numeric(18,2)) as [Monthly Bud], EveryMonth.April, EveryMonth.May, EveryMonth.June, EveryMonth.July, EveryMonth.August,EveryMonth.September,(April+May+June+July+August+September) as [YTD Sale] ," + Query1 + " AS [Yest Sale(in Lac)],CAST(Avg_Sale.AvgSale/(select * from DaysCount) as numeric(18,2))AS [Avg/Day(in lac)] FROM         tblPlantMaster INNER JOIN EveryMonth ON tblPlantMaster.plantName = EveryMonth.Plant INNER JOIN tblSaleMaster_16_17 ON tblPlantMaster.plantName = tblSaleMaster_16_17.Plant INNER JOIN tblSaleMaster_15_16 ON tblPlantMaster.plantName = tblSaleMaster_15_16.Plant INNER JOIN tblBudgetMaster ON tblPlantMaster.plantName = tblBudgetMaster.Plant INNER JOIN Final_Yest ON tblPlantMaster.plantName=Final_Yest.PlantGroup INNER JOIN Avg_Sale ON tblPlantMaster.plantName=Avg_Sale.Plant";

                //string query = "select tblPlantMaster.plantName,dbo.tblSaleMaster_15_16.sale AS [FY 15-16],dbo.tblSaleMaster_16_17.sale AS [FY 16-17],dbo.tblBudgetMaster.Budget AS [Bud FY 17-18],CAST(dbo.tblBudgetMaster.Budget / 12 AS numeric(10, 2)) AS [Monthly Bud],April,May,June,July,(April+May+June+July) as YTDSale,CAST(dbo.Final_Yest.Y_Sale/ 100000 AS decimal(10, 2)) AS [Yest Sale(in lac)],CAST(dbo.Avg_Sale.AvgSale /(SELECT DayCount FROM dbo.DaysCount) AS numeric(10, 2)) AS [Avg/Day(in lac)] from tblPlantMaster,EveryMonth,tblSaleMaster_15_16,tblSaleMaster_16_17,tblBudgetMaster,Final_Yest,Avg_Sale where tblPlantMaster.plantName=EveryMonth.Plant and tblPlantMaster.plantName=tblSaleMaster_15_16.Plant and tblPlantMaster.plantName=tblSaleMaster_16_17.Plant and tblPlantMaster.plantName=tblBudgetMaster.Plant and Final_Yest.PlantGroup=tblPlantMaster.plantName and tblPlantMaster.plantName=Avg_Sale.Plant";
                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);

                //Adding inversion code
                double sum = 0, val = 0;
                DataTable invertDT = new DataTable();
                invertDT = GetInversedDataTable(dt, "Plant", "");
                invertDT.Columns.Add("Total");

                for (int i = 0; i < invertDT.Rows.Count; i++)
                {
                    for (int j = 0; j < invertDT.Columns.Count; j++)
                    {
                        if (j != 0)
                        {
                            sum = sum + DBNulls.NumberValue(invertDT.Rows[i][j]);
                        }
                        val = j;
                    }
                    invertDT.Rows[i][invertDT.Columns.Count - 1] = sum;
                    sum = 0;
                }

                string result = string.Empty;

                RadGridPlant.DataSource = invertDT;
                RadGridPlant.DataBind();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadKPlantSaleSummaary()
        {
            DataTable dt = new DataTable();
            try
            {
                string dateNow = "";
                dateNow = DateTime.Now.ToShortDateString();

                string query = "select tblCustomerMaster.CustomerAcronym As Customer,cast(SUM(CAST(Col6 as numeric(18,2)))/100000 as numeric(18,2))  as [Plan],cast(SUM(CAST(Col10 as numeric(18,2)))/100000 as numeric(18,2)) as Actual,CAST(SUM(CAST(dbo.tblSalesDb.Col6 AS numeric(18, 2))) / 100000 AS numeric(18, 2)) - CAST(SUM(CAST(dbo.tblSalesDb.Col10 AS numeric(18, 2))) / 100000 AS numeric(18, 2)) as GAP from tblSalesDb,tblCustomerMaster where Col2=tblCustomerMaster.CustomerCode and Col17='K96' group by tblCustomerMaster.CustomerAcronym";

                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);

                //Adding inversion code
                //DataTable invertDT = new DataTable();
                //invertDT = GetInversedDataTable(dt, "Customer Name", "");
                RadGrid1.DataSource = dt;
                RadGrid1.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadPPlantSaleSummaary()
        {
            DataTable dt = new DataTable();
            try
            {
                string dateNow = "";
                dateNow = DateTime.Now.ToShortDateString();

                string query = "select tblPCustomerMaster.CustomerAcronym Customer,cast(SUM(CAST(Col6 as numeric(18,2)))/100000 as numeric(18,2))  as [Plan],cast(SUM(CAST(Col10 as numeric(18,2)))/100000 as numeric(18,2)) as Actual,CAST(SUM(CAST(dbo.tblSalesDb.Col6 AS numeric(18, 2))) / 100000 AS numeric(18, 2)) - CAST(SUM(CAST(dbo.tblSalesDb.Col10 AS numeric(18, 2))) / 100000 AS numeric(18, 2)) as GAP from tblSalesDb,tblPCustomerMaster where Col2=tblPCustomerMaster.CustomerCode and Col17='P115' group by tblPCustomerMaster.CustomerAcronym";

                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);

                RadGrid2.DataSource = dt;
                RadGrid2.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadMatSummary()
        {
            DataTable dt = new DataTable();
            try
            {
                string dateNow = "";
                dateNow = DateTime.Now.ToShortDateString();

                string query = "select * from SOBPartView";
                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);
                RadGrid4.DataSource = dt;
                RadGrid4.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void LoadBajajSpares()
        {
            DataTable dt = new DataTable();
            try
            {
                string dateNow = "";
                dateNow = DateTime.Now.ToShortDateString();

                string query = "select [Plan],Actual,Percentage from BajajSpares";
                SqlCommand cmd = new SqlCommand(query, constr);
                constr.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dtSch = dr.GetSchemaTable();
                dt.Load(dr);
                RadGrid3.DataSource = dt;
                RadGrid3.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        private void ReadSyncTime()
        {
            try
            {
                constr.Open();
                String Qry = "Select PrimaryColumn from tblSalesDb";
                String Qry2 = "Select * from DaysCount";
                string Qry3 = "select * from TotalWorkdays";
                SqlCommand cmd = new SqlCommand(Qry, constr);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                Label1.Text = "Last Sync: " + dr["PrimaryColumn"].ToString();

                SqlCommand cmd2 = new SqlCommand(Qry2, constr);
                SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                SqlDataReader dr2 = cmd2.ExecuteReader();
                dr2.Read();

                SqlCommand cmd3 = new SqlCommand(Qry3, constr);
                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                SqlDataReader dr3 = cmd3.ExecuteReader();
                dr3.Read();

                rdWorkingDays.Text = "Working Days:-" + dr2["DayCount"].ToString() + "/ " + dr3["TotalDays"].ToString() + "";
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert(" + ex.Message + ")", true);
            }
            finally
            {
                constr.Close();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadIfHoliday();
            LoadPlantSaleSummaary();
            LoadKPlantSaleSummaary();
            LoadPPlantSaleSummaary();
            LoadMatSummary();
            LoadBajajSpares();
            ReadSyncTime();

            string value = string.Empty;
        }

        protected void RadButton1_Click(object sender, EventArgs e)
        {
            //Response.Redirect("");
        }

        protected void RadGrid4_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                TableCell PerDay = dataItem["PerDayReqd"];
                TableCell Avg = dataItem["Average"];
                if (DBNulls.NumberValue(PerDay.Text) > DBNulls.NumberValue(Avg.Text))
                {
                    //dataItem.BackColor = System.Drawing.Color.Red;  
                    dataItem["Average"].ForeColor = System.Drawing.Color.Red;
                    //dataItem.Font.Bold = true;
                }
            }
        }

        protected void RadGridPlant_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            try
            {

                if (e.Item is GridDataItem)
                {

                    GridDataItem item = (GridDataItem)e.Item;
                    string value = item.Cells[3].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
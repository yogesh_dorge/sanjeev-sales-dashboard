﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Yest_Sales.aspx.cs" Inherits="WebApplication1.sales.Yest_Sales" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
 <!-- Meta Tag -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <!-- SEO -->
    <meta name="description" content="150 words">
    <meta name="author" content="uipasta">
    <meta name="url" content="http://www.yourdomainname.com">
    <meta name="copyright" content="company name">
    <meta name="robots" content="index,follow">
    <meta http-equiv="refresh" content="900">
    <head id="Head1" runat="server">
    <title>Sale Dashboard Yesterday</title>
        <script type="text/javascript">
            function BatchEditCellValueChanged(sender, args) {


                var grid = sender;

                var row = args.get_row();
                var cell = args.get_cell();
                var tableView = args.get_tableView();
                var column = args.get_column();
                var columnUniqueName = args.get_columnUniqueName();
                var editorValue = args.get_editorValue();
                var cellValue = args.get_cellValue();
                alert(editorValue);

                var columnlength = grid.get_masterTableView().get_columns().length;
                for (i = 0 ; i < columnlength; i++) {
                    cell = grid.get_masterTableView().get_columns()[i].get_uniqueName();
                    alert(cell);
                }

            }
    </script>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon/favicon.ico">
    <!-- All CSS Plugins -->
    <link rel="stylesheet" type="text/css" href="css/plugin.css">
    <!-- Main CSS Stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Google Web Fonts  -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">
        <style type="text/css">
            .navbar-header {
                text-align: center;
            }
        </style>
</head>
    <body>
        <form id="form1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="jquery.ui.combined" />
            </Scripts>
        </asp:ScriptManager>
            <!--Menu section Start-->
            <div style=" width:100%; padding-bottom:60px;" >
                <table class="col-lg-11">
                    <tr>
                        <td>
                            <img height="30" src="../Images/index.jpg" />
                            
                        </td>
                        <td style="color:yellow">
                            <h2>
                                Sale till 00:00 AM
                            </h2>
                        </td>
                        <td>
                            <ul class=" navbar-right">

                                    <a href="../frmLogin.aspx">Log Out</a>
                                    <br />
                                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
            <!--Menu section End-->

            <!-- About Start -->

        <section id="about" class="about section-space-padding">
            <div class="container">                
                <div class="row">

                </div>
            </div>
            <div class="modal fade subscribe padding-top-120" id="subscribemodal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-offset-2 col-xs-offset-0 col-md-8 col-sm-8"><div class="margin-bottom-50">
                    <form id="mc-form" method="post" action="http://uipasta.us14.list-manage.com/subscribe/post?u=854825d502cdc101233c08a21&amp;id=86e84d44b7">	
						  <div class="subscribe-form">
							 <input id="mc-email" type="email" placeholder="Email Address" class="text-input">
							  <button class="submit-btn" type="submit">Submit</button>
								</div>
								<label for="mc-email" class="mc-label"></label>
							  </form>
                           </div>
                        </div>
                     </div>
                 </div>
             </div>
          </div>
       </div>
            <!-- About End-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <telerik:RadTextBox ID="rdWorkingDays" runat="server" ReadOnly="true" width="200px"></telerik:RadTextBox>
                        <h5><b>Plantwise Sales Summary</b></h5>
                        
                        <telerik:RadGrid ID="RadGridPlant" runat="server" AutoGenerateColumns="false" Font-Size="Large" Skin="Simple" GridLines="Both" OnItemDataBound="RadGridPlant_ItemDataBound">
                            <ClientSettings>
                                <ClientEvents OnBatchEditCellValueChanged="BatchEditCellValueChanged" />
                            </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                                <Columns>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Plant" DataType="System.String" HeaderText="Plant" SortExpression="Plant" UniqueName="Plant">
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="C4" DataType="System.Decimal" HeaderText="C4" SortExpression="C4" UniqueName="C4">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="PNST" DataType="System.Decimal" HeaderText="E89" SortExpression="E89" UniqueName="E89">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="K96" DataType="System.Decimal" HeaderText="K96" SortExpression="K96" UniqueName="K96">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="P115" DataType="System.Decimal" HeaderText="P115" SortExpression="P115" UniqueName="P115">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="X1" DataType="System.Decimal" HeaderText="X1" SortExpression="X1" UniqueName="X1">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn Aggregate="None" DataField="Total" DataType="System.Decimal" HeaderText="Total" SortExpression="Total" UniqueName="Total">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridNumericColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
              </div>

           <div class="col-lg-4">
                <h5>
                    <b>K96 & P115 Plant Sale:=></b>
                </h5>
               <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="false" AllowSorting="true" Font-Size="Medium" Skin="Simple" GridLines="Both" ShowFooter="true">
                   <MasterTableView TableLayout="Fixed">
                       <Columns>
                           <telerik:GridNumericColumn Aggregate="None" DataField="Customer" DataType="System.String" HeaderText="Cust Name K96" SortExpression="Customer Name" UniqueName="Customer_Name">
                               <HeaderStyle Width="30px" />
                           </telerik:GridNumericColumn>
                           <telerik:GridNumericColumn Aggregate="Sum" DataField="Plan" DataType="System.Decimal" HeaderText="Plan" SortExpression="Plan" UniqueName="Plan" FooterAggregateFormatString="{0}">
                               <HeaderStyle Width="20px" />
                           </telerik:GridNumericColumn>
                           <telerik:GridNumericColumn Aggregate="Sum" DataField="Actual" DataType="System.Decimal" HeaderText="Actual" SortExpression="Actual" UniqueName="Actual" FooterAggregateFormatString="{0}">
                               <HeaderStyle Width="20px" />
                           </telerik:GridNumericColumn> 
                           <telerik:GridNumericColumn Aggregate="Sum" DataField="Gap" DataType="System.Decimal" HeaderText="Gap" SortExpression="Gap" UniqueName="Gap" FooterAggregateFormatString="{0}">
                               <HeaderStyle Width="20px" />
                           </telerik:GridNumericColumn>
                       </Columns>
                   </MasterTableView>
               </telerik:RadGrid>
               <telerik:RadGrid ID="RadGrid2" runat="server" AutoGenerateColumns="false" AllowSorting="true" Font-Size="Medium" Skin="Simple" GridLines="Both" ShowFooter="true">
                   <MasterTableView TableLayout="Fixed">
                       <Columns>
                           <telerik:GridNumericColumn Aggregate="None" DataField="Customer" DataType="System.String" HeaderText="Cust Name P115" SortExpression="Customer Name" UniqueName="Customer_Name" FooterAggregateFormatString="{0}">
                               <HeaderStyle Width="30px" />
                           </telerik:GridNumericColumn>
                           <telerik:GridNumericColumn Aggregate="Sum" DataField="Plan" DataType="System.Decimal" HeaderText="Plan" SortExpression="Plan" UniqueName="Plan" FooterAggregateFormatString="{0}">
                               <HeaderStyle Width="20px" />
                           </telerik:GridNumericColumn>
                           <telerik:GridNumericColumn Aggregate="Sum" DataField="Actual" DataType="System.Decimal" HeaderText="Actual" SortExpression="Actual" UniqueName="Actual" FooterAggregateFormatString="{0}">
                               <HeaderStyle Width="20px" />
                           </telerik:GridNumericColumn>
                           <telerik:GridNumericColumn Aggregate="Sum" DataField="Gap" DataType="System.Decimal" HeaderText="Gap" SortExpression="Gap" UniqueName="Gap" FooterAggregateFormatString="{0}">
                               <HeaderStyle Width="20px" />
                           </telerik:GridNumericColumn>
                       </Columns>
                   </MasterTableView>
               </telerik:RadGrid>
          </div>
                    <div class="col-lg-4">
                
          </div>
          <div class="col-lg-4">
              <h5>
                <b>Bajaj Spares in Lacs:=></b>
            </h5>
              <telerik:RadGrid runat="server" ID="RadGrid3" AllowSorting="true" Font-Size="Large" Skin="Simple" GridLines="Both">
                  
              </telerik:RadGrid>
            <h5>
                <b>SOB Part wise Sale:=></b>
            </h5>
              <telerik:RadGrid runat="server" ID="RadGrid4" AutoGenerateColumns="false" AllowSorting="true" Font-Size="Large" Skin="Simple" GridLines="Both" OnItemDataBound="RadGrid4_ItemDataBound">
                  <MasterTableView TableLayout="Fixed">
                      <Columns>
                          <telerik:GridNumericColumn Aggregate="None" DataField="Part" DataType="System.String" HeaderText="Part" SortExpression="Part" UniqueName="Part">
                              <HeaderStyle Width="80px" />
                          </telerik:GridNumericColumn>
                          <telerik:GridNumericColumn Aggregate="None" DataField="Customer Code" DataType="System.String" HeaderText="Cust" SortExpression="Customer" UniqueName="Customer">
                              <HeaderStyle Width="50px" />
                          </telerik:GridNumericColumn>
                          <telerik:GridNumericColumn Aggregate="None" DataField="Desc" DataType="System.String" HeaderText="Desc" SortExpression="Desc" UniqueName="Description">
                              <HeaderStyle Width="100px" />
                          </telerik:GridNumericColumn>
                          <telerik:GridNumericColumn Aggregate="None" DataField="Per Day Reqd" DataType="System.String" HeaderText="Per Day Reqd" SortExpression="Per Day Reqd" UniqueName="PerDayReqd">
                              <HeaderStyle Width="40px" />
                          </telerik:GridNumericColumn>
                          <telerik:GridNumericColumn Aggregate="Sum" DataField="YestDay Sale Qty" DataType="System.Decimal" HeaderText="Yest Sale Qty" SortExpression="Yesterday Sale Qty" UniqueName="YesterdaySaleQty">
                              <HeaderStyle Width="40px" />
                          </telerik:GridNumericColumn>
                          <telerik:GridNumericColumn Aggregate="Sum" DataField="Average" DataType="System.Decimal" HeaderText="Avg" SortExpression="Average" UniqueName="Average">
                              <HeaderStyle Width="40px" />
                          </telerik:GridNumericColumn>
                      </Columns>
                  </MasterTableView>
              </telerik:RadGrid>
          </div>
                </div>
            </div>
        </section>
        <!-- Footer Start -->
        <footer class="footer-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->
        <!-- Back to Top Start -->
        <a href="#" class="scroll-to-top"><i class="icon-arrow-up-circle"></i></a>
        <!-- Back to Top End -->
        <!-- All Javascript Plugins  -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugin.js"></script>
        <!-- Main Javascript File  -->
        <script type="text/javascript" src="js/scripts.js"></script>
        </form>
    </body>
</html>

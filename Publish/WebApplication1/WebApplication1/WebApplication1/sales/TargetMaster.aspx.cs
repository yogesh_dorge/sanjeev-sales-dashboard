﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using Telerik;
using System.Data;

namespace WebApplication1.sales
{
    public partial class TargetMaster : System.Web.UI.Page
    {
        SqlConnection constr = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        SqlDataAdapter adapt;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowData();
            }
        }

        private void ShowData()
        {
            try
            {
                dt = new DataTable();
                constr.Open();
                adapt = new SqlDataAdapter("select ID,Plant,Target from tblDDTarget_PurchaseRatio", constr);
                adapt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }
        
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            ShowData();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            ShowData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            constr.Open();
            try
            {

                Label id = GridView1.Rows[e.RowIndex].FindControl("lbl_Id") as Label;
                TextBox plant = GridView1.Rows[e.RowIndex].FindControl("txt_plant") as TextBox;
                TextBox target = GridView1.Rows[e.RowIndex].FindControl("txt_Target") as TextBox;

                SqlCommand cmd = new SqlCommand("Update tblDDTarget_PurchaseRatio set Plant='" + plant.Text + "',Target='" + target.Text + "' where ID='" + id.Text + "'", constr);
                cmd.ExecuteNonQuery();
                GridView1.EditIndex = -1;
                constr.Close();
                ShowData();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                constr.Close();
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.SessionState;
using System.Net.Mail;
using System.Net;
using Telerik.Web.UI;
using System.Globalization;
using System.Text.RegularExpressions;
using unirest_net.http;
using System.Web.Configuration;

namespace WebApplication1
{
    public partial class frmLogin : System.Web.UI.Page
    {

        #region DECLARATIONS

        string query = String.Empty;

        SqlCommand cmd;
        #endregion

        string login = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            txtUsername.Focus();
            if (!IsPostBack)
            {
                cmbRolecode.Visible = false;
                doOnce();
            }

        }
        private void doOnce()
        {
            lblBuildId.Text = "20170622000000";

            Session.RemoveAll();
            check_Internert();
        }

        private void check_Internert()
        {
            WebClient client = new WebClient();
            byte[] datasize = null;
            try
            {
                datasize = client.DownloadData("http://www.google.com");
            }
            catch (Exception ex)
            {

            }
            if (datasize != null && datasize.Length > 0)
            {
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Internet Connection Not Available.')", true);
                return;
            }
        }

        protected void btn_Login_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtUsername.Text.ToUpper() == WebConfigurationManager.AppSettings["Admin"].ToString().ToUpper())
                {
                    if (txtPassword.Text == WebConfigurationManager.AppSettings["Password"].ToString())
                    {
                        DateTime Today_date = DateTime.Now;
                        string Today_date_rtc = PublicMethods.fnGetUsableRTC_RTC(Today_date);

                        ////string qry = "insert into tbl_UserLogDetails(User_EmailID,User_Name,Role,Datetime,Datetime_RTC) values ('" + txtUsername.Text + "','Admin','Admin','" + Today_date + "','" + Today_date_rtc + "') ";
                        ////cmd = new SqlCommand(qry);
                        ////DBUtils.ExecuteSQLCommand(cmd);

                        Session.Add("LoginUserEmail", txtUsername.Text);
                        Session.Add("Role", WebConfigurationManager.AppSettings["Admin"].ToString().ToUpper());
                        Session.Add("Password", txtPassword.Text);
                        Response.Redirect("~/sales/SaleDashboard.aspx", false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();


                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Password is wrong')", true);
                        return;
                    }

                }


                else
                {

                    if (txtUsername.Text.Contains("@vtelebyte.com"))
                    {

                        DateTime Today_date = DateTime.Now;
                        string Today_date_rtc = PublicMethods.fnGetUsableRTC_RTC(Today_date);

                        ////string qry = "insert into tbl_UserLogDetails(User_EmailID,User_Name,Role,Datetime,Datetime_RTC) values ('" + txtUsername.Text + "','Admin','" + cmbRolecode.SelectedValue + "','" + Today_date + "','" + Today_date_rtc + "') ";
                        ////cmd = new SqlCommand(qry);
                        ////DBUtils.ExecuteSQLCommand(cmd);

                        Session.Add("LoginUserEmail", txtUsername.Text);
                        Session.Add("Role", cmbRolecode.Text);
                        Session.Add("Password", txtPassword.Text);
                        Response.Redirect("~/sales/SaleDashboard.aspx", false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {

                        if (txtUsername.Text.Contains("@sanjeevgroup.com"))
                        {

                        }
                        else
                        {
                            txtUsername.Text = txtUsername.Text + "@sanjeevgroup.com";
                        }

                        bool result = false;
                        bool result1 = false;
                        HttpResponse<string> response = null;
                        try
                        {
                            response = Unirest.get("https://outlook.office365.com/api/v1.0/me/").basicAuth(txtUsername.Text, txtPassword.Text).asJson<string>();
                        }
                        catch (Exception ex)
                        {
                            lblMessage.Text = "Login Failed";
                            return;
                        }
                        if (response.Code == 200)
                            result = true;

                        if (result)
                        {
                            string Name = response.Body.ToString();
                            string[] lines = Name.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                            string line = lines[6].ToString();
                            string[] pair = line.Split(new[] { ',' });
                            string Login_Name = pair[0].Trim().ToString();

                            string qry;
                            System.Data.DataTable dt;
                            DateTime Today_date = DateTime.Now;
                            string Today_date_rtc = PublicMethods.fnGetUsableRTC_RTC(Today_date);

                            DataTable dtrole = new DataTable();


                            //////qry = "insert into tbl_UserLogDetails(User_EmailID,User_Name,Role,Datetime,Datetime_RTC) values ('" + txtUsername.Text + "','" + Login_Name + "','" + cmbRolecode.SelectedValue + "','" + Today_date + "','" + Today_date_rtc + "') ";
                            //////cmd = new SqlCommand(qry);
                            //////DBUtils.ExecuteSQLCommand(cmd);


                            Session.Add("LoginUserEmail", txtUsername.Text);
                            Session.Add("Role", cmbRolecode.Text);
                            Session.Add("Password", txtPassword.Text);
                            Response.Redirect("~/sales/SaleDashboard.aspx", false);
                            HttpContext.Current.ApplicationInstance.CompleteRequest();

                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Email/Password is wrong')", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void txtUsername_TextChanged(object sender, EventArgs e)
        {
            if (txtUsername.Text.ToUpper() == WebConfigurationManager.AppSettings["Admin"].ToString().ToUpper())
            {
                cmbRolecode.Visible = false;
                txtPassword.Focus();
            }
            else
            {
                cmbRolecode.Visible = false;
                txtPassword.Focus();
            }
        }
    }
}
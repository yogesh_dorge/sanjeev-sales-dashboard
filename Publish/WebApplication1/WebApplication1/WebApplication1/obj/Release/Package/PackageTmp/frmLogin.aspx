﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogin.aspx.cs" Inherits="WebApplication1.frmLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="Dashboard" />
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina" />

    <title>LOGIN</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets_responsive/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets_responsive/css/main.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="assets_responsive/css/ie8.css" /><![endif]-->

    <!-- Bootstrap core CSS -->
    
    <link href="Pages/assets/css/bootstrap.css" rel="stylesheet" />
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="Pages/assets/css/style.css" rel="stylesheet" />
      <link href="Pages/assets/css/table-responsive.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <%--<script>
        function InternetCheck() {
            if (navigator.onLine) {
                //alert('online');
            }
            else {
                //window.stop();
                alert('Please Check your Internet Connection');

            }
        }
    </script>--%>

    <style type="text/css">
        .text
        {
            font-style: italic;
        }

        div#home a:link
        {
            color: #2874A6;
        }

        div#home a:visited
        {
            color: #2874A6;
        }

        div#home a:hover
        {
            color: #2874A6;
        }

        div#home a:active
        {
            color: #2874A6;
        }

        body
        {
            background-image: url("Images/Sales_Image.jpg");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>

</head>
<body>
    <%--<form id="form1" runat="server">
    <div>--%>
    <div id="header-wrapper">

        <div id="header" class="container" style="vertical-align: top; text-align: center;">

            <!-- Logo -->
            <%--<h1 id="logo"><a href="index.html">Strongly Typed</a></h1>--%>
            <h1 id="logo"></h1>
        </div>
    </div>
      <div id="login-page"">
        <div class="container">


            <%--   <h1 class="form-login-heading" style="color: white">Budgeted VS Actual Expense Tracking</h1>--%>
            <form class="form-login" id="form2" style="background-color:snow;" runat="server">



                <div class="login-wrap" align="center">
                    <img src="Images/index.jpg" />
                </div>
                <h2 class="form-login-heading">sign in now</h2>
                <div>
                    <asp:TextBox ID="txtUsername" runat="server" AutoPostBack="True" OnTextChanged="txtUsername_TextChanged" CssClass="form-control" ToolTip="Login" placeholder="Login"></asp:TextBox><div>@sanjeevgroup.com</div>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvUsrName" runat="server" ControlToValidate="txtUserName"
                            ErrorMessage="(*) User Email ID is required" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <br />
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password" ToolTip="Password" placeholder="Password"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                            Display="Dynamic" ErrorMessage="(*) Password is required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>

                    <br />
                    <asp:DropDownList runat="server" ID="cmbRolecode" Filter="StartsWith"  DataTextField="RoleCode" CssClass="form-control" TabIndex="3" AutoPostBack="false" EmptyMessage="Select Role"></asp:DropDownList>


                    <div>

                      <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cmbRolecode"
                            Display="Dynamic" ErrorMessage="(*) Role is required" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                    </div>

                    <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="SELECT [RoleCode] FROM [tblRoleMaster]"></asp:SqlDataSource>

                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>

                    <br />
                    <asp:Button ID="btn_Login" runat="server" Text="SIGN IN" OnClick="btn_Login_Click" CssClass="btn btn-theme btn-block" />
                    <hr />
                    <div id="hide_login">
                        <asp:Label ID="lblLogin_Failed" runat="server" Text="Unauthorised credentials" class="alert alert-danger" Visible="false"></asp:Label>
                        <div class="login-social-link centered">
                            <p>
                                BUILD ID :
                                <asp:Label ID="lblBuildId" runat="server" Text="." Visible="True"></asp:Label>
                            </p>

                        </div>
                    </div>

                    <!-- Modal -->
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Forgot Password ?</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Enter your e-mail address below to reset your password.</p>
                                    <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                    <button class="btn btn-theme" type="button">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal -->
                    <br>
            </form>

        </div>
    </div>
    <div id="home" style="text-align: right; color: black; font-family: 'Bookman Old Style'">
        <span class="text">&copy;&nbsp;2017 - <b>SANJEEV</b> - Developed by <a target="_blank" href="http://www.vtelebyte.com"><b>Vtelebyte Software Pvt.Ltd Pune</b></a>
        </span>
    </div>
         <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
   <%--  <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("Images/FANUCrobots.jpg", { speed: 500 });
    </script>--%>
   
    <%--</form>--%>
    <%--</div>
    </form>--%>
</body>
</html>

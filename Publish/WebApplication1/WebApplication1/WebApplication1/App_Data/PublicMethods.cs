﻿using System;
using System.Collections.Generic;
using System.Web;
using BusinessLogic;
using System.Data.SqlClient;
using System.Configuration;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;

/// <summary>
/// Summary description for PublicMethods
/// </summary>
public class PublicMethods
{
    private string connquery = "";
   
    public PublicMethods()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void assignConnectionString(ref SqlConnection connectionObj)
    {
        try
        {
            if (connectionObj == null)
                return;
            if (connectionObj.State == System.Data.ConnectionState.Open)
            {
                SqlObj.Sql.CommitTran(ref connectionObj);
                return;
            }
            else if (connectionObj.State == System.Data.ConnectionState.Closed)
                connectionObj.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            SqlObj.Sql.ExecuteSql(ref connectionObj, "SET DATEFORMAT DMY");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public static string fnGetUsableRTC(System.DateTime vDate)
    {
        return string.Format("{0:yyyyMMddHHmmssfff}", vDate);
    }
    public static string fnGetUsableRTC_RTC(System.DateTime vDate)
    {
        return string.Format("{0:yyyyMMddHHmmss}", vDate);
    }

    public static string fnGetUsableRTC(System.DateTime vDate, bool withoutTime, bool withMaxTime)
    {
        string functionReturnValue = null;
        if (withoutTime == true)
        {
            functionReturnValue = string.Format("{0:yyyyMMdd}", vDate).ToString() + "000000";
        }
        else if (withMaxTime == true)
        {
            functionReturnValue = string.Format("{0:yyyyMMdd}", vDate).ToString() + "235959";
        }
        else
        {
            functionReturnValue = string.Format("{0:yyyyMMddHHmmssfff}", vDate);
        }
        return functionReturnValue;
    }

    public string EncryptPassword(string txtPassword)
    {
        byte[] passBytes = System.Text.Encoding.Unicode.GetBytes(txtPassword);
        string encryptPassword = Convert.ToBase64String(passBytes);
        return encryptPassword;
    }

    public string DecryptPassword(string encryptedPassword)
    {
        byte[] passByteData = Convert.FromBase64String(encryptedPassword);
        string originalPassword = System.Text.Encoding.Unicode.GetString(passByteData);
        return originalPassword;
    }

    public bool isDate(object iDate)
    {
        DateTime date = new DateTime();
        return DateTime.TryParse(DBNulls.StringValue(iDate), out date);
    }

    public void bigButtons(GridItemEventArgs e)
    {
        if (!e.Item.OwnerTableView.IsItemInserted)
        {

            e.Canceled = true;

            // Change Update button UI
            LinkButton updateButton = (LinkButton)e.Item.FindControl("UpdateButton");
            if (updateButton != null)
            {

                updateButton.Style["display"] = "block";
                updateButton.Style["font-weight"] = "bold";
                updateButton.Style["color"] = "#000000";
                updateButton.Style["background-color"] = "#98bf21";
                updateButton.Style["width"] = "100px";
                updateButton.Style["height"] = "30px";
                updateButton.Style["text-align"] = "center";
                updateButton.Style["padding"] = "4px";
                updateButton.Style["text-decoration"] = "none";


                LinkButton cancelButton = (LinkButton)e.Item.FindControl("CancelButton");

                cancelButton.Style["display"] = "block";
                cancelButton.Style["font-weight"] = "bold";
                cancelButton.Style["color"] = "#000000";
                cancelButton.Style["background-color"] = "#CC3300";
                cancelButton.Style["width"] = "100px";
                cancelButton.Style["height"] = "30px";
                cancelButton.Style["text-align"] = "center";
                cancelButton.Style["padding"] = "4px";
                cancelButton.Style["text-decoration"] = "none";
            }
        }
        else
        {
            LinkButton updateButton = (LinkButton)e.Item.FindControl("PerformInsertButton");
            // updateButton.Style["display"] = "block";
            // updateButton.Style["font-weight"] = "bold";
            if (updateButton != null)
            {
                updateButton.Style["display"] = "block";
                updateButton.Style["font-weight"] = "bold";
                updateButton.Style["color"] = "#FFFFFF";
                updateButton.Style["background-color"] = "#98bf21";
                updateButton.Style["width"] = "120px";
                updateButton.Style["height"] = "30px";
                updateButton.Style["text-align"] = "center";
                updateButton.Style["padding"] = "4px";
                updateButton.Style["text-decoration"] = "none";


                LinkButton cancelButton = (LinkButton)e.Item.FindControl("CancelButton");

                cancelButton.Style["display"] = "block";
                cancelButton.Style["font-weight"] = "bold";
                cancelButton.Style["color"] = "#FFFFFF";
                cancelButton.Style["background-color"] = "#CC3300";
                cancelButton.Style["width"] = "120px";
                cancelButton.Style["height"] = "30px";
                cancelButton.Style["text-align"] = "center";
                cancelButton.Style["padding"] = "4px";
                cancelButton.Style["text-decoration"] = "none";
            }
        }
    }

     public bool restrictAllowedQty(int caseNo, double deliveredQty, double asnQty, double scheduledQty, out double allowedQty)
    {
        try
        {
            //Case 1: don't restrict
            //Case 2: consider only Scheduled Qty - Delivered Qty difference
            //Case 3: Qty1 = Scheduled Qty - Delivered Qty and Qty2 = Scheduled Qty - ASN Qty 
            //return whichever difference is higher

            allowedQty = 0;
            if ((scheduledQty - asnQty) < 0)
            {
                allowedQty = 0;
                return true;
            }

            //MAHESH 20150816
            //if (asnQty < 0)
            //{
            //    allowedQty = 0;
            //    return true;
            //}

            switch (caseNo)
            {
                case 1:
                    allowedQty = 0;
                    return false;
                case 2:
                    allowedQty = scheduledQty - deliveredQty;
                    return true;
                case 3:
                    //if (deliveredQty > asnQty)
                    //{
                    //    allowedQty = scheduledQty - deliveredQty;
                    //}
                    //else
                    //{
                    //    allowedQty = scheduledQty - asnQty;
                    //}
                    allowedQty = scheduledQty - (asnQty + deliveredQty);
                    return true;
            }
            return true;

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


}